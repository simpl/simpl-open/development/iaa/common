# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.8.0] - 2024-12-02

### Added
- Added `mandatory` field in `DocumentDTO`
- Added API endpoint HEAD /keypair existsKeypair in `KeyPairExchange`.
- Added `Shedlock` dependency.
- Added `isAuthenticated()` method in `JwtService`.
- Added user `id` to `KeycloakUserDTO`. 
- Added `APPLICANT` role constant.
- Added `filename`, `filesize` and `hasContent` to `DocumentDTO`
- Added readonly `mandatory` property to `DocumentDTO`.
- Added sonar configuration.
- Added DTOs and model interface.
- Added `StatusException` constructor overload.
- Use @UtilityClass annotation for utils
- Added Base64 util to calculate length in bytes from string length

### Changed
- Renamed OnboardingRequestFilter field
- `OnboardingRequestService.create()` now takes an `OnboardingRequestDTO`
- Refactored onboarding procedure.
- Reversed comments order.
- Field `KeycloakUserDTO.roles` is not readonly anymore.
- Converted `id` field type from `long` to `UUID` in `MimeTypeDTO`, `OnboardingStatusDTO`, `ParticipantTypeDTO`.

### Fixed
- Fix query param `ParticipantExchange`.
- Fix `isAuthenticated()` method in `JwtService`
- Fix `CreationTimeStamp` and `updateTimestamp` in `DocumentDTO` and `OnboardingRequestDTO`.
- Fix `AuthorizationHeaderInterceptor`

### Removed
- Remove READ_ONLY access from `KeycloakUserDTO.roles`
- Remove old validation interfaces, to be replaced with events.

## [0.7.0] - 2024-11-11

### Added
- Add `participantType` field on `OnboardingRequestDTO`.
- Added Gateway business logging.
- Added `loadPrivateKey()` in `CredentialUtil`.
- Added auxiliary methods to `PemUtil`.

### Changed
- Update http client version to 0.7.0.
- `AlgorithmConfig` is now an interface.
- Adapted logic to use `credential-id` instead of `participant-id`.
- Refactor `OnboardingRequestExchange`.
- When loading keystore, the sign of the private key and of the certificate is now checked.

### Fixed
- Fixed `CSRExchange`.
- Fixed `LoggingFilter`.
- Fixed `TestCertificateUtil`.
- Fixed `AbstractCertificateSignRequest`.

### Removed
- Removed `participantType` enum.
- Removed `CertificateRequest` because now CSR is stored in DB.
