package eu.europa.ec.simpl.api.authenticationprovider.v1.model;

import jakarta.validation.constraints.NotBlank;

public interface DistinguishedName {
    @NotBlank
    String getCommonName();

    @NotBlank
    String getOrganization();

    @NotBlank
    String getOrganizationalUnit();

    @NotBlank
    String getCountry();
}
