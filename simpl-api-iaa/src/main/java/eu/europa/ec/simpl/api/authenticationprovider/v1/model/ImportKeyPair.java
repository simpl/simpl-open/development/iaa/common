package eu.europa.ec.simpl.api.authenticationprovider.v1.model;

import jakarta.validation.constraints.NotBlank;

public interface ImportKeyPair {
    @NotBlank
    String getPublicKey();

    @NotBlank
    String getPrivateKey();
}
