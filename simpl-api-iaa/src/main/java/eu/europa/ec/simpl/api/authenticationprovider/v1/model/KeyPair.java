package eu.europa.ec.simpl.api.authenticationprovider.v1.model;

public interface KeyPair {
    byte[] getPublicKey();

    byte[] getPrivateKey();
}
