package eu.europa.ec.simpl.api.identityprovider.v1.model;

import java.time.Instant;

public interface Certificate {
    String getPublicKey();

    Instant getExpiryDate();
}
