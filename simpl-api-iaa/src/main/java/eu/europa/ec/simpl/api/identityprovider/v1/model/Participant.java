package eu.europa.ec.simpl.api.identityprovider.v1.model;

import jakarta.validation.constraints.NotBlank;
import java.time.Instant;
import java.util.UUID;

public interface Participant {
    UUID getId();

    @NotBlank
    String getParticipantType();

    @NotBlank
    String getOrganization();

    Instant getCreationTimestamp();

    Instant getUpdateTimestamp();

    String getCredentialId();

    Instant getExpiryDate();
}
