package eu.europa.ec.simpl.api.identityprovider.v1.model;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import eu.europa.ec.simpl.api.securityattributesprovider.v1.model.IdentityAttribute;
import jakarta.validation.Valid;
import java.util.List;

public interface ParticipantWithIdentityAttributes {
    @Valid
    @JsonUnwrapped
    Participant getParticipant();

    List<? extends IdentityAttribute> getIdentityAttributes();
}
