package eu.europa.ec.simpl.api.onboarding.v1.model;

import java.util.List;
import java.util.UUID;

public interface Approve {
    List<UUID> getIdentityAttributes();
}
