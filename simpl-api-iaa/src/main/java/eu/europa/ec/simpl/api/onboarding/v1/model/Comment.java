package eu.europa.ec.simpl.api.onboarding.v1.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import java.time.Instant;
import java.util.UUID;

public interface Comment {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    UUID getId();

    @NotBlank
    String getAuthor();

    @NotBlank
    @Size(max = 5000)
    String getContent();

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    Instant getCreationTimestamp();

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    Instant getUpdateTimestamp();
}
