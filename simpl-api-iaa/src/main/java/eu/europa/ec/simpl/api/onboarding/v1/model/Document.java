package eu.europa.ec.simpl.api.onboarding.v1.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.Instant;
import java.util.UUID;

public interface Document {
    UUID getId();

    DocumentTemplate getDocumentTemplate();

    MimeType getMimeType();

    String getDescription();

    String getFilename();

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    Long getFilesize();

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    Instant getCreationTimestamp();

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    Instant getUpdateTimestamp();

    String getContent();
}
