package eu.europa.ec.simpl.api.onboarding.v1.model;

import jakarta.validation.constraints.NotBlank;
import java.util.UUID;

public interface MimeType {
    UUID getId();

    @NotBlank
    String getValue();

    @NotBlank
    String getDescription();
}
