package eu.europa.ec.simpl.api.onboarding.v1.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;

public interface OnboardingApplicant {
    @NotBlank
    String getUsername();

    @NotBlank
    String getFirstName();

    @NotBlank
    String getLastName();

    @NotBlank
    String getEmail();

    @NotBlank
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    String getPassword();
}
