package eu.europa.ec.simpl.api.onboarding.v1.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

public interface OnboardingRequest {
    UUID getId();

    UUID getParticipantId();

    OnboardingStatus getStatus();

    @NotNull OnboardingApplicant getApplicant();

    @NotNull ParticipantType getParticipantType();

    @NotNull String getOrganization();

    List<? extends Document> getDocuments();

    List<? extends Comment> getComments();

    String getRejectionCause();

    Long getExpirationTimeframe();

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    Instant getCreationTimestamp();

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    Instant getUpdateTimestamp();

    Instant getLastParticipantUpdateTimestamp();
}
