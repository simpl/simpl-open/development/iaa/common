package eu.europa.ec.simpl.api.onboarding.v1.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import java.util.UUID;

public interface OnboardingStatus {
    UUID getId();

    @NotBlank
    @Size(max = 255)
    String getLabel();

    @NotBlank
    @Size(max = 255)
    OnboardingStatusValueDTO getValue();
}
