package eu.europa.ec.simpl.api.onboarding.v1.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

public interface OnboardingTemplate {
    UUID getId();

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    ParticipantType getParticipantType();

    @NotBlank
    String getDescription();

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    Instant getCreationTimestamp();

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    Instant getUpdateTimestamp();

    @Valid
    List<? extends DocumentTemplate> getDocumentTemplates();

    @NotNull Long getExpirationTimeframe();
}
