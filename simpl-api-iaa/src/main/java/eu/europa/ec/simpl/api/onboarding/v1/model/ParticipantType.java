package eu.europa.ec.simpl.api.onboarding.v1.model;

import java.util.UUID;

public interface ParticipantType {
    UUID getId();

    String getValue();

    String getLabel();
}
