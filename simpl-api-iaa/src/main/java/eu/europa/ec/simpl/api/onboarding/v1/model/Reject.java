package eu.europa.ec.simpl.api.onboarding.v1.model;

import jakarta.validation.constraints.NotNull;

public interface Reject {
    @NotNull String getRejectionCause();
}
