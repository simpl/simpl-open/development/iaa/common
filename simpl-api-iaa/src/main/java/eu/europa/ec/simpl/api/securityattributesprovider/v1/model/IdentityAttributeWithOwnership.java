package eu.europa.ec.simpl.api.securityattributesprovider.v1.model;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import jakarta.validation.Valid;

public interface IdentityAttributeWithOwnership {
    @Valid
    @JsonUnwrapped
    IdentityAttribute getIdentityAttribute();

    Boolean getAssignedToParticipant();
}
