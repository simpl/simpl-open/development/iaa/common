package eu.europa.ec.simpl.api.usersroles.v1.model;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import eu.europa.ec.simpl.api.identityprovider.v1.model.ParticipantWithIdentityAttributes;
import java.util.List;

public interface Echo {
    String getUsername();

    String getEmail();

    ConnectionStatusDTO getConnectionStatus();

    MTLSStatusDTO getMtlsStatus();

    List<String> getUserIdentityAttributes();

    @JsonUnwrapped
    ParticipantWithIdentityAttributes getParticipant();
}
