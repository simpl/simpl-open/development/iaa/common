package eu.europa.ec.simpl.api.usersroles.v1.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import java.util.List;

public interface KeycloakUser {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    String getId();

    @NotBlank
    String getUsername();

    @NotBlank
    String getFirstName();

    @NotBlank
    String getLastName();

    @NotBlank
    String getEmail();

    @NotBlank
    String getPassword();

    List<String> getRoles();
}
