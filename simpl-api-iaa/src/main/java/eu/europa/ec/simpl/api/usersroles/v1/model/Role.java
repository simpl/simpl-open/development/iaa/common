package eu.europa.ec.simpl.api.usersroles.v1.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

public interface Role {
    @NotNull UUID getId();

    @NotBlank
    String getName();

    String getDescription();

    List<String> getAssignedIdentityAttributes();
}
