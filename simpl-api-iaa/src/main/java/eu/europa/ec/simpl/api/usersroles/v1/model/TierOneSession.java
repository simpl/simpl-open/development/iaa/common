package eu.europa.ec.simpl.api.usersroles.v1.model;

import jakarta.validation.constraints.NotBlank;

public interface TierOneSession {
    @NotBlank
    String getJwt();
}
