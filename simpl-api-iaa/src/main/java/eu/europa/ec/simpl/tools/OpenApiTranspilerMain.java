package eu.europa.ec.simpl.tools;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.common.base.CaseFormat;
import io.swagger.v3.core.util.Yaml31;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.media.*;
import io.swagger.v3.oas.models.parameters.Parameter;
import io.swagger.v3.oas.models.parameters.RequestBody;
import io.swagger.v3.oas.models.security.*;
import io.swagger.v3.oas.models.servers.Server;
import io.swagger.v3.oas.models.tags.Tag;
import io.swagger.v3.parser.OpenAPIV3Parser;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.WordUtils;
import org.springframework.http.HttpMethod;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.CollectionUtils;

public class OpenApiTranspilerMain {

    // Currenty not completely supported by generators, see
    // https://github.com/OpenAPITools/openapi-generator/pull/14064
    private static final boolean addSpringPaginated = false;

    private static final OpenAPIV3Parser parser = new OpenAPIV3Parser();

    public static final String OAUTH_2_AUTH_CODE_SECURITY_SCHEMA = "oauth2AuthCode";
    public static final String OAUTH_2_CLIENT_CREDENTIALS_SECURITY_SCHEMA = "oauth2ClientCredentials";
    public static final String MTLS_SECURITY_SCHEMA = "mtls";

    private static final Overrides overrides = initOverrides();

    @SneakyThrows
    private static Overrides initOverrides() {
        try {
            return new ObjectMapper(new YAMLFactory())
                    .readValue(new File(System.getenv("OVERRIDES_YAML")), Overrides.class);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            throw e;
        }
    }

    public static void main(String[] args) {

        if (args.length != 1
                || StringUtils.isEmpty(args[0])
                || StringUtils.isEmpty(System.getenv("ROUTES_YAML"))
                || StringUtils.isEmpty(System.getenv("OVERRIDES_YAML"))) {
            System.err.print("Usage: openapi-transpiler path/to/openapi/root/dir");
            System.err.print("ROUTES_YAML path to routes.yaml to get roles, denied urls etc");
            System.err.print("OVERRIDES_YAML path to overrides.yaml to override specific parts of the spec");
            System.exit(1);
        }

        String openapiRootDir = args[0];
        var openapiDirs = new File(openapiRootDir).listFiles();

        if (openapiDirs == null) {
            System.err.print("Could not access directory " + openapiRootDir);
            System.exit(1);
        }

        Arrays.stream(openapiDirs).forEach(OpenApiTranspilerMain::transpileOpenApi);
    }

    @SneakyThrows
    private static void transpileOpenApi(File openapiDir) {

        var openapiSourceFile = Paths.get(openapiDir.toURI()).resolve("v0/openapi.yaml");

        var api = parser.read(openapiSourceFile.toAbsolutePath().toString());

        var openapiTargetDir = Paths.get(openapiDir.toURI()).resolve("v1");

        var openapiWriter = new OpenApiWriter(openapiTargetDir);

        Map<String, Path> schemaPaths = new HashMap<>();

        var schemaNamesToRemove = new ArrayList<>();
        var newSchemas = new HashMap<String, Schema>();

        api.getComponents().getSchemas().forEach((name, schema) -> {
            var newName = sanitizeSchemaName(name);
            schemaNamesToRemove.add(name);
            newSchemas.put(newName, schema);
        });

        schemaNamesToRemove.forEach(
                schemaName -> api.getComponents().getSchemas().remove((String) schemaName));
        api.getComponents().getSchemas().putAll(newSchemas);

        api.getComponents().getSchemas().forEach((schemaName, schema) -> {
            var schemaOverride = overrides.getSchemas().get(schemaName);

            if (schemaOverride == null) {
                return;
            }

            if (schemaOverride.getChangeProperty() != null && schema.getProperties() != null) {

                schemaOverride.getChangeProperty().forEach((propertyName, propOverride) -> {

                    //                    var property = (Schema<?>)schema.getProperties().get(propertyName);
                    if (propOverride.getSchemaType() != null) {
                        var newSchema =
                                switch (propOverride.getSchemaType()) {
                                    case "string" -> new StringSchema();
                                    default -> throw new IllegalStateException(
                                            "Unexpected value: " + propOverride.getSchemaType());
                                };
                        ((Map<String, Schema<?>>) schema.getProperties()).put(propertyName, newSchema);
                    }
                });
            }

            // TODO
            System.err.println();
        });

        api.getComponents().getSchemas().forEach((name, schema) -> {
            if (schema.getProperties() != null) {
                ((Schema<?>) schema).getProperties().forEach((property, propertySchema) -> {
                    if (propertySchema.getItems() != null
                            && propertySchema.getItems().get$ref() != null) {
                        var itemSchemaName = sanitizeSchemaName(StringUtils.substringAfterLast(
                                propertySchema.getItems().get$ref(), "/"));
                        propertySchema.getItems().set$ref("./%s.yaml".formatted(itemSchemaName));
                    }
                    if (propertySchema.get$ref() != null) {
                        var schemaName =
                                sanitizeSchemaName(StringUtils.substringAfterLast(propertySchema.get$ref(), "/"));
                        propertySchema.set$ref("./%s.yaml".formatted(schemaName));
                    }
                });
            }

            var schemaPath = openapiWriter.writeSchema(name, schema);

            var newSchema = new Schema<>();
            schemaPaths.put(name, schemaPath);
            newSchema.set$ref(relativize(openapiTargetDir, schemaPath));
            api.getComponents().getSchemas().put(name, newSchema);
        });

        // Must be done before renaming endpoints
        api.getPaths().forEach(OpenApiTranspilerMain::addSimplPathExtensions);

        api.getPaths().forEach((path, pathItem) -> {
            var get = pathItem.getGet();
            if (get != null && isPageable(get)) {

                if (addSpringPaginated) {
                    get.addExtension("x-spring-paginated", true);
                    get.getParameters()
                            .removeIf(parameter -> parameter.getName().equals("sort"));
                    get.getParameters()
                            .removeIf(parameter -> parameter.getName().equals("page"));
                    get.getParameters()
                            .removeIf(parameter -> parameter.getName().equals("size"));
                }

                var paramsToRemove = new ArrayList<String>();

                var props = get.getParameters().stream()
                        .filter(parameter -> parameter != null
                                && !parameter.getName().equals("sort")
                                && !parameter.getName().equals("page")
                                && !parameter.getName().equals("size"))
                        .map(parameter -> {
                            paramsToRemove.add(parameter.getName());
                            return Map.entry(parameter.getName(), parameter.getSchema());
                        })
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

                paramsToRemove.forEach(parameterName -> get.getParameters()
                        .removeIf(parameter -> parameter.getName().equals(parameterName)));

                var schema = new Schema<>();
                schema.setType("object");
                schema.setProperties(props);

                var filterParam = new Parameter();
                filterParam.setIn("query");
                filterParam.setName("filter");
                filterParam.setSchema(schema);

                get.getParameters().add(filterParam);
            }
        });

        var authCodeSecurityScheme = addAuthCodeSecurityScheme(api);

        var clientCredentialsSecurityScheme = addClientCredentialSecurityScheme(api);

        var mtlsSecurityScheme = addMtlsSecurityScheme(api);

        var tagPluralizer = new Pluralizer(overrides.getTags() /*.entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey, entry -> entry.getValue().getName()))*/);

        List<String> allTags = api.getPaths().values().stream()
                .flatMap(pathItem -> {
                    var tags = mapOperations(pathItem, operation -> {
                        var opTags = operation.getTags().stream()
                                .map(tag -> tag.replace("-controller", ""))
                                .map(tag -> tag.replace(" API", ""))
                                .map(tagPluralizer::pluralize)
                                .map(StringUtils::splitByCharacterTypeCamelCase)
                                .map(strings -> Arrays.stream(strings)
                                        .filter(Predicate.not(String::isBlank))
                                        .toList())
                                .map(strings -> String.join(" ", strings))
                                .map(WordUtils::capitalize)
                                //                                .map(tag -> tag + " API")
                                .toList();
                        operation.setTags(opTags);
                        return opTags;
                    });
                    return tags.stream().flatMap(Collection::stream);
                })
                .distinct()
                .toList();

        var apiTags = allTags.stream()
                .map(tagName -> {
                    var tag = new Tag();
                    tag.setName(tagName /* + " API"*/);
                    var description = String.join(" ", tagName.split("-"));
                    tag.setDescription("API for managing " + WordUtils.capitalize(description));
                    return tag;
                })
                .toList();

        api.setTags(apiTags);

        api.getInfo().setVersion("1.1.1");

        var server = new Server();
        server.setUrl("/v1");
        server.setDescription(openapiDir.getName());
        api.setServers(List.of(server));

        api.getPaths().forEach((path, pathItem) -> {
            mapOperations(pathItem, (httpMethod, operation) -> {
                var override = overrides.getOperations().get(operation.getOperationId());

                if (override == null) {
                    return null;
                }

                if (Boolean.TRUE.equals(override.getFileUpload())) {
                    var body = new RequestBody();
                    var content = new Content();
                    var mediaType = new MediaType();
                    var mediaTypeSchema = new ObjectSchema();
                    var propertySchema = new StringSchema();
                    propertySchema.setFormat("binary");
                    mediaTypeSchema.setProperties(Map.of(override.getFileUploadParamName(), propertySchema));
                    mediaTypeSchema.setRequired(List.of(override.getFileUploadParamName()));
                    mediaType.setSchema(mediaTypeSchema);
                    //                    content.addMediaType("application/octet-stream", mediaType);
                    content.addMediaType("multipart/form-data", mediaType);
                    body.setContent(content);
                    operation.setRequestBody(body);
                }

                if (override.getChangeParameter() != null) {

                    override.getChangeParameter().forEach((paramName, changeParameter) -> {
                        if (operation.getParameters() == null) {
                            return; //
                        }

                        var param = operation.getParameters().stream()
                                .filter(parameter -> parameter.getName().equals(paramName))
                                .findFirst();

                        param.ifPresent(parameter -> {
                            if (Boolean.TRUE.equals(changeParameter.getClearFormat())) {
                                parameter.getSchema().setFormat(null);
                            }
                        });

                        if (Boolean.TRUE.equals(changeParameter.getRemove())) {
                            operation
                                    .getParameters()
                                    .removeIf(parameter -> parameter.getName().equals(paramName));
                        }
                    });
                }

                if (operation.getParameters() != null
                        && operation.getParameters().isEmpty()) {
                    operation.setParameters(null);
                }

                if (override.getChangeBody() != null
                        && Boolean.TRUE.equals(override.getChangeBody().getClearFormat())) {
                    operation.getRequestBody().getContent().values().stream()
                            .findFirst()
                            .ifPresent(mediaType -> {
                                mediaType.getSchema().setFormat(null);
                            });
                }

                if (override.getChangeBody() != null && override.getChangeBody().getSchemaRef() != null) {
                    operation.getRequestBody().getContent().entrySet().stream()
                            .findFirst()
                            .ifPresent(entry -> {
                                entry.getValue()
                                        .getSchema()
                                        .set$ref(override.getChangeBody().getSchemaRef());
                            });
                }

                if (override.getOperationId() != null && override.getApiName().equals(openapiDir.getName())) {
                    operation.setOperationId(override.getOperationId());
                }

                return null;
            });
        });

        api.getPaths().forEach((path, pathItem) -> {
            mapOperations(pathItem, (httpMethod, operation) -> {
                if (operation.getOperationId().contains("_")) {
                    throw new IllegalArgumentException(
                            "Your spec contains duplicated operation id %s. Use overrides to specify an alternative operationId%n"
                                    .formatted(operation.getOperationId()));
                }
                return null;
            });
        });

        api.getPaths().forEach((path, pathItem) -> {
            mapOperations(pathItem, (httpMethod, operation) -> {
                operation.getResponses().forEach((status, apiResponse) -> {
                    if (apiResponse.getContent() == null) {
                        return;
                    }

                    var fileResponse = apiResponse.getContent().entrySet().stream()
                            .filter(resp -> resp.getValue().getSchema() != null
                                    && ((Objects.equals(
                                                            resp.getValue()
                                                                    .getSchema()
                                                                    .getType(),
                                                            "string")
                                                    && Objects.equals(
                                                            resp.getValue()
                                                                    .getSchema()
                                                                    .getFormat(),
                                                            "binary"))
                                            || Objects.equals(
                                                    resp.getValue().getSchema().get$ref(),
                                                    "#/components/schemas/StreamingResponseBody")))
                            .findFirst();

                    if (fileResponse.isPresent()) {
                        var schema = new StringSchema();
                        schema.setFormat("stream");
                        fileResponse.get().getValue().setSchema(schema);
                    }
                });

                return null;
            });
        });

        var newPaths = new io.swagger.v3.oas.models.Paths();

        api.getPaths().forEach((path, pathItem) -> {
            var segments = Segment.parse(path);

            var relativePath = segments.length < 2
                    ? Path.of("")
                    : Path.of(
                            segments[0].format(),
                            Arrays.copyOfRange(
                                    Arrays.stream(segments).map(Segment::format).toArray(String[]::new),
                                    1,
                                    segments.length - 1));

            var segment = segments[segments.length - 1];
            var pathPath = openapiWriter.writePath(relativePath, segment.format(), pathItem);

            updateSchemaPaths(schemaPaths, pathPath, pathItem);
            // TODO Workaround: need to know path before saving
            openapiWriter.writePath(relativePath, segment.format(), pathItem);

            var newPath = new PathItem();
            newPath.set$ref(relativize(openapiTargetDir, pathPath));
            //            api.getPaths().put(path, newPath);
            var pluralizedPath =
                    "/" + Arrays.stream(segments).map(Objects::toString).collect(Collectors.joining("/"));
            newPaths.put(pluralizedPath, newPath);
        });

        api.setPaths(newPaths);

        openapiWriter.writeApi(api);
    }

    private static SecurityScheme addAuthCodeSecurityScheme(OpenAPI api) {
        var securityScheme = new SecurityScheme();
        securityScheme.setType(SecurityScheme.Type.OAUTH2);

        var scopes = new Scopes();
        var flow = new OAuthFlow();
        flow.setAuthorizationUrl("");
        flow.setTokenUrl("");
        flow.setScopes(scopes);
        var flows = new OAuthFlows();
        flows.setAuthorizationCode(flow);

        securityScheme.setFlows(flows);

        api.getComponents().addSecuritySchemes(OAUTH_2_AUTH_CODE_SECURITY_SCHEMA, securityScheme);
        return securityScheme;
    }

    private static SecurityScheme addClientCredentialSecurityScheme(OpenAPI api) {
        var securityScheme = new SecurityScheme();
        securityScheme.setType(SecurityScheme.Type.OAUTH2);

        var scopes = new Scopes();
        var flow = new OAuthFlow();
        flow.setTokenUrl("");
        flow.setScopes(scopes);
        var flows = new OAuthFlows();
        flows.setClientCredentials(flow);

        securityScheme.setFlows(flows);

        api.getComponents().addSecuritySchemes(OAUTH_2_CLIENT_CREDENTIALS_SECURITY_SCHEMA, securityScheme);
        return securityScheme;
    }

    private static SecurityScheme addMtlsSecurityScheme(OpenAPI api) {
        var securityScheme = new SecurityScheme();
        securityScheme.setType(SecurityScheme.Type.MUTUALTLS);
        // TODO attribute components.securitySchemes.mtls.type.type is not of type
        // `http|apiKey|oauth2|openIdConnect|mutualTLS `
        //        api.getComponents().addSecuritySchemes(MTLS_SECURITY_SCHEMA, securityScheme);
        return securityScheme;
    }

    @SneakyThrows
    private static void addSimplPathExtensions(String path, PathItem pathItem) {
        var routes = new ObjectMapper(new YAMLFactory())
                .readValue(new File(System.getenv("ROUTES_YAML")), RouteConfig.class);

        mapOperations(pathItem, (method, operation) -> {
            var matcher = new RouteMatcher(routes, path, method);

            var matches = matcher.matches();

            var roles = matcher.getRbacMatches().stream()
                    .flatMap(rule -> rule.roles().stream())
                    .toList();

            var checksum = 0;

            if (!CollectionUtils.isEmpty(roles)) {
                operation.addExtension("x-simpl-roles", roles);
                checksum++;
            }

            var isPublic = !matcher.getPublicMatches().isEmpty();

            var isDenied = !matcher.getDeniedMatches().isEmpty();

            if (isPublic) {
                checksum++;
            }

            if (isDenied) {
                checksum++;
            }

            if (checksum > 1) {
                throw new IllegalArgumentException(
                        "Operation matches in more than one category (public/denied/rbac): %s %s"
                                .formatted(method, path));
            }

            if (checksum == 0) {
                System.err.printf(
                        "[WARNING] No route config found for operation %s %s (operationId: %s)%n",
                        method, path, operation.getOperationId());
            }

            if (!isPublic) {
                var securityRequirement = new SecurityRequirement();
                securityRequirement.put(OAUTH_2_AUTH_CODE_SECURITY_SCHEMA, List.of());
                operation.setSecurity(List.of(securityRequirement));
            }

            if (isDenied) {
                var securityRequirement = new SecurityRequirement();
                securityRequirement.put(OAUTH_2_CLIENT_CREDENTIALS_SECURITY_SCHEMA, List.of());
                operation.setSecurity(List.of(securityRequirement));
            }

            if (path.contains("mtls")) {
                var securityRequirement = new SecurityRequirement();
                securityRequirement.put(MTLS_SECURITY_SCHEMA, List.of());
                operation.setSecurity(List.of(securityRequirement));
            }

            return matches;
        });
    }

    private static class RouteMatcher {
        private final AntPathMatcher matcher = new AntPathMatcher();
        private final RouteConfig routes;
        private final String path;
        private final HttpMethod method;

        @Getter
        private List<Rule> rbacMatches;

        @Getter
        private List<Rule> publicMatches;

        @Getter
        private List<Rule> deniedMatches;

        public RouteMatcher(RouteConfig routes, String path, HttpMethod method) {
            this.routes = routes;
            this.path = path;
            this.method = method;
        }

        public boolean matches() {
            rbacMatches = getMatches(routes.rbac());
            publicMatches = getMatches(routes.publicUrls());
            deniedMatches = getMatches(routes.deniedUrls());
            return !rbacMatches.isEmpty() || !publicMatches.isEmpty() || !deniedMatches.isEmpty();
        }

        private List<Rule> getMatches(List<Rule> rules) {
            return rules.stream()
                    .filter(rule -> {
                        var ruleWithoutPrefix = rule.path().replaceAll("^/.*?/", "/");
                        return matcher.match(ruleWithoutPrefix, path)
                                && (rule.method() == null || Objects.equals(rule.method(), method));
                    })
                    .toList();
        }
    }

    private static String sanitizeSchemaName(String name) {
        return name.replaceAll("DTO$", "");
    }

    private static boolean isPageable(Operation get) {

        if (get.getParameters() == null) {
            return false;
        }

        var hasSort = hasQueryParameter(get, "sort");
        var hasSize = hasQueryParameter(get, "size");
        var hasPage = hasQueryParameter(get, "page");
        return hasSort && hasSize && hasPage;
    }

    private static boolean hasQueryParameter(Operation get, String name) {
        return get.getParameters().stream()
                .anyMatch(parameter ->
                        parameter.getName().equals(name) && parameter.getIn().equals("query"));
    }

    private static <T> List<T> mapOperations(PathItem pathItem, Function<Operation, T> operationMapper) {
        var results = new ArrayList<T>();

        Optional.ofNullable(pathItem.getGet()).ifPresent(operation -> results.add(operationMapper.apply(operation)));
        Optional.ofNullable(pathItem.getPost()).ifPresent(operation -> results.add(operationMapper.apply(operation)));
        Optional.ofNullable(pathItem.getPatch()).ifPresent(operation -> results.add(operationMapper.apply(operation)));
        Optional.ofNullable(pathItem.getPut()).ifPresent(operation -> results.add(operationMapper.apply(operation)));
        Optional.ofNullable(pathItem.getHead()).ifPresent(operation -> results.add(operationMapper.apply(operation)));
        Optional.ofNullable(pathItem.getOptions())
                .ifPresent(operation -> results.add(operationMapper.apply(operation)));
        Optional.ofNullable(pathItem.getDelete()).ifPresent(operation -> results.add(operationMapper.apply(operation)));
        Optional.ofNullable(pathItem.getTrace()).ifPresent(operation -> results.add(operationMapper.apply(operation)));

        return results;
    }

    private static <T> List<T> mapOperations(PathItem pathItem, BiFunction<HttpMethod, Operation, T> operationMapper) {
        var results = new ArrayList<T>();

        Optional.ofNullable(pathItem.getGet())
                .ifPresent(operation -> results.add(operationMapper.apply(HttpMethod.GET, operation)));
        Optional.ofNullable(pathItem.getPost())
                .ifPresent(operation -> results.add(operationMapper.apply(HttpMethod.POST, operation)));
        Optional.ofNullable(pathItem.getPatch())
                .ifPresent(operation -> results.add(operationMapper.apply(HttpMethod.PATCH, operation)));
        Optional.ofNullable(pathItem.getPut())
                .ifPresent(operation -> results.add(operationMapper.apply(HttpMethod.PUT, operation)));
        Optional.ofNullable(pathItem.getHead())
                .ifPresent(operation -> results.add(operationMapper.apply(HttpMethod.HEAD, operation)));
        Optional.ofNullable(pathItem.getOptions())
                .ifPresent(operation -> results.add(operationMapper.apply(HttpMethod.OPTIONS, operation)));
        Optional.ofNullable(pathItem.getDelete())
                .ifPresent(operation -> results.add(operationMapper.apply(HttpMethod.DELETE, operation)));
        Optional.ofNullable(pathItem.getTrace())
                .ifPresent(operation -> results.add(operationMapper.apply(HttpMethod.TRACE, operation)));

        return results;
    }

    private static void updateSchemaPaths(Map<String, Path> schemaPaths, Path pathPath, PathItem pathItem) {
        updateSchemaPaths(schemaPaths, pathPath, pathItem.getGet());
        updateSchemaPaths(schemaPaths, pathPath, pathItem.getPost());
        updateSchemaPaths(schemaPaths, pathPath, pathItem.getPatch());
        updateSchemaPaths(schemaPaths, pathPath, pathItem.getPut());
        updateSchemaPaths(schemaPaths, pathPath, pathItem.getHead());
        updateSchemaPaths(schemaPaths, pathPath, pathItem.getOptions());
        updateSchemaPaths(schemaPaths, pathPath, pathItem.getDelete());
        updateSchemaPaths(schemaPaths, pathPath, pathItem.getTrace());
    }

    private static void updateSchemaPaths(Map<String, Path> schemaPaths, Path pathPath, Operation operation) {
        if (operation == null) {
            return;
        }

        updateRequestBodySchema(schemaPaths, pathPath, operation);
        updateResponseSchema(schemaPaths, pathPath, operation);
    }

    private static void updateResponseSchema(Map<String, Path> schemaPaths, Path pathPath, Operation operation) {
        operation.getResponses().forEach((responseCode, response) -> {
            if (response.getContent() == null) {
                return;
            }
            response.getContent().forEach((s, mediaType) -> {
                updateMediaTypeSchema(schemaPaths, pathPath, mediaType);
            });
        });
    }

    private static void updateRequestBodySchema(Map<String, Path> schemaPaths, Path pathPath, Operation operation) {
        if (operation.getRequestBody() == null) {
            return;
        }

        operation.getRequestBody().getContent().forEach((mediaTypeName, mediaType) -> {
            updateMediaTypeSchema(schemaPaths, pathPath, mediaType);
        });
    }

    private static void updateMediaTypeSchema(Map<String, Path> schemaPaths, Path pathPath, MediaType mediaType) {

        if (mediaType.getSchema() == null) {
            return;
        }

        if (mediaType.getSchema().getItems() != null
                && mediaType.getSchema().getItems().get$ref() != null) {
            var itemSchemaName = sanitizeSchemaName(StringUtils.substringAfterLast(
                    mediaType.getSchema().getItems().get$ref(), "/"));
            var schemaPath = schemaPaths.get(itemSchemaName);
            var newSchema = new ArraySchema();
            var newItemSchema = new Schema<>();
            // TODO Workaround to fix relative path
            newItemSchema.set$ref(relativize(pathPath, schemaPath).replaceFirst("../", ""));
            newSchema.setItems(newItemSchema);
            mediaType.setSchema(newSchema);
        }
        if (mediaType.getSchema().get$ref() != null) {
            var schemaName = sanitizeSchemaName(
                    StringUtils.substringAfterLast(mediaType.getSchema().get$ref(), "/"));
            var schemaPath = schemaPaths.get(schemaName);
            var newSchema = new Schema<>();
            // TODO Workaround to fix relative path
            newSchema.set$ref(relativize(pathPath, schemaPath).replaceFirst("../", ""));
            mediaType.setSchema(newSchema);
        }
    }

    private static String relativize(Path a, Path b) {
        return FilenameUtils.separatorsToUnix(a.relativize(b).toString());
    }

    private static class Segment {

        public static Segment[] parse(String path) {
            return Arrays.stream(path.split("/"))
                    .filter(Predicate.not(String::isBlank))
                    .map(Segment::new)
                    .toArray(Segment[]::new);
        }

        private static String sanitize(String s) {
            return s.replace("{", "").replace("}", "");
        }

        private final String segment;
        private final Boolean isVariable;
        private final Pluralizer pluralizer;

        private Segment(String segment) {
            String sanitized = sanitize(segment);
            this.isVariable = !sanitized.equals(segment);
            this.segment = sanitized;
            this.pluralizer = new Pluralizer(overrides.getPaths());
        }

        public String format() {
            return isVariable ? segment : pluralizer.pluralize(segment);
        }

        @Override
        public String toString() {
            return isVariable ? "{%s}".formatted(format()) : format();
        }
    }

    @RequiredArgsConstructor
    private static class Pluralizer {
        private final Map<String, String> overrides;

        public String pluralize(String s) {
            if (overrides.get(s) != null) {
                return overrides.get(s);
            }
            return CaseFormat.LOWER_HYPHEN.to(CaseFormat.LOWER_CAMEL, s) + "s";
        }
    }

    @RequiredArgsConstructor
    public static class OpenApiWriter {
        private final Path openapiRoot;

        @SneakyThrows
        private static Path write(Path path, Object o) {
            var file = path.toFile();
            file.getParentFile().mkdirs();
            try (var writer = Yaml31.pretty().writeValues(file)) {
                writer.write(o);
            }
            return path;
        }

        public Path writeSchema(String name, Schema<?> schema) {
            var path = openapiRoot.resolve(Path.of("schemas", "%s.yaml".formatted(name)));
            return write(path, schema);
        }

        public Path writePath(Path relativePath, String name, PathItem pathItem) {
            var directoryPath = openapiRoot.resolve("paths").resolve(relativePath);
            var path = directoryPath.resolve(Path.of("%s.yaml".formatted(name)));
            return write(path, pathItem);
        }

        public Path writeApi(OpenAPI openAPI) {
            var path = openapiRoot.resolve("openapi.yaml");
            return write(path, openAPI);
        }
    }

    @Data
    private static class Overrides {

        Map<String, String> tags;
        Map<String, String> paths;

        Map<String, OpOverride> operations;

        Map<String, SchemaOverride> schemas;

        //        @Data
        //        public static class TagOverride {
        //            String name;
        //            String description;
        //        }

        @Data
        public static class OpOverride {
            Boolean fileUpload;
            String fileUploadParamName;
            String operationId;
            String apiName; // currently supported only for operations

            Map<String, ChangeParameter> changeParameter;
            ChangeParameter changeBody;
        }

        @Data
        public static class SchemaOverride {
            Map<String, ChangeParameter> changeProperty;
        }

        @Data
        public static class ChangeParameter {
            Boolean clearFormat;
            Boolean remove;
            String schemaRef;
            String schemaType;
        }
        /*
          changeProperty:
            password:
              schema:
                type: string
        * */
    }
}
