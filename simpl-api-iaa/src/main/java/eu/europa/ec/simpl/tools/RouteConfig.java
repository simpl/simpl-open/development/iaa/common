package eu.europa.ec.simpl.tools;

import java.util.List;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "routes")
public record RouteConfig(List<Rule> publicUrls, List<Rule> deniedUrls, List<Rule> rbac) {}
