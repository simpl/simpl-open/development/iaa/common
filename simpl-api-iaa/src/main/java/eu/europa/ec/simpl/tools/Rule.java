package eu.europa.ec.simpl.tools;

import java.util.List;
import org.springframework.http.HttpMethod;

public record Rule(HttpMethod method, String path, List<String> roles) {}
