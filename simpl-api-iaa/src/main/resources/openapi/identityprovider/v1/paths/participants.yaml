get:
  tags:
    - Participants
  summary: Search for participants
  description: Searches for participants based on the provided filter criteria
  operationId: search
  parameters:
    - name: page
      in: query
      description: Zero-based page index (0..N)
      required: false
      style: form
      explode: true
      schema:
        type: integer
        default: 0
        minimum: 0
    - name: size
      in: query
      description: The size of the page to be returned
      required: false
      style: form
      explode: true
      schema:
        type: integer
        default: 10
        minimum: 1
    - name: sort
      in: query
      description: "Sorting criteria in the format: property,(asc|desc). Default sort\
      \ order is ascending. Multiple sort criteria are supported."
      required: false
      style: form
      explode: true
      schema:
        type: array
        default:
          - "id,ASC"
        items:
          type: string
    - name: filter
      x-parameter-object: true
      in: query
      schema:
        properties:
          isAuthority:
            type: boolean
          organization:
            type: string
          participantType:
            type: string
          dateTo:
            type: string
            format: date-time
          credentialId:
            type: string
          dateFrom:
            type: string
            format: date-time
  responses:
    "200":
      description: Participants retrieved successfully
      content:
        'application/json':
          schema:
            $ref: ../../schemas/PageResponseParticipant.yaml
    "400":
      description: Invalid filter criteria
  security:
    - oauth2AuthCode: []
  x-simpl-roles:
    - NOTARY
post:
  tags:
  - Participants
  summary: Create a new participant
  description: Creates a new participant with the provided details
  operationId: create
  requestBody:
    description: Details of the participant to create and of the identity attributes
      that will be assigned to it
    content:
      application/json:
        schema:
          $ref: ../schemas/ParticipantWithIdentityAttributes.yaml
    required: true
  responses:
    "201":
      description: Participant created successfully
      content:
        application/json:
          schema:
            type: string
            format: uuid
    "400":
      description: Invalid participant data
    "401":
      description: Access denied
    "403":
      description: "Forbidden: User does not have the required role"
  security:
  - oauth2ClientCredentials: []
