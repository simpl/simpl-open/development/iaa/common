openapi: 3.0.1
info:
  title: OpenAPI definition
  version: v0
servers:
  - url: http://localhost:8080
    description: Generated server url
tags:
  - name: Onboarding Requests API
    description: API for managing onboarding requests
  - name: Participant Type API
    description: API for managing participant type
paths:
  /onboarding-template/{participantTypeId}:
    get:
      tags:
        - onboarding-template-controller
      summary: Get Onboarding Template by Participant Type
      description: Retrieves an onboarding template for a specific participant type.
      operationId: getOnboardingTemplate
      parameters:
        - name: participantTypeId
          in: path
          description: Type of participant for which the onboarding template is retrieved
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '200':
          description: Onboarding template retrieved successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OnboardingTemplateDTO'
        '204':
          description: Onboarding template not present for the specified participant type
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/OnboardingTemplateDTO'
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
        '404':
          description: Participant type not found
    put:
      tags:
        - onboarding-template-controller
      summary: Update Onboarding Template
      description: Updates the onboarding template for a specific participant type.
      operationId: updateOnboardingTemplate
      parameters:
        - name: participantTypeId
          in: path
          description: Type of participant for which the onboarding template is updated
          required: true
          schema:
            type: string
            format: uuid
      requestBody:
        description: Onboarding template data to be updated
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/OnboardingTemplateDTO'
        required: true
      responses:
        '200':
          description: Onboarding template updated successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OnboardingTemplateDTO'
        '400':
          description: Invalid input data
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
        '404':
          description: Participant type not found
    delete:
      tags:
        - onboarding-template-controller
      summary: Delete Onboarding Template
      description: Deletes the onboarding template for a specific participant type.
      operationId: deleteOnboardingTemplate
      parameters:
        - name: participantTypeId
          in: path
          description: Type of participant for which the onboarding template is deleted
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '204':
          description: Onboarding template deleted successfully
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
        '404':
          description: Participant type not found
    patch:
      tags:
        - onboarding-template-controller
      summary: Update Onboarding Template
      description: Updates the onboarding template for a specific participant type with a list of document template UUIDs.
      operationId: updateOnboardingTemplate_1
      parameters:
        - name: participantTypeId
          in: path
          description: Type of participant for which the onboarding template is updated
          required: true
          schema:
            type: string
            format: uuid
      requestBody:
        description: List of document template UUIDs to be associated with the onboarding template
        content:
          application/json:
            schema:
              type: array
              items:
                type: string
                format: uuid
        required: true
      responses:
        '200':
          description: Onboarding template updated successfully
        '400':
          description: Invalid input data
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
        '404':
          description: Participant type not found
  /mime-type/{id}:
    get:
      tags:
        - mime-type-controller
      summary: Get MIME type by ID
      description: Retrieves a MIME type by its unique identifier.
      operationId: getMimeType
      parameters:
        - name: id
          in: path
          description: Unique identifier of the MIME type
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '200':
          description: Successfully retrieved MIME type
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MimeTypeDTO'
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
        '404':
          description: MIME type not found
    put:
      tags:
        - mime-type-controller
      summary: Update MIME type by ID
      description: Updates an existing MIME type with the provided details.
      operationId: updateMimeType
      parameters:
        - name: id
          in: path
          description: Unique identifier of the MIME type to be updated
          required: true
          schema:
            type: string
            format: uuid
      requestBody:
        description: Updated MIME type details
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/MimeTypeDTO'
        required: true
      responses:
        '200':
          description: MIME type updated successfully
        '400':
          description: Invalid input data
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
        '404':
          description: MIME type not found
    delete:
      tags:
        - mime-type-controller
      summary: Delete MIME type by ID
      description: Deletes a MIME type by its unique identifier.
      operationId: deleteMimeType
      parameters:
        - name: id
          in: path
          description: Unique identifier of the MIME type to be deleted
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '204':
          description: MIME type deleted successfully
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
  /onboarding-request:
    get:
      tags:
        - Onboarding Requests API
      summary: Search Onboarding Requests
      description: Searches for onboarding requests based on filters and pagination
      operationId: search
      parameters:
        - name: status
          in: query
          required: false
          schema:
            type: string
        - name: email
          in: query
          required: false
          schema:
            type: string
        - name: creationTimestampFrom
          in: query
          required: false
          schema:
            type: string
            format: date-time
        - name: creationTimestampTo
          in: query
          required: false
          schema:
            type: string
            format: date-time
        - name: updateTimestampFrom
          in: query
          required: false
          schema:
            type: string
            format: date-time
        - name: updateTimestampTo
          in: query
          required: false
          schema:
            type: string
            format: date-time
        - name: lastParticipantUpdateTimestampFrom
          in: query
          required: false
          schema:
            type: string
            format: date-time
        - name: lastParticipantUpdateTimestampTo
          in: query
          required: false
          schema:
            type: string
            format: date-time
        - name: page
          in: query
          description: Zero-based page index (0..N)
          required: false
          schema:
            minimum: 0
            type: integer
            default: 0
        - name: size
          in: query
          description: The size of the page to be returned
          required: false
          schema:
            minimum: 1
            type: integer
            default: 10
        - name: sort
          in: query
          description: 'Sorting criteria in the format: property,(asc|desc). Default sort order is ascending. Multiple sort criteria are supported.'
          required: false
          schema:
            type: array
            items:
              type: string
            default:
              - creationTimestamp,ASC
      responses:
        '200':
          description: Search results
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/PageResponseOnboardingRequestDTO'
        '400':
          description: Invalid search criteria provided
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
    post:
      tags:
        - Onboarding Requests API
      summary: Create Onboarding Request
      description: Creates a new onboarding request
      operationId: create
      requestBody:
        description: The onboarding request to create
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/OnboardingRequestDTO'
        required: true
      responses:
        '201':
          description: Onboarding request created successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OnboardingRequestDTO'
        '400':
          description: Invalid request data provided
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
  /onboarding-request/{id}/submit:
    post:
      tags:
        - Onboarding Requests API
      operationId: submit
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '200':
          description: OK
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/OnboardingRequestDTO'
  /onboarding-request/{id}/request-revision:
    post:
      tags:
        - Onboarding Requests API
      operationId: requestRevision
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '200':
          description: OK
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/OnboardingRequestDTO'
  /onboarding-request/{id}/reject:
    post:
      tags:
        - Onboarding Requests API
      operationId: reject
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
            format: uuid
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/RejectDTO'
        required: true
      responses:
        '200':
          description: OK
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/OnboardingRequestDTO'
  /onboarding-request/{id}/document:
    post:
      tags:
        - Onboarding Requests API
      summary: Add Document to Onboarding Request
      description: Adds a document to an existing onboarding request
      operationId: requestAdditionalDocument
      parameters:
        - name: id
          in: path
          description: The ID of the onboarding request
          required: true
          schema:
            type: string
            format: uuid
      requestBody:
        description: The document to add
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/DocumentDTO'
        required: true
      responses:
        '200':
          description: Document added successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OnboardingRequestDTO'
        '400':
          description: Invalid request data provided
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
        '404':
          description: Onboarding request not found
    patch:
      tags:
        - Onboarding Requests API
      summary: Set Document for Onboarding Request
      description: Updates a document for an existing onboarding request
      operationId: uploadDocument
      parameters:
        - name: id
          in: path
          description: The ID of the onboarding request
          required: true
          schema:
            type: string
            format: uuid
      requestBody:
        description: The document to upload or update
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/DocumentDTO'
        required: true
      responses:
        '200':
          description: Document updated successfully
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/OnboardingRequestDTO'
        '400':
          description: Invalid request data provided
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
        '404':
          description: Onboarding request not found
  /onboarding-request/{id}/approve:
    post:
      tags:
        - Onboarding Requests API
      operationId: approve
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
            format: uuid
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ApproveDTO'
        required: true
      responses:
        '200':
          description: OK
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/OnboardingRequestDTO'
  /mime-type:
    get:
      tags:
        - mime-type-controller
      summary: Get all MIME types
      description: Retrieves a paginated list of all MIME types.
      operationId: getAllMimeTypes
      parameters:
        - name: page
          in: query
          description: Zero-based page index (0..N)
          required: false
          schema:
            minimum: 0
            type: integer
            default: 0
        - name: size
          in: query
          description: The size of the page to be returned
          required: false
          schema:
            minimum: 1
            type: integer
            default: 10
        - name: sort
          in: query
          description: 'Sorting criteria in the format: property,(asc|desc). Default sort order is ascending. Multiple sort criteria are supported.'
          required: false
          schema:
            type: array
            items:
              type: string
            default:
              - id,ASC
      responses:
        '200':
          description: Successfully retrieved list of MIME types
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/PagedModelMimeTypeDTO'
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
    post:
      tags:
        - mime-type-controller
      summary: Create a new MIME type
      description: Creates a new MIME type with the provided details.
      operationId: createMimeType
      requestBody:
        description: MIME type details
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/MimeTypeDTO'
        required: true
      responses:
        '201':
          description: MIME type created successfully
          content:
            application/json:
              schema:
                type: string
                format: uuid
        '400':
          description: Invalid input data
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
        '422':
          description: Mime type not supported
  /onboarding-status/{id}:
    patch:
      tags:
        - onboarding-status-controller
      summary: Set Onboarding Status Label
      description: Set the onboarding status label
      operationId: setLabel
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
            format: uuid
        - name: label
          in: query
          required: true
          schema:
            type: string
      responses:
        '204':
          description: onboarding status label set correctly
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
        '404':
          description: 'Not found: No Status associated to the id'
  /onboarding-request/{id}/expiration-timeframe:
    patch:
      tags:
        - Onboarding Requests API
      summary: Set Expiration Timeframe for Onboarding Request
      description: Sets the expiration timeframe for an existing onboarding request
      operationId: setExpirationTimeframe
      parameters:
        - name: id
          in: path
          description: The ID of the onboarding request
          required: true
          schema:
            type: string
            format: uuid
        - name: expirationTimeframe
          in: query
          description: The expiration timeframe in milliseconds
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: Expiration timeframe set successfully
        '400':
          description: Invalid request data provided
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
        '404':
          description: Onboarding request not found
  /onboarding-request/{id}/comment:
    patch:
      tags:
        - Onboarding Requests API
      summary: Add Comment to Onboarding Request
      description: Adds a comment to an existing onboarding request
      operationId: addComment
      parameters:
        - name: id
          in: path
          description: The ID of the onboarding request
          required: true
          schema:
            type: string
            format: uuid
      requestBody:
        description: The comment to add
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CommentDTO'
        required: true
      responses:
        '200':
          description: Comment added successfully
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/OnboardingRequestDTO'
        '400':
          description: Invalid request data provided
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
        '404':
          description: Onboarding request not found
  /participant-type:
    get:
      tags:
        - Participant Type API
      summary: Get all participant types
      description: Returns a set of all participant types available in the system.
      operationId: getParticipantTypes
      responses:
        '200':
          description: Successfully retrieved participant types
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/ParticipantTypeDTO'
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
  /onboarding-template:
    get:
      tags:
        - onboarding-template-controller
      summary: Get Onboarding Templates
      description: Retrieves a list of all onboarding templates.
      operationId: getOnboardingTemplates
      responses:
        '200':
          description: List of onboarding templates retrieved successfully
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/OnboardingTemplateDTO'
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
  /onboarding-status:
    get:
      tags:
        - onboarding-status-controller
      summary: Get Onboarding Status
      description: Retrieves the onboarding status
      operationId: getStatus
      responses:
        '200':
          description: onboarding status retrieved successfully
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/OnboardingStatusDTO'
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
  /onboarding-request/{onboardingRequestId}:
    get:
      tags:
        - Onboarding Requests API
      summary: Get Onboarding Request
      description: Retrieves an onboarding request by its ID
      operationId: getOnboardingRequest
      parameters:
        - name: onboardingRequestId
          in: path
          description: The ID of the onboarding request
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '200':
          description: Onboarding request retrieved successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OnboardingRequestDTO'
        '400':
          description: Invalid request data provided
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
        '404':
          description: Onboarding request not found
  /onboarding-request/{onboardingRequestId}/document/{documentId}:
    get:
      tags:
        - Onboarding Requests API
      summary: Get Document from Onboarding Request
      description: Retrieves a specific document from an existing onboarding request
      operationId: getDocument
      parameters:
        - name: onboardingRequestId
          in: path
          description: The ID of the onboarding request
          required: true
          schema:
            type: string
            format: uuid
        - name: documentId
          in: path
          description: The ID of the document to retrieve
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '200':
          description: Document retrieved successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DocumentDTO'
        '400':
          description: Invalid request data provided
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
        '404':
          description: Onboarding request or document not found
    delete:
      tags:
        - Onboarding Requests API
      operationId: deleteDocument
      parameters:
        - name: onboardingRequestId
          in: path
          required: true
          schema:
            type: string
            format: uuid
        - name: documentId
          in: path
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '204':
          description: No Content
  /mime-type/supported:
    get:
      tags:
        - mime-type-controller
      summary: Get all supported MIME types
      description: Retrieves all supported MIME types.
      operationId: getAllAvailableMimeTypes
      parameters:
        - name: search
          in: query
          required: false
          schema:
            type: string
            default: ''
      responses:
        '200':
          description: Successfully retrieved list of MIME types
          content:
            '*/*':
              schema:
                type: array
                items:
                  type: string
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
components:
  schemas:
    ErrorDTO:
      type: object
      properties:
        error:
          type: string
        elementName:
          type: string
    DocumentTemplateDTO:
      required:
        - name
        - description
        - id
        - mandatory
        - mimeType
      type: object
      properties:
        id:
          type: string
          format: uuid
        name:
          type: string
        description:
          type: string
        mandatory:
          type: boolean
        mimeType:
          $ref: '#/components/schemas/MimeTypeDTO'
        creationTimestamp:
          type: string
          format: date-time
          readOnly: true
        updateTimestamp:
          type: string
          format: date-time
          readOnly: true
    MimeTypeDTO:
      required:
        - description
        - value
      type: object
      properties:
        id:
          type: string
          format: uuid
        value:
          type: string
        description:
          type: string
    OnboardingTemplateDTO:
      required:
        - description
        - expirationTimeframe
      type: object
      properties:
        id:
          type: string
          format: uuid
        participantType:
          $ref: '#/components/schemas/ParticipantTypeDTO'
        description:
          type: string
        creationTimestamp:
          type: string
          format: date-time
          readOnly: true
        updateTimestamp:
          type: string
          format: date-time
          readOnly: true
        documentTemplates:
          type: array
          items:
            $ref: '#/components/schemas/DocumentTemplateDTO'
        expirationTimeframe:
          type: integer
          format: int64
    ParticipantTypeDTO:
      type: object
      properties:
        id:
          type: string
          format: uuid
        value:
          type: string
        label:
          type: string
    CommentDTO:
      required:
        - author
        - content
      type: object
      properties:
        id:
          type: string
          format: uuid
          readOnly: true
        author:
          type: string
        content:
          type: string
        creationTimestamp:
          type: string
          format: date-time
          readOnly: true
        updateTimestamp:
          type: string
          format: date-time
          readOnly: true
    DocumentDTO:
      type: object
      properties:
        id:
          type: string
          format: uuid
        documentTemplate:
          $ref: '#/components/schemas/DocumentTemplateDTO'
        mimeType:
          $ref: '#/components/schemas/MimeTypeDTO'
        description:
          type: string
        filename:
          type: string
        filesize:
          type: integer
          format: int64
          readOnly: true
        creationTimestamp:
          type: string
          format: date-time
          readOnly: true
        updateTimestamp:
          type: string
          format: date-time
          readOnly: true
        content:
          type: string
        hasContent:
          type: boolean
        mandatory:
          type: boolean
    OnboardingApplicantDTO:
      required:
        - email
        - firstName
        - lastName
        - password
        - username
      type: object
      properties:
        username:
          type: string
        firstName:
          type: string
        lastName:
          type: string
        email:
          type: string
        password:
          $ref: '#/components/schemas/OnboardingApplicantDTO'
    OnboardingRequestDTO:
      required:
        - applicant
        - organization
        - participantType
      type: object
      properties:
        id:
          type: string
          format: uuid
        participantId:
          type: string
          format: uuid
        status:
          $ref: '#/components/schemas/OnboardingStatusDTO'
        applicant:
          $ref: '#/components/schemas/OnboardingApplicantDTO'
        participantType:
          $ref: '#/components/schemas/ParticipantTypeDTO'
        organization:
          type: string
        documents:
          type: array
          items:
            $ref: '#/components/schemas/DocumentDTO'
        comments:
          type: array
          items:
            $ref: '#/components/schemas/CommentDTO'
        rejectionCause:
          type: string
        expirationTimeframe:
          type: integer
          format: int64
        creationTimestamp:
          type: string
          format: date-time
          readOnly: true
        updateTimestamp:
          type: string
          format: date-time
          readOnly: true
        lastStatusUpdateTimestamp:
          type: string
          format: date-time
          readOnly: true
    OnboardingStatusDTO:
      required:
        - label
        - value
      type: object
      properties:
        id:
          type: string
          format: uuid
        label:
          type: string
        value:
          $ref: '#/components/schemas/OnboardingStatusValue'
    OnboardingStatusValue:
      type: string
      enum:
        - IN_PROGRESS
        - IN_REVIEW
        - REJECTED
        - APPROVED
    RejectDTO:
      required:
        - rejectionCause
      type: object
      properties:
        rejectionCause:
          type: string
    ApproveDTO:
      type: object
      properties:
        identityAttributes:
          type: array
          items:
            type: string
            format: uuid
    PageMetadata:
      type: object
      properties:
        size:
          type: integer
          format: int64
        number:
          type: integer
          format: int64
        totalElements:
          type: integer
          format: int64
        totalPages:
          type: integer
          format: int64
    PageResponseOnboardingRequestDTO:
      type: object
      properties:
        content:
          type: array
          items:
            $ref: '#/components/schemas/OnboardingRequestDTO'
        page:
          $ref: '#/components/schemas/PageMetadata'
        empty:
          type: boolean
    PagedModelMimeTypeDTO:
      type: object
      properties:
        content:
          type: array
          items:
            $ref: '#/components/schemas/MimeTypeDTO'
        page:
          $ref: '#/components/schemas/PageMetadata'
