get:
  tags:
  - Onboarding Templates
  summary: Get Onboarding Template by id
  description: Retrieves an onboarding template for a specific id.
  operationId: getOnboardingTemplate
  parameters:
  - name: onboardingProcedureTemplateId
    in: path
    description: Type of participant for which the onboarding template is retrieved
    required: true
    style: simple
    explode: false
    schema:
      type: string
      format: uuid
  responses:
    "200":
      description: Onboarding template retrieved successfully
      content:
        application/json:
          schema:
            $ref: ../../schemas/OnboardingTemplate.yaml
    "204":
      description: Onboarding template not present by the specified id
      content:
        application/json:
          schema:
            $ref: ../../schemas/OnboardingTemplate.yaml
    "401":
      description: Access denied
    "403":
      description: "Forbidden: User does not have the required role"
    "404":
      description: Onboarding procedure template not found
  security:
  - oauth2AuthCode: []
  x-simpl-roles:
  - T2IAA_M
put:
  tags:
  - Onboarding Templates
  summary: Update Onboarding Template
  description: Updates the onboarding template by id.
  operationId: updateOnboardingTemplate
  parameters:
  - name: onboardingProcedureTemplateId
    in: path
    description: Type of participant for which the onboarding template is updated
    required: true
    style: simple
    explode: false
    schema:
      type: string
      format: uuid
  requestBody:
    description: Onboarding template data to be updated
    content:
      application/json:
        schema:
          $ref: ../../schemas/OnboardingTemplate.yaml
    required: true
  responses:
    "200":
      description: Onboarding template updated successfully
      content:
        application/json:
          schema:
            $ref: ../../schemas/OnboardingTemplate.yaml
    "400":
      description: Invalid input data
    "401":
      description: Access denied
    "403":
      description: "Forbidden: User does not have the required role"
    "404":
      description: Onboarding procedure template not found
  security:
  - oauth2AuthCode: []
  x-simpl-roles:
  - T2IAA_M
delete:
  tags:
  - Onboarding Templates
  summary: Delete Onboarding Template
  description: Deletes the onboarding template by id.
  operationId: deleteOnboardingTemplate
  parameters:
  - name: onboardingProcedureTemplateId
    in: path
    description: Type of participant for which the onboarding template is deleted
    required: true
    style: simple
    explode: false
    schema:
      type: string
      format: uuid
  responses:
    "204":
      description: Onboarding template deleted successfully
    "401":
      description: Access denied
    "403":
      description: "Forbidden: User does not have the required role"
    "404":
      description: Onboarding procedure template not found
  security:
  - oauth2AuthCode: []
  x-simpl-roles:
  - T2IAA_M
patch:
  tags:
  - Onboarding Templates
  deprecated: true
  summary: Not used.
  description: Updates the onboarding template by id.
  operationId: updateOnboardingTemplateDocuments
  parameters:
  - name: onboardingProcedureTemplateId
    in: path
    description: Type of participant for which the onboarding template is updated
    required: true
    style: simple
    explode: false
    schema:
      type: string
      format: uuid
  requestBody:
    description: List of document template UUIDs to be associated with the onboarding
      template
    content:
      application/json:
        schema:
          type: array
          items:
            type: string
            format: uuid
    required: true
  responses:
    "200":
      description: Onboarding template updated successfully
    "400":
      description: Invalid input data
    "401":
      description: Access denied
    "403":
      description: "Forbidden: User does not have the required role"
    "404":
      description: Onboarding procedure template not found
  security:
  - oauth2AuthCode: []
  x-simpl-roles:
  - T2IAA_M
