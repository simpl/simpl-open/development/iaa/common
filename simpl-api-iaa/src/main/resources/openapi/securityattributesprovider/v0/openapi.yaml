openapi: 3.0.1
info:
  title: OpenAPI definition
  version: v0
servers:
  - url: http://localhost:8082
    description: Generated server url
tags:
  - name: Identity Attribute API
    description: API for managing identity attributes
paths:
  /identity-attribute/{id}:
    get:
      tags:
        - Identity Attribute API
      summary: Find Identity Attribute by ID
      description: Retrieves an identity attribute by its unique identifier.
      operationId: findById
      parameters:
        - name: id
          in: path
          description: UUID of the identity attribute to be retrieved
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '200':
          description: Successfully retrieved identity attribute
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/IdentityAttributeDTO'
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
        '404':
          description: Identity attribute not found
    put:
      tags:
        - Identity Attribute API
      summary: Update Identity Attribute
      description: Updates an existing identity attribute by its unique identifier.
      operationId: updateAttributes
      parameters:
        - name: id
          in: path
          description: UUID of the identity attribute to be updated
          required: true
          schema:
            type: string
            format: uuid
      requestBody:
        description: Updated identity attribute data
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/IdentityAttributeDTO'
        required: true
      responses:
        '204':
          description: Successfully updated identity attribute
        '400':
          description: Invalid input data
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
        '404':
          description: Identity attribute not found
    delete:
      tags:
        - Identity Attribute API
      summary: Delete Identity Attribute
      description: Deletes an identity attribute by its unique identifier.
      operationId: delete
      parameters:
        - name: id
          in: path
          description: UUID of the identity attribute to be deleted
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '204':
          description: Successfully deleted identity attribute
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
        '404':
          description: Identity attribute not found
  /identity-attribute/assignable/{value}:
    put:
      tags:
        - Identity Attribute API
      summary: Update Assignable Parameter
      description: Updates the assignable parameter for a list of identity attributes.
      operationId: updateAssignableParameter
      parameters:
        - name: value
          in: path
          description: Boolean value to assign to the 'assignable' parameter
          required: true
          schema:
            type: boolean
      requestBody:
        description: List of UUIDs representing identity attributes
        content:
          application/json:
            schema:
              type: array
              items:
                type: string
                format: uuid
        required: true
      responses:
        '200':
          description: Successfully updated assignable parameter
        '400':
          description: Invalid input data
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
  /identity-attribute/assign/{participantType}/{participantId}:
    put:
      tags:
        - Identity Attribute API
      operationId: assignSnapshot
      parameters:
        - name: participantType
          in: path
          required: true
          schema:
            type: string
        - name: participantId
          in: path
          required: true
          schema:
            type: string
            format: uuid
      requestBody:
        content:
          application/json:
            schema:
              type: array
              items:
                type: string
                format: uuid
        required: true
      responses:
        '204':
          description: No Content
  /identity-attribute/assign-participant/{participantId}:
    put:
      tags:
        - Identity Attribute API
      summary: Assign Identity Attributes
      description: Assigns a list of identity attributes to a user.
      operationId: assign
      parameters:
        - name: participantId
          in: path
          description: UUID of the user to whom the identity attributes will be assigned
          required: true
          schema:
            type: string
            format: uuid
      requestBody:
        description: List of UUIDs representing identity attributes to be assigned
        content:
          application/json:
            schema:
              type: array
              items:
                type: string
                format: uuid
        required: true
      responses:
        '204':
          description: Successfully assigned identity attributes
        '400':
          description: Invalid input data
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
  /identity-attribute/add-participant-type/{participantType}:
    put:
      tags:
        - Identity Attribute API
      summary: Add Participant Type
      description: Adds a participant type to a list of identity attributes.
      operationId: addParticipantType
      parameters:
        - name: participantType
          in: path
          description: Type of participant to be added
          required: true
          schema:
            type: string
      requestBody:
        description: Array of UUIDs representing identity attributes
        content:
          application/json:
            schema:
              type: array
              items:
                type: string
                format: uuid
        required: true
      responses:
        '200':
          description: Successfully added participant type
        '400':
          description: Invalid input data
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
  /identity-attribute:
    post:
      tags:
        - Identity Attribute API
      summary: Create Identity Attribute
      description: Creates a new identity attribute.
      operationId: create
      requestBody:
        description: Identity attribute to be created
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/IdentityAttributeDTO'
        required: true
      responses:
        '201':
          description: Successfully created identity attribute
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/IdentityAttributeDTO'
        '400':
          description: Invalid input data
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
        '409':
          description: 'Conflict: Identity attribute with the same code or name already exists'
  /identity-attribute/import:
    post:
      tags:
        - Identity Attribute API
      operationId: importIdentityAttributes
      requestBody:
        content:
          application/json:
            schema:
              type: array
              items:
                $ref: '#/components/schemas/IdentityAttributeDTO'
        required: true
      responses:
        '201':
          description: Created
  /mtls/identity-attribute:
    get:
      tags:
        - mtls-controller
      summary: Get Identity Attributes with Ownership
      description: Retrieves a list of identity attributes with ownership information for a given participant ID.
      operationId: getIdentityAttributesWithOwnership
      parameters:
        - name: Credential-Id
          in: header
          description: Participant Public Key Hash
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '200':
          description: Successfully retrieved identity attributes with ownership
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/IdentityAttributeWithOwnershipDTO'
  /mtls/identity-attribute/{credentialId}:
    get:
      tags:
        - mtls-controller
      summary: Get Identity Attributes by Certificate ID
      description: Retrieves a list of identity attributes associated with a given certificate ID.
      operationId: getIdentityAttributesByCertificateIdInUri
      parameters:
        - name: certificateId
          in: query
          description: Certificate ID used to fetch associated identity attributes
          required: true
          schema:
            type: string
        - name: credentialId
          in: path
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Successfully retrieved identity attributes
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/IdentityAttributeDTO'
        '404':
          description: Certificate ID not found
  /identity-attribute/search:
    get:
      tags:
        - Identity Attribute API
      summary: Search Identity Attributes
      description: Searches for identity attributes based on provided filters and pagination options.
      operationId: search
      parameters:
        - name: code
          in: query
          required: false
          schema:
            type: string
        - name: name
          in: query
          required: false
          schema:
            type: string
        - name: enabled
          in: query
          required: false
          schema:
            type: boolean
        - name: participantTypeIn
          in: query
          required: false
          schema:
            type: string
        - name: participantTypeNotIn
          in: query
          required: false
          schema:
            type: string
        - name: participantIdIn
          in: query
          required: false
          schema:
            type: string
            format: uuid
        - name: participantIdNotIn
          in: query
          required: false
          schema:
            type: string
            format: uuid
        - name: updateTimestampFrom
          in: query
          required: false
          schema:
            type: string
            format: date-time
        - name: updateTimestampTo
          in: query
          required: false
          schema:
            type: string
            format: date-time
        - name: page
          in: query
          description: Zero-based page index (0..N)
          required: false
          schema:
            minimum: 0
            type: integer
            default: 0
        - name: size
          in: query
          description: The size of the page to be returned
          required: false
          schema:
            minimum: 1
            type: integer
            default: 10
        - name: sort
          in: query
          description: 'Sorting criteria in the format: property,(asc|desc). Default sort order is ascending. Multiple sort criteria are supported.'
          required: false
          schema:
            type: array
            items:
              type: string
            default:
              - id,ASC
      responses:
        '200':
          description: Successfully retrieved search results
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/PageResponseIdentityAttributeDTO'
        '400':
          description: Invalid input data
  /identity-attribute/unassign-participant/{participantId}:
    delete:
      tags:
        - Identity Attribute API
      summary: Unassign Identity Attributes
      description: Unassigns a list of identity attributes from a user.
      operationId: unassign
      parameters:
        - name: participantId
          in: path
          description: UUID of the user from whom the identity attributes will be unassigned
          required: true
          schema:
            type: string
            format: uuid
      requestBody:
        description: List of UUIDs representing identity attributes to be unassigned
        content:
          application/json:
            schema:
              type: array
              items:
                type: string
                format: uuid
        required: true
      responses:
        '200':
          description: Successfully unassigned identity attributes
        '204':
          description: No Content
        '400':
          description: Invalid input data
        '401':
          description: Access denied
        '403':
          description: 'Forbidden: User does not have the required role'
components:
  schemas:
    ErrorDTO:
      type: object
      properties:
        error:
          type: string
        elementName:
          type: string
    IdentityAttributeDTO:
      required:
        - assignableToRoles
        - code
        - enabled
        - name
      type: object
      properties:
        id:
          type: string
          format: uuid
        code:
          type: string
        name:
          type: string
        description:
          type: string
        assignableToRoles:
          type: boolean
        enabled:
          type: boolean
        creationTimestamp:
          type: string
          format: date-time
        updateTimestamp:
          type: string
          format: date-time
        participantTypes:
          uniqueItems: true
          type: array
          items:
            type: string
        used:
          type: boolean
          readOnly: true
    IdentityAttributeWithOwnershipDTO:
      required:
        - assignableToRoles
        - code
        - enabled
        - name
      type: object
      properties:
        id:
          type: string
          format: uuid
        code:
          type: string
        name:
          type: string
        description:
          type: string
        assignableToRoles:
          type: boolean
        enabled:
          type: boolean
        creationTimestamp:
          type: string
          format: date-time
        updateTimestamp:
          type: string
          format: date-time
        participantTypes:
          uniqueItems: true
          type: array
          items:
            type: string
        used:
          type: boolean
          readOnly: true
        assignedToParticipant:
          type: boolean
    PageMetadata:
      type: object
      properties:
        size:
          type: integer
          format: int64
        number:
          type: integer
          format: int64
        totalElements:
          type: integer
          format: int64
        totalPages:
          type: integer
          format: int64
    PageResponseIdentityAttributeDTO:
      type: object
      properties:
        content:
          type: array
          items:
            $ref: '#/components/schemas/IdentityAttributeDTO'
        page:
          $ref: '#/components/schemas/PageMetadata'
        empty:
          type: boolean
