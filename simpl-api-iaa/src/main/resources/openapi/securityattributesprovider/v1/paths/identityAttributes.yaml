get:
  tags:
    - Identity Attributes
  summary: Search Identity Attributes
  description: Searches for identity attributes based on provided filters and pagination
    options.
  operationId: search
  parameters:
    - name: page
      in: query
      description: Zero-based page index (0..N)
      required: false
      style: form
      explode: true
      schema:
        type: integer
        default: 0
        minimum: 0
    - name: size
      in: query
      description: The size of the page to be returned
      required: false
      style: form
      explode: true
      schema:
        type: integer
        default: 10
        minimum: 1
    - name: sort
      in: query
      description: "Sorting criteria in the format: property,(asc|desc). Default sort\
      \ order is ascending. Multiple sort criteria are supported."
      required: false
      style: form
      explode: true
      schema:
        type: array
        default:
          - "id,ASC"
        items:
          type: string
    - name: filter
      x-parameter-object: true
      in: query
      schema:
        properties:
          code:
            type: string
          participantTypeIn:
            type: string
          participantIdNotIn:
            type: string
            format: uuid
          updateTimestampFrom:
            type: string
            format: date-time
          name:
            type: string
          participantIdIn:
            type: string
            format: uuid
          updateTimestampTo:
            type: string
            format: date-time
          enabled:
            type: boolean
          participantTypeNotIn:
            type: string
  responses:
    "200":
      description: Successfully retrieved search results
      content:
        'application/json':
          schema:
            $ref: ../schemas/PageResponseIdentityAttribute.yaml
    "400":
      description: Invalid input data
  security:
    - oauth2AuthCode: []
  x-simpl-roles:
    - T1UAR_M
    - NOTARY
    - IATTR_M
    - NOTARY
post:
  tags:
  - Identity Attributes
  summary: Create Identity Attribute
  description: Creates a new identity attribute.
  operationId: create
  requestBody:
    description: Identity attribute to be created
    content:
      application/json:
        schema:
          $ref: ../schemas/IdentityAttribute.yaml
    required: true
  responses:
    "201":
      description: Successfully created identity attribute
      content:
        application/json:
          schema:
            $ref: ../schemas/IdentityAttribute.yaml
    "400":
      description: Invalid input data
    "401":
      description: Access denied
    "403":
      description: "Forbidden: User does not have the required role"
    "409":
      description: "Conflict: Identity attribute with the same code or name already\
        \ exists"
  security:
  - oauth2AuthCode: []
  x-simpl-roles:
  - IATTR_M
