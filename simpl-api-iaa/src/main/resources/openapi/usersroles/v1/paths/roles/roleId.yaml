put:
  tags:
    - Roles
  summary: Update an existing role
  description: Updates the details of an existing role in the system
  operationId: update
  parameters:
    - name: roleId
      in: path
      description: The UUID of the role
      required: true
      style: simple
      explode: false
      schema:
        type: string
        format: uuid
      example: 123e4567-e89b-12d3-a456-426614174000
  requestBody:
    description: The details of the role to be updated
    content:
      application/json:
        schema:
          $ref: ../../schemas/Role.yaml
    required: true
  responses:
    "200":
      description: Successfully updated the role
      content:
        application/json:
          schema:
            $ref: ../../schemas/Role.yaml
    "400":
      description: Invalid input data
    "401":
      description: Access denied
    "403":
      description: "Forbidden: Cannot modify role's name or User does not have the\
        \ required role"
    "404":
      description: Role not found
    "409":
      description: Conflict
  security:
    - oauth2AuthCode: []
  x-simpl-roles:
    - T1UAR_M
get:
  tags:
    - Roles
  summary: Find role by ID
  description: Retrieves a role by its unique identifier (UUID)
  operationId: findById
  parameters:
    - name: roleId
      in: path
      description: The UUID of the role
      required: true
      style: simple
      explode: false
      schema:
        type: string
        format: uuid
      example: 123e4567-e89b-12d3-a456-426614174000
  responses:
    "200":
      description: Successfully retrieved the role
      content:
        application/json:
          schema:
            $ref: ../../schemas/Role.yaml
    "400":
      description: Invalid UUID format
    "401":
      description: Access denied
    "403":
      description: "Forbidden: User does not have the required role"
    "404":
      description: Role not found
  security:
    - oauth2AuthCode: []
  x-simpl-roles:
    - T1UAR_M
delete:
  tags:
  - Roles
  summary: Delete a role by id
  description: Removes a role from the system using its id
  operationId: deleteCredential
  parameters:
  - name: roleId
    in: path
    description: The name of the role to be deleted
    required: true
    style: simple
    explode: false
    schema:
      type: string
      format: uuid
  responses:
    "204":
      description: Successfully deleted the role
    "400":
      description: Invalid role name
    "401":
      description: Access denied
    "403":
      description: "Forbidden: User does not have the required role"
    "404":
      description: Role not found
  security:
  - oauth2AuthCode: []
  x-simpl-roles:
  - T1UAR_M
