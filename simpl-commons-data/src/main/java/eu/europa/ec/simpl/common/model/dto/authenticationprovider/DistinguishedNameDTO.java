package eu.europa.ec.simpl.common.model.dto.authenticationprovider;

import jakarta.validation.constraints.NotBlank;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

@Data
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DistinguishedNameDTO {
    @NotBlank
    String commonName;

    @NotBlank
    String organization;

    @NotBlank
    String organizationalUnit;

    @NotBlank
    String country;
}
