package eu.europa.ec.simpl.common.model.dto.authenticationprovider;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import eu.europa.ec.simpl.common.model.dto.identityprovider.ParticipantWithIdentityAttributesDTO;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class EchoDTO {

    String username;
    String email;

    ConnectionStatus connectionStatus;
    MTLSStatus mtlsStatus;

    List<String> userIdentityAttributes = new ArrayList<>();

    @JsonUnwrapped
    ParticipantWithIdentityAttributesDTO participant;

    public enum ConnectionStatus {
        CONNECTED,
        NOT_CONNECTED
    }

    public enum MTLSStatus {
        SECURED,
        NOT_SECURED
    }
}
