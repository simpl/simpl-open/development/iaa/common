package eu.europa.ec.simpl.common.model.dto.authenticationprovider;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class ImportKeyPairDTO {
    @NotBlank
    String publicKey;

    @NotBlank
    String privateKey;
}
