package eu.europa.ec.simpl.common.model.dto.authenticationprovider;

import lombok.*;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@AllArgsConstructor
public class KeyPairDTO {
    private byte[] publicKey;
    private byte[] privateKey;
}
