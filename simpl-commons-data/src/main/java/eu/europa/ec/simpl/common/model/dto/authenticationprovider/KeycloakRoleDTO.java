package eu.europa.ec.simpl.common.model.dto.authenticationprovider;

import jakarta.validation.constraints.NotBlank;

public record KeycloakRoleDTO(@NotBlank String name, String description) {}
