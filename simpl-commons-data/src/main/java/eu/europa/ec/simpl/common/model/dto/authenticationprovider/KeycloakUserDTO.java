package eu.europa.ec.simpl.common.model.dto.authenticationprovider;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.europa.ec.simpl.validation.annotations.RfcEmail;
import jakarta.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class KeycloakUserDTO {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String id;

    @NotBlank
    private String username;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @RfcEmail
    @NotBlank
    private String email;

    @ToString.Exclude
    @NotBlank
    private String password;

    private List<String> roles = new ArrayList<>();
}
