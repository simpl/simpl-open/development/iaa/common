package eu.europa.ec.simpl.common.model.dto.authenticationprovider;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TierOneSessionDTO {
    @NotBlank
    private String jwt;
}
