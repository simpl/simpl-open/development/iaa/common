package eu.europa.ec.simpl.common.model.dto.identityprovider;

import java.time.Instant;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CertificateDTO {
    private String publicKey;
    private Instant expiryDate;
}
