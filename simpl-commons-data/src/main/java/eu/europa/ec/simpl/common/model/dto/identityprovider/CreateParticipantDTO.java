package eu.europa.ec.simpl.common.model.dto.identityprovider;

import java.util.List;
import java.util.UUID;

public record CreateParticipantDTO(ParticipantDTO participantDTO, List<UUID> identityAttributes) {
    public CreateParticipantDTO {
        identityAttributes = List.copyOf(identityAttributes);
    }
}
