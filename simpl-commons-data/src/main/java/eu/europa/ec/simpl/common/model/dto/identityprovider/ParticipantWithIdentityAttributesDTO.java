package eu.europa.ec.simpl.common.model.dto.identityprovider;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import jakarta.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ParticipantWithIdentityAttributesDTO {
    @Valid
    @JsonUnwrapped
    private ParticipantDTO participant;

    private List<IdentityAttributeDTO> identityAttributes = new ArrayList<>();
}
