package eu.europa.ec.simpl.common.model.dto.onboarding;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ApproveDTO {
    List<UUID> identityAttributes = new ArrayList<>();
}
