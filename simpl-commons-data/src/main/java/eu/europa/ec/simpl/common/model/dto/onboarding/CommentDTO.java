package eu.europa.ec.simpl.common.model.dto.onboarding;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import java.time.Instant;
import java.util.UUID;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CommentDTO {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    UUID id;

    @NotBlank
    String author;

    @NotBlank
    @Size(max = 5000)
    String content;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    Instant creationTimestamp;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    Instant updateTimestamp;
}
