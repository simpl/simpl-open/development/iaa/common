package eu.europa.ec.simpl.common.model.dto.onboarding;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.europa.ec.simpl.common.model.interfaces.DocumentModel;
import java.time.Instant;
import java.util.UUID;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
// @ValidDocumentRequest(groups = RequestAdditionalDocument) // TODO Validation
public class DocumentDTO implements DocumentModel {

    //    @NotNull(groups = UploadDocument.class)
    private UUID id;

    //    @Null(groups = UploadDocument.class)
    private DocumentTemplateDTO documentTemplate;

    //    @Null(groups = UploadDocument.class)
    private MimeTypeDTO mimeType;

    private String description;

    //    @NotNull(groups = UploadDocument.class)
    private String filename;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long filesize;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Instant creationTimestamp;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Instant updateTimestamp;

    //    @NotNull(groups = UploadDocument.class)
    private String content;

    @Override
    public Boolean getHasContent() {
        return filesize != null && filesize > 0;
    }

    public Boolean getMandatory() {
        return getDocumentTemplate() == null || getDocumentTemplate().isMandatory();
    }
}
