package eu.europa.ec.simpl.common.model.dto.onboarding;

import jakarta.validation.constraints.NotBlank;
import java.util.UUID;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class MimeTypeDTO {

    private UUID id;

    @NotBlank
    private String value;

    @NotBlank
    private String description;
}
