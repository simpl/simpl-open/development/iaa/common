package eu.europa.ec.simpl.common.model.dto.onboarding;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.europa.ec.simpl.validation.annotations.RfcEmail;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class OnboardingApplicantDTO {

    @NotBlank
    private String username;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @RfcEmail
    @NotBlank
    private String email;

    @ToString.Exclude
    @NotBlank
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
}
