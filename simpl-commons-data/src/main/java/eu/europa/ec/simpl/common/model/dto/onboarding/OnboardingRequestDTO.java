package eu.europa.ec.simpl.common.model.dto.onboarding;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import java.time.Instant;
import java.util.*;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class OnboardingRequestDTO {
    private UUID id;
    private UUID participantId;
    private OnboardingStatusDTO status;

    @NotNull private OnboardingApplicantDTO applicant;

    @NotNull private ParticipantTypeDTO participantType;

    @NotNull private String organization;

    private List<DocumentDTO> documents;
    private List<CommentDTO> comments;
    private String rejectionCause;
    private Long expirationTimeframe;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Instant creationTimestamp;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Instant updateTimestamp;

    private Instant lastStatusUpdateTimestamp;

    public List<CommentDTO> getComments() {
        return Optional.ofNullable(comments).orElse(List.of()).stream()
                .sorted(Comparator.comparing(CommentDTO::getCreationTimestamp).reversed())
                .toList();
    }
}
