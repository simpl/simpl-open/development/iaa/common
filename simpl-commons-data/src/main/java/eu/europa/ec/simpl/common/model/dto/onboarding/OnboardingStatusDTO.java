package eu.europa.ec.simpl.common.model.dto.onboarding;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import java.util.UUID;
import lombok.Data;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Data
public class OnboardingStatusDTO {

    private UUID id;

    @NotBlank
    @Size(max = 255)
    private String label;

    @NotBlank
    @Size(max = 255)
    private OnboardingStatusValue value;
}
