package eu.europa.ec.simpl.common.model.dto.onboarding;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class OnboardingTemplateDTO {

    private UUID id;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private ParticipantTypeDTO participantType;

    @NotBlank
    private String description;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Instant creationTimestamp;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Instant updateTimestamp;

    @Valid
    private List<DocumentTemplateDTO> documentTemplates = new ArrayList<>();

    @NotNull private Long expirationTimeframe;
}
