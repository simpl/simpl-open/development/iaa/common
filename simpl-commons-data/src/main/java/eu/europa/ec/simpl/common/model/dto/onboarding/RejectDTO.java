package eu.europa.ec.simpl.common.model.dto.onboarding;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class RejectDTO {
    @NotNull String rejectionCause;
}
