package eu.europa.ec.simpl.common.model.dto.securityattributesprovider;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import jakarta.validation.Valid;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class IdentityAttributeWithOwnershipDTO {

    @Valid
    @JsonUnwrapped
    public IdentityAttributeDTO identityAttribute;

    Boolean assignedToParticipant;
}
