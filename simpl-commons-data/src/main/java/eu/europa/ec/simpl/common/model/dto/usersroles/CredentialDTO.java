package eu.europa.ec.simpl.common.model.dto.usersroles;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CredentialDTO {
    private String publicKey;
}
