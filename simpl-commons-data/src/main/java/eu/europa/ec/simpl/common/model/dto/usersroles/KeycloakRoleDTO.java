package eu.europa.ec.simpl.common.model.dto.usersroles;

import jakarta.validation.constraints.NotBlank;

public record KeycloakRoleDTO(@NotBlank String name, String description) {}
