package eu.europa.ec.simpl.common.model.interfaces;

public interface DocumentModel {

    Long getFilesize();

    default Boolean getHasContent() {
        return getFilesize() != null && getFilesize() > 0;
    }
}
