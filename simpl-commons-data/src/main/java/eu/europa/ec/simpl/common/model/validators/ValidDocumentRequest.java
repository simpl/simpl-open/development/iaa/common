package eu.europa.ec.simpl.common.model.validators;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = {ValidDocumentRequestValidator.class})
public @interface ValidDocumentRequest {

    String message() default "{eu.europa.ec.simpl.common.model.validators.ValidDocumentRequest.message}";

    Class<?>[] groups() default {}; // TODO RequestAdditionalDocument

    Class<? extends Payload>[] payload() default {};
}
