package eu.europa.ec.simpl.common.model.validators;

import eu.europa.ec.simpl.common.model.dto.onboarding.DocumentDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.DocumentTemplateDTO;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.util.Optional;

public class ValidDocumentRequestValidator implements ConstraintValidator<ValidDocumentRequest, DocumentDTO> {

    @Override
    public boolean isValid(DocumentDTO document, ConstraintValidatorContext context) {
        return isTemplateSpecified(document) || hasExplicitMimeTypeAndDescription(document);
    }

    private boolean hasExplicitMimeTypeAndDescription(DocumentDTO document) {
        return document.getMimeType() != null && document.getDescription() != null;
    }

    private static boolean isTemplateSpecified(DocumentDTO document) {
        return Optional.ofNullable(document.getDocumentTemplate())
                .map(DocumentTemplateDTO::getId)
                .isPresent();
    }
}
