package eu.europa.ec.simpl.common.argumentresolvers;

import eu.europa.ec.simpl.common.exceptions.RuntimeWrapperException;
import java.lang.reflect.Field;
import java.util.Objects;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import lombok.extern.log4j.Log4j2;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.BeanUtils;
import org.springframework.core.MethodParameter;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.service.invoker.HttpRequestValues;
import org.springframework.web.service.invoker.HttpServiceArgumentResolver;

@Log4j2
public class QueryParamsArgumentResolver implements HttpServiceArgumentResolver {

    @Override
    public boolean resolve(Object argument, MethodParameter parameter, HttpRequestValues.Builder requestValues) {
        Assert.notNull(parameter, "MethodParameter cannot be null");
        if (!isQueryParam(parameter)) {
            return false;
        }

        if (argument instanceof Iterable<?> iterable) {

            var values = StreamSupport.stream(iterable.spliterator(), false)
                    .filter(Objects::nonNull)
                    .map(Object::toString)
                    .toList();

            if (!CollectionUtils.isEmpty(values)) {
                requestValues.addRequestParameter(parameter.getParameterName(), values.toArray(String[]::new));
            }

            return true;
        }

        if (hasNestedObjects(argument)) {
            return false;
        }

        Stream.of(argument.getClass().getDeclaredFields())
                .filter(field -> isNotNull(field, argument))
                .forEach(field ->
                        requestValues.addRequestParameter(field.getName(), String.valueOf(getValue(field, argument))));
        return true;
    }

    private boolean isQueryParam(MethodParameter parameter) {
        return parameter.hasParameterAnnotation(ModelAttribute.class)
                || parameter.hasParameterAnnotation(ParameterObject.class);
    }

    private boolean hasNestedObjects(Object argument) {
        return Stream.of(argument.getClass().getDeclaredFields())
                .map(field -> getValue(field, argument))
                .filter(Objects::nonNull)
                .anyMatch(obj -> !BeanUtils.isSimpleProperty(obj.getClass()));
    }

    private boolean isNotNull(Field field, Object argument) {
        var obj = getValue(field, argument);
        if (obj == null) {
            return false;
        }
        if (obj instanceof String s) {
            return StringUtils.hasText(s);
        }

        return true;
    }

    @SuppressWarnings("all")
    private Object getValue(Field field, Object argument) {
        try {
            field.setAccessible(true);
            var obj = field.get(argument);
            field.setAccessible(false);
            return obj;
        } catch (IllegalAccessException e) {
            field.setAccessible(false);
            throw new RuntimeWrapperException(e);
        } finally {
            field.setAccessible(false);
        }
    }
}
