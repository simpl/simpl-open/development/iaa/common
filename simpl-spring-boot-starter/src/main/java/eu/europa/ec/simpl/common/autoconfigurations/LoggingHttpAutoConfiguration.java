package eu.europa.ec.simpl.common.autoconfigurations;

import static eu.simpl.MessageBuilder.buildMessage;

import eu.simpl.types.HttpLogMessage;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.web.exchanges.HttpExchange;
import org.springframework.boot.actuate.web.exchanges.HttpExchangeRepository;
import org.springframework.boot.actuate.web.exchanges.InMemoryHttpExchangeRepository;
import org.springframework.boot.actuate.web.exchanges.Include;
import org.springframework.boot.actuate.web.exchanges.servlet.HttpExchangesFilter;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;

@Log4j2
@AutoConfiguration
@ConditionalOnClass(HttpExchangesFilter.class)
@ConditionalOnProperty(prefix = "logging.http", name = "enabled", havingValue = "true")
public class LoggingHttpAutoConfiguration {

    @Bean
    public HttpExchangesFilter httpExchangesFilter(HttpExchangeRepository httpExchangeRepository) {
        return new HttpExchangesFilter(
                httpExchangeRepository,
                Stream.concat(Include.defaultIncludes().stream(), Set.of(Include.PRINCIPAL).stream())
                        .collect(Collectors.toSet()));
    }

    @Bean
    public HttpExchangeRepository loggingHttpExchangeRepository(
            @Value("${logging.service.level:INFO}") Level serviceLoggingLevel) {
        return new LoggingHttpExchangeRepository(serviceLoggingLevel);
    }

    public static class LoggingHttpExchangeRepository extends InMemoryHttpExchangeRepository {

        private final Level level;

        public LoggingHttpExchangeRepository(Level level) {
            this.level = level;
        }

        @Override
        public void add(HttpExchange exchange) {

            log.atLevel(level)
                    .log(buildMessage(HttpLogMessage.builder()
                            .msg("%s %s"
                                    .formatted(
                                            exchange.getRequest().getMethod(),
                                            exchange.getRequest().getUri().getPath()))
                            .httpStatus(Objects.toString(exchange.getResponse().getStatus()))
                            .httpExecutionTime(
                                    Objects.toString(exchange.getTimeTaken().toMillis()))
                            .user(Optional.ofNullable(exchange.getPrincipal())
                                    .map(HttpExchange.Principal::getName)
                                    .orElse(null))
                            .build()));

            super.add(exchange);
        }
    }
}
