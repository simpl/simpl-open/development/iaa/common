package eu.europa.ec.simpl.common.constants;

public class SimplHeaders {

    private SimplHeaders() {
        throw new IllegalStateException("Constants class");
    }

    public static final String CREDENTIAL_ID = "Credential-Id";
    public static final String USER_ATTRIBUTES = "User-Attributes";
}
