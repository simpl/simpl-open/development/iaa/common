package eu.europa.ec.simpl.common.csr;

import eu.europa.ec.simpl.common.exceptions.CSRException;
import eu.europa.ec.simpl.common.exceptions.RuntimeWrapperException;
import eu.europa.ec.simpl.common.model.dto.authenticationprovider.DistinguishedNameDTO;
import eu.europa.ec.simpl.common.model.dto.authenticationprovider.KeyPairDTO;
import java.io.IOException;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;

@Getter
@FieldDefaults(makeFinal = true, level = AccessLevel.PROTECTED)
public abstract class AbstractCertificateSignRequest implements CertificateSignRequest {

    DistinguishedNameDTO dn;
    X500Name subject;
    PrivateKey privateKey;
    PublicKey publicKey;
    String dnsName;
    PKCS10CertificationRequest csr;
    AlgorithmConfig config;

    protected AbstractCertificateSignRequest(
            DistinguishedNameDTO dn, KeyPairDTO keyPair, String dnsName, AlgorithmConfig config) {
        this.dn = dn;
        this.config = config;
        this.publicKey = parsePublicKey(keyPair);
        this.privateKey = parsePrivateKey(keyPair);
        this.subject = new X500Name(buildDn());
        this.dnsName = dnsName;
        this.csr = generateCSR();
    }

    protected abstract JcaContentSignerBuilder getContentSignerBuilder();

    @Override
    public String getRawCsr() {
        try {
            return "-----BEGIN CERTIFICATE REQUEST-----\n" + Base64.getEncoder().encodeToString(this.csr.getEncoded())
                    + "\n-----END CERTIFICATE REQUEST-----\n";
        } catch (IOException e) {
            throw new RuntimeWrapperException(e);
        }
    }

    protected String buildDn() {
        X500NameBuilder builder = new X500NameBuilder(BCStyle.INSTANCE);

        if (StringUtils.isNotBlank(dn.getCommonName())) {
            builder.addRDN(BCStyle.CN, dn.getCommonName());
        }
        if (StringUtils.isNotBlank(dn.getOrganization())) {
            builder.addRDN(BCStyle.O, dn.getOrganization());
        }
        if (StringUtils.isNotBlank(dn.getOrganizationalUnit())) {
            builder.addRDN(BCStyle.OU, dn.getOrganizationalUnit());
        }
        if (StringUtils.isNotBlank(dn.getCountry())) {
            builder.addRDN(BCStyle.C, dn.getCountry());
        }

        return builder.build().toString();
    }

    protected PKCS10CertificationRequest generateCSR() {
        try {
            var signer = getContentSignerBuilder().build(privateKey);

            var csrBuilder = new PKCS10CertificationRequestBuilder(
                    subject, SubjectPublicKeyInfo.getInstance(publicKey.getEncoded()));

            if (StringUtils.isNotBlank(this.dnsName)) {
                csrBuilder.addAttribute(PKCSObjectIdentifiers.pkcs_9_at_extensionRequest, buildSAN());
            }

            return csrBuilder.build(signer);
        } catch (Exception e) {
            throw new CSRException("Error during CSR initialization", e);
        }
    }

    private Extensions buildSAN() {
        var subjectAltName = new GeneralNames(new GeneralName(GeneralName.dNSName, this.dnsName));
        var extensionsGenerator = new ExtensionsGenerator();
        try {
            extensionsGenerator.addExtension(Extension.subjectAlternativeName, false, subjectAltName);
        } catch (IOException e) {
            throw new CSRException("Error during CSR initialization", e);
        }
        return extensionsGenerator.generate();
    }

    @SneakyThrows
    private PrivateKey parsePrivateKey(KeyPairDTO keyPair) {
        var kf = KeyFactory.getInstance(config.algorithm());
        return kf.generatePrivate(new PKCS8EncodedKeySpec(keyPair.getPrivateKey()));
    }

    @SneakyThrows
    private PublicKey parsePublicKey(KeyPairDTO keyPair) {
        var kf = KeyFactory.getInstance(config.algorithm());
        return kf.generatePublic(new X509EncodedKeySpec(keyPair.getPublicKey()));
    }
}
