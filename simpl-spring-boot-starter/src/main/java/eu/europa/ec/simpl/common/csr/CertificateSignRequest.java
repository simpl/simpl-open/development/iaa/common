package eu.europa.ec.simpl.common.csr;

public interface CertificateSignRequest {
    String getRawCsr();
}
