package eu.europa.ec.simpl.common.csr;

import eu.europa.ec.simpl.common.model.dto.authenticationprovider.DistinguishedNameDTO;
import eu.europa.ec.simpl.common.model.dto.authenticationprovider.KeyPairDTO;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

@Getter
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class EllipticCertificateSignRequest extends AbstractCertificateSignRequest {

    public EllipticCertificateSignRequest(
            DistinguishedNameDTO dn, KeyPairDTO keyPair, String dnsName, AlgorithmConfig config) {
        super(dn, keyPair, dnsName, config);
    }

    @Override
    protected JcaContentSignerBuilder getContentSignerBuilder() {
        return new JcaContentSignerBuilder(config.signatureAlgorithm());
    }
}
