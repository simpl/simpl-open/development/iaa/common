package eu.europa.ec.simpl.common.entity.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import org.hibernate.annotations.IdGeneratorType;

@Retention(RUNTIME)
@Target({FIELD, METHOD})
@IdGeneratorType(eu.europa.ec.simpl.common.entity.generators.UUIDv7Generator.class)
public @interface UUIDv7Generator {}
