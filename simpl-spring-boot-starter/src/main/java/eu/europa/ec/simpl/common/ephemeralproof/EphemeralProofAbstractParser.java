package eu.europa.ec.simpl.common.ephemeralproof;

import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import lombok.Getter;

@Getter
public abstract class EphemeralProofAbstractParser<T> {

    protected final T ephemeralProof;

    public abstract String getRaw();

    public abstract UUID getSubject();

    public abstract List<IdentityAttributeDTO> getIdentityAttributes();

    public abstract List<String> getPublicKeys();

    public abstract Instant getExpiration();

    protected EphemeralProofAbstractParser(T ephemeralProof) {
        this.ephemeralProof = ephemeralProof;
    }
}
