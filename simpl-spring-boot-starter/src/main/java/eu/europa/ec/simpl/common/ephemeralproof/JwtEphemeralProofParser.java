package eu.europa.ec.simpl.common.ephemeralproof;

import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import eu.europa.ec.simpl.common.exceptions.InvalidEphemeralProofException;
import eu.europa.ec.simpl.common.exceptions.RuntimeWrapperException;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.utils.JwtUtil;
import java.text.ParseException;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class JwtEphemeralProofParser extends EphemeralProofAbstractParser<SignedJWT> {

    public JwtEphemeralProofParser(String ep) {
        super(initEphemeralProof(ep));
    }

    private static SignedJWT initEphemeralProof(String ep) {
        try {
            return SignedJWT.parse(ep);
        } catch (ParseException e) {
            throw new InvalidEphemeralProofException();
        }
    }

    @Override
    public String getRaw() {
        return ephemeralProof.getParsedString();
    }

    @Override
    public UUID getSubject() {
        return UUID.fromString(getClaimsSet().getSubject());
    }

    @Override
    public List<IdentityAttributeDTO> getIdentityAttributes() {
        return JwtUtil.getListClaim(ephemeralProof, "attributes", IdentityAttributeDTO.class);
    }

    @Override
    public List<String> getPublicKeys() {
        try {
            return getClaimsSet().getListClaim("publicKeys").stream()
                    .map(Object::toString)
                    .toList();
        } catch (ParseException e) {
            throw new RuntimeWrapperException(e);
        }
    }

    @Override
    public Instant getExpiration() {
        return getClaimsSet().getExpirationTime().toInstant();
    }

    public JWTClaimsSet getClaimsSet() {
        try {
            return ephemeralProof.getJWTClaimsSet();
        } catch (ParseException e) {
            log.error("Unable to parse ephemeral proof", e);
            throw new InvalidEphemeralProofException();
        }
    }
}
