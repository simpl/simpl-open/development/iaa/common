package eu.europa.ec.simpl.common.exceptions;

import org.springframework.http.HttpStatus;

public class EphemeralProofNotFoundException extends StatusException {
    public EphemeralProofNotFoundException() {
        this(HttpStatus.UNAUTHORIZED, "Ephemeral Proof not found");
    }

    protected EphemeralProofNotFoundException(HttpStatus status, String message) {
        super(status, message);
    }
}
