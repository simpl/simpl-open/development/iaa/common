package eu.europa.ec.simpl.common.exceptions;

import org.springframework.http.HttpStatus;

public class InvalidTierOneSessionException extends StatusException {
    public InvalidTierOneSessionException() {
        this(HttpStatus.UNAUTHORIZED, "Invalid Tier One session");
    }

    protected InvalidTierOneSessionException(HttpStatus status, String message) {
        super(status, message);
    }
}
