package eu.europa.ec.simpl.common.exceptions;

import org.springframework.http.HttpStatus;

public class JwtNotFoundException extends StatusException {
    public JwtNotFoundException() {
        super(HttpStatus.BAD_REQUEST, "Authorization header is missing");
    }
}
