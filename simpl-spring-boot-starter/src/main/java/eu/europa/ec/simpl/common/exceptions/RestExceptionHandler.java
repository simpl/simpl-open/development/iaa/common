package eu.europa.ec.simpl.common.exceptions;

import static eu.europa.ec.simpl.common.exceptions.SimplErrors.VALIDATION_ERROR_TYPE;

import jakarta.validation.ConstraintViolationException;
import java.net.ConnectException;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;
import lombok.extern.log4j.Log4j2;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.HandlerMethodValidationException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Log4j2
@RestControllerAdvice
@Component
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    public static final String SQL_UNIQUE_KEY_ERROR = "23505";

    public static final String VALIDATION_ERRORS_PROPERTY_NAME = "errors";

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(value = DataIntegrityViolationException.class)
    private ProblemDetail handleDataIntegrityViolationException(DataIntegrityViolationException e) {
        logError(e);
        if (e.getCause() instanceof org.hibernate.exception.ConstraintViolationException ex
                && Objects.equals(ex.getSQLException().getSQLState(), SQL_UNIQUE_KEY_ERROR)) {
            return buildUniqueConstraintProblemDetail(ex);
        }
        return ProblemDetail.forStatusAndDetail(HttpStatus.CONFLICT, e.getMessage());
    }

    private ProblemDetail buildUniqueConstraintProblemDetail(org.hibernate.exception.ConstraintViolationException ex) {
        var matcher = Pattern.compile("\\(([^)]+)\\)=?\\(([^)]+)\\)").matcher(ex.getMessage());
        var found = matcher.find();
        return new UniqueConstraintProblemDetail(
                found ? matcher.group(1) : null, found ? matcher.group(2) : null, ex.getConstraintName());
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        ex.getBody().setType(VALIDATION_ERROR_TYPE);
        ex.getBody()
                .setProperty(
                        VALIDATION_ERRORS_PROPERTY_NAME,
                        ex.getFieldErrors().stream().map(ValidationError::of));
        return super.handleMethodArgumentNotValid(ex, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleHandlerMethodValidationException(
            HandlerMethodValidationException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        ex.getBody().setType(VALIDATION_ERROR_TYPE);
        ex.getBody()
                .setProperty(
                        VALIDATION_ERRORS_PROPERTY_NAME,
                        ex.getParameterValidationResults().stream().map(ValidationError::of));
        return super.handleHandlerMethodValidationException(ex, headers, status, request);
    }

    @ExceptionHandler(value = ConstraintViolationException.class)
    private ResponseEntity<ProblemDetail> handleConstraintViolationException(ConstraintViolationException ex) {
        var status = HttpStatus.BAD_REQUEST;
        var problem = ProblemDetail.forStatusAndDetail(status, "Invalid request content.");
        problem.setType(VALIDATION_ERROR_TYPE);
        problem.setProperty(
                VALIDATION_ERRORS_PROPERTY_NAME,
                ex.getConstraintViolations().stream().map(ValidationError::of));
        return ResponseEntity.status(status).body(problem);
    }

    @ExceptionHandler(value = ConnectException.class)
    protected ResponseEntity<Object> handleConnectException(ConnectException connectException) {
        logError(connectException);
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE)
                .body(ProblemDetail.forStatusAndDetail(HttpStatus.SERVICE_UNAVAILABLE, connectException.getMessage()));
    }

    @ExceptionHandler(value = HttpStatusCodeException.class)
    public ResponseEntity<ProblemDetail> handleClientException(HttpStatusCodeException e) {
        log.error("Client exception reported: ", e);
        var cause = tryParseProblem(e).orElse(ProblemDetail.forStatusAndDetail(e.getStatusCode(), e.getMessage()));
        return ResponseEntity.status(e.getStatusCode()).body(new ClientProblemDetail(cause));
    }

    private Optional<ProblemDetail> tryParseProblem(HttpStatusCodeException httpException) {
        try {
            return Optional.ofNullable(httpException.getResponseBodyAs(ProblemDetail.class));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    private void logError(Exception e) {
        log.error("Exception thrown! ", e);
    }
}
