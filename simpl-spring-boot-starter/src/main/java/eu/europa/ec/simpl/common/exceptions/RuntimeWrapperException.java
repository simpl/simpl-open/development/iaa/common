package eu.europa.ec.simpl.common.exceptions;

public class RuntimeWrapperException extends RuntimeException {

    public RuntimeWrapperException(Exception e) {
        super(e);
    }
}
