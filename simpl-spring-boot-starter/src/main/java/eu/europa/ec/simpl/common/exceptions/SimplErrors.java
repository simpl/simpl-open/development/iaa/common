package eu.europa.ec.simpl.common.exceptions;

import java.net.URI;
import lombok.experimental.UtilityClass;

@UtilityClass
public class SimplErrors {

    public static final String ERROR_TYPE_PREFIX = "https://simpl.ec.europa.eu/errors/";

    public static final URI UNIQUE_CONSTRAINT_ERROR_TYPE = URI.create(ERROR_TYPE_PREFIX + "uniqueConstraintError");

    public static final URI VALIDATION_ERROR_TYPE = URI.create(ERROR_TYPE_PREFIX + "validationError");

    public static final URI CLIENT_ERROR_TYPE = URI.create(ERROR_TYPE_PREFIX + "clientError");
}
