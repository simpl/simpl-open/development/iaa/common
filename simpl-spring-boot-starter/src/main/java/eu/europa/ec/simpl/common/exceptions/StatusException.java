package eu.europa.ec.simpl.common.exceptions;

import jakarta.annotation.Nullable;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ProblemDetail;
import org.springframework.web.ErrorResponseException;

public abstract class StatusException extends ErrorResponseException {
    private final HttpStatus status;

    protected StatusException(String message) {
        this(null, message);
    }

    protected StatusException(@Nullable HttpStatus status, String message) {
        this(status, message, null);
    }

    protected StatusException(@Nullable HttpStatus status, String message, Exception cause) {
        this(status, asProblemDetail(status, message), cause);
    }

    protected StatusException(@Nullable HttpStatus status, ProblemDetail problemDetail) {
        this(status, problemDetail, null);
    }

    protected StatusException(@Nullable HttpStatus status, ProblemDetail problemDetail, Exception cause) {
        super(
                HttpStatusCode.valueOf((status == null ? HttpStatus.INTERNAL_SERVER_ERROR : status).value()),
                problemDetail,
                cause);
        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }

    private static ProblemDetail asProblemDetail(HttpStatus status, String message) {
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(status, message);
        problemDetail.setTitle(message);
        return problemDetail;
    }
}
