package eu.europa.ec.simpl.common.exceptions;

import org.springframework.http.HttpStatus;

public class TierOneTokenNotFound extends StatusException {
    public TierOneTokenNotFound() {
        this(HttpStatus.UNAUTHORIZED, "Tier One token not found");
    }

    protected TierOneTokenNotFound(HttpStatus status, String message) {
        super(status, message);
    }
}
