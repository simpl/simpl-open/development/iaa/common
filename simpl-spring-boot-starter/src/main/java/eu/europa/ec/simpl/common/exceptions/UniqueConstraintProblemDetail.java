package eu.europa.ec.simpl.common.exceptions;

import static eu.europa.ec.simpl.common.exceptions.SimplErrors.UNIQUE_CONSTRAINT_ERROR_TYPE;

import java.net.URI;
import java.util.Optional;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;

public class UniqueConstraintProblemDetail extends ProblemDetail {

    private final String field;
    private final String value;
    private final String constraintName;

    public UniqueConstraintProblemDetail(String field, String value, String constraintName) {
        this.field = field;
        this.value = value;
        this.constraintName = constraintName;
    }

    public String getField() {
        return field;
    }

    public String getValue() {
        return value;
    }

    public String getConstraintName() {
        return constraintName;
    }

    @Override
    public URI getType() {
        return UNIQUE_CONSTRAINT_ERROR_TYPE;
    }

    @Override
    public String getTitle() {
        return "Unique constraint violated";
    }

    @Override
    public String getDetail() {
        return "Unique constraint %sviolated%s%s"
                .formatted(getConstraintNameMessage(), getFieldMessage(), getValueMessage());
    }

    @Override
    public int getStatus() {
        return HttpStatus.CONFLICT.value();
    }

    private String getConstraintNameMessage() {
        return Optional.ofNullable(constraintName).map("[ %s ] "::formatted).orElse("");
    }

    private String getFieldMessage() {
        return Optional.ofNullable(field).map(" on field [ %s ]"::formatted).orElse("");
    }

    private Object getValueMessage() {
        return Optional.ofNullable(value)
                .map(", value [ %s ] already present"::formatted)
                .orElse("");
    }
}
