package eu.europa.ec.simpl.common.exceptions;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ElementKind;
import jakarta.validation.Path;
import java.util.stream.Collectors;
import lombok.Value;
import org.apache.commons.lang3.stream.Streams;
import org.springframework.validation.FieldError;
import org.springframework.validation.method.ParameterValidationResult;

@Value
public class ValidationError {

    String key;

    @JsonInclude
    Object value;

    String reason;

    Type type;

    static ValidationError of(ParameterValidationResult error) {
        return new ValidationError(
                error.getMethodParameter().getParameterName(),
                error.getArgument(),
                error.getResolvableErrors().getFirst().getDefaultMessage(),
                Type.PARAMETER);
    }

    static ValidationError of(ConstraintViolation<?> error) {
        return new ValidationError(
                getFieldPath(error.getPropertyPath()), error.getInvalidValue(), error.getMessage(), Type.FIELD);
    }

    static ValidationError of(FieldError error) {
        return new ValidationError(error.getField(), error.getRejectedValue(), error.getDefaultMessage(), Type.FIELD);
    }

    public String getMessage() {
        return "Error on %s [ %s ], rejected value [ %s ] with reason [ %s ]"
                .formatted(type.getText(), key, value, reason);
    }

    private static String getFieldPath(Path propertyPath) {
        return Streams.of(propertyPath)
                .filter(node -> node.getKind() == ElementKind.PROPERTY)
                .map(Path.Node::getName)
                .collect(Collectors.joining("."));
    }

    enum Type {
        FIELD("field"),
        PARAMETER("parameter");

        private final String text;

        Type(String text) {
            this.text = text;
        }

        public String getText() {
            return text;
        }
    }
}
