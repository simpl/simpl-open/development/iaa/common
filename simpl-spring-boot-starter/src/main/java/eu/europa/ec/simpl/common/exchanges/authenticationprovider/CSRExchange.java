package eu.europa.ec.simpl.common.exchanges.authenticationprovider;

import eu.europa.ec.simpl.common.model.dto.authenticationprovider.DistinguishedNameDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.io.File;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PostExchange;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

@Tag(name = "Certificate Sign Request API", description = "API for managing CSR")
@HttpExchange("csr")
public interface CSRExchange {
    @PostExchange("generate")
    @Operation(
            summary = "Generate CSR",
            description = "Generate the CSR for the agent",
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "CSR generated successfully",
                        content = {@Content(schema = @Schema(implementation = File.class))})
            })
    StreamingResponseBody generateCSR(@RequestBody @Valid DistinguishedNameDTO distinguishedNameDTO);
}
