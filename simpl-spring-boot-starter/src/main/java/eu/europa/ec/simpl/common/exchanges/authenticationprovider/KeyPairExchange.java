package eu.europa.ec.simpl.common.exchanges.authenticationprovider;

import eu.europa.ec.simpl.common.model.dto.authenticationprovider.KeyPairDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PostExchange;

@Tag(name = "Keypair API", description = "API for managing Keypair")
@HttpExchange("keypair")
public interface KeyPairExchange {
    @GetExchange
    KeyPairDTO getInstalledKeyPair();

    @PostExchange("generate")
    void generateKeyPair();

    @Operation(
            summary = "Keypair Exists",
            description = "ONBOARDER_M checks whether a public/private Key Pair is stored in the database",
            responses = {
                @ApiResponse(responseCode = "200", description = "KeyPair is present"),
                @ApiResponse(responseCode = "404", description = "KeyPair is not present"),
                @ApiResponse(responseCode = "401", description = "Unauthorized")
            })
    @HttpExchange(method = "HEAD")
    ResponseEntity<Void> existsKeypair();
}
