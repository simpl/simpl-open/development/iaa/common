package eu.europa.ec.simpl.common.exchanges.identityprovider;

import eu.europa.ec.simpl.common.model.dto.identityprovider.CertificateDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.io.File;
import java.util.UUID;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.service.annotation.DeleteExchange;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PutExchange;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

@Tag(name = "Certificate API", description = "API for managing certificates")
@HttpExchange("certificate")
public interface CertificateExchange {
    @PutExchange("{id}")
    @ResponseStatus(HttpStatus.CREATED)
    CertificateDTO createCertificate(@PathVariable(name = "id") UUID id);

    @PutExchange(value = "/renew/{id}")
    @Operation(
            summary = "Renew Certificate",
            description = "Downloads the renewed certificate with the specified ID as a file",
            parameters = {
                @Parameter(
                        name = "id",
                        description = "ID of the certificate to be downloaded",
                        required = true,
                        schema = @Schema(implementation = UUID.class))
            },
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Certificate renewed successfully",
                        content = {@Content(schema = @Schema(implementation = File.class))}),
                @ApiResponse(
                        responseCode = "422",
                        description = "CSR not found for the requested certificate",
                        content =
                                @Content(
                                        mediaType = MediaType.APPLICATION_JSON_VALUE,
                                        schema = @Schema(implementation = ProblemDetail.class))),
            })
    StreamingResponseBody renewCertificate(@PathVariable(name = "id") UUID id);

    @GetExchange(url = "{id}")
    @Operation(
            summary = "Download Certificate",
            description = "Downloads the certificate with the specified ID as a file",
            parameters = {
                @Parameter(
                        name = "id",
                        description = "ID of the certificate to be downloaded",
                        required = true,
                        schema = @Schema(implementation = UUID.class))
            },
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Certificate downloaded successfully",
                        content = {@Content(schema = @Schema(implementation = File.class))}),
                @ApiResponse(
                        responseCode = "404",
                        description = "Certificate not found",
                        content = @Content(schema = @Schema(implementation = ProblemDetail.class))),
            })
    StreamingResponseBody downloadCertificate(@PathVariable(name = "id") UUID id);

    @GetExchange("{id}/validity")
    CertificateDTO checkCertificateValidity(@PathVariable(name = "id") UUID id);

    @DeleteExchange("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(
            summary = "Revoke Certificate",
            description = "Revokes the certificate with the specified ID",
            parameters = {
                @Parameter(
                        name = "id",
                        description = "ID of the certificate to be revoked",
                        required = true,
                        schema = @Schema(implementation = UUID.class))
            },
            responses = {
                @ApiResponse(responseCode = "204", description = "Certificate revoked successfully"),
                @ApiResponse(responseCode = "404", description = "Certificate not found")
            })
    void revoke(@PathVariable(name = "id") UUID id);
}
