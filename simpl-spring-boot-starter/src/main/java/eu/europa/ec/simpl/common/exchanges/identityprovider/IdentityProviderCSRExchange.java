package eu.europa.ec.simpl.common.exchanges.identityprovider;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.UUID;
import org.springframework.http.MediaType;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PostExchange;

@Tag(name = "Certificate Sign Request API", description = "API for managing CSR")
@HttpExchange("csr")
public interface IdentityProviderCSRExchange {

    @PostExchange(value = "{onboardingRequestId}", contentType = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(
            summary = "Validate and store a CSR for an onboarding request",
            description = "Validate and store a CSR for an onboarding request, then create a certificate",
            parameters = {
                @Parameter(
                        name = "onboardingRequestId",
                        description = "ID of the onboarding request",
                        required = true,
                        schema = @Schema(implementation = UUID.class))
            },
            responses = {
                @ApiResponse(responseCode = "200", description = "CSR uploaded successfully"),
                @ApiResponse(
                        responseCode = "400",
                        description = "CSR invalid or null",
                        content =
                                @Content(
                                        mediaType = MediaType.APPLICATION_JSON_VALUE,
                                        schema = @Schema(implementation = ProblemDetail.class))),
            })
    void validateAndStoreCSR(
            @PathVariable("onboardingRequestId") UUID requestId, @RequestPart("csr") MultipartFile csrFile);

    @Operation(
            summary = "Validate and store a CSR for a participant",
            description = "Validate and store a CSR for a participant, then create a certificate",
            parameters = {
                @Parameter(
                        name = "participantId",
                        description = "ID of the participant",
                        required = true,
                        schema = @Schema(implementation = UUID.class))
            },
            responses = {
                @ApiResponse(responseCode = "200", description = "CSR uploaded successfully"),
                @ApiResponse(
                        responseCode = "400",
                        description = "CSR invalid or null",
                        content =
                                @Content(
                                        mediaType = MediaType.APPLICATION_JSON_VALUE,
                                        schema = @Schema(implementation = ProblemDetail.class))),
            })
    @PostExchange(value = "store/{participantId}", contentType = MediaType.MULTIPART_FORM_DATA_VALUE)
    void storeCSRAndCreateCertificate(@PathVariable UUID participantId, @RequestPart("csr") MultipartFile csrFile);
}
