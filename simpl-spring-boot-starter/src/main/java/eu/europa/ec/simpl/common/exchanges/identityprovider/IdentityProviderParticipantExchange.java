package eu.europa.ec.simpl.common.exchanges.identityprovider;

import eu.europa.ec.simpl.common.filters.ParticipantFilter;
import eu.europa.ec.simpl.common.model.dto.identityprovider.CertificateDTO;
import eu.europa.ec.simpl.common.model.dto.identityprovider.ParticipantDTO;
import eu.europa.ec.simpl.common.model.dto.identityprovider.ParticipantWithIdentityAttributesDTO;
import eu.europa.ec.simpl.common.model.validators.CreateOperation;
import eu.europa.ec.simpl.common.responses.PageResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.util.UUID;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PostExchange;

@Tag(name = "Participant API", description = "API for managing Participant")
@HttpExchange("participant")
public interface IdentityProviderParticipantExchange {
    @ResponseStatus(HttpStatus.CREATED)
    @PostExchange
    @Operation(
            summary = "Create a new participant",
            description = "Creates a new participant with the provided details",
            requestBody =
                    @io.swagger.v3.oas.annotations.parameters.RequestBody(
                            description =
                                    "Details of the participant to create and of the identity attributes that will be assigned to it",
                            required = true,
                            content =
                                    @Content(
                                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                                            schema =
                                                    @Schema(
                                                            implementation =
                                                                    ParticipantWithIdentityAttributesDTO.class))),
            responses = {
                @ApiResponse(
                        responseCode = "201",
                        description = "Participant created successfully",
                        content =
                                @Content(
                                        mediaType = MediaType.APPLICATION_JSON_VALUE,
                                        schema = @Schema(implementation = UUID.class))),
                @ApiResponse(responseCode = "400", description = "Invalid participant data"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role")
            })
    UUID create(@RequestBody @Validated(CreateOperation.class) ParticipantWithIdentityAttributesDTO participantDTO);

    @GetExchange("{participantId}/credential-validity")
    @Operation(
            summary = "Check credential validity for a participant",
            description = "Checks the validity of the credential for the specified participant",
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Credential validity checked successfully",
                        content =
                                @Content(
                                        mediaType = MediaType.APPLICATION_JSON_VALUE,
                                        schema = @Schema(implementation = CertificateDTO.class))),
                @ApiResponse(responseCode = "404", description = "Participant not found"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role")
            })
    CertificateDTO checkCredentialValidity(
            @Parameter(description = "ID of the participant to check", required = true)
                    @PathVariable(name = "participantId")
                    UUID participantId);

    @GetExchange("{participantId}")
    @Operation(
            summary = "Get participant by ID",
            description = "Get participant by ID",
            responses = {
                @ApiResponse(responseCode = "200", description = "Participant retrieved successfully"),
                @ApiResponse(responseCode = "404", description = "No participant found with used ID"),
            })
    ParticipantDTO get(@PathVariable("participantId") UUID participantId);

    @GetExchange("search")
    @Operation(
            summary = "Search for participants",
            description = "Searches for participants based on the provided filter criteria",
            responses = {
                @ApiResponse(responseCode = "200", description = "Participants retrieved successfully"),
                @ApiResponse(responseCode = "400", description = "Invalid filter criteria"),
            })
    PageResponse<ParticipantDTO> search(
            @Parameter(description = "Filter criteria for searching participants", required = true)
                    @ParameterObject
                    @ModelAttribute
                    @Valid
                    ParticipantFilter filter,
            @Parameter(description = "Pagination information", required = true)
                    @ParameterObject
                    @PageableDefault(sort = "id")
                    Pageable pageable);

    @GetExchange("authority-exist")
    @Operation(summary = "Check if a the governance authority's participant is present")
    boolean isAuthorityAlreadyInitialized();
}
