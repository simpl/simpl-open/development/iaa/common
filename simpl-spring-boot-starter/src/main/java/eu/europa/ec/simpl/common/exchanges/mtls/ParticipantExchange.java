package eu.europa.ec.simpl.common.exchanges.mtls;

import eu.europa.ec.simpl.client.feign.annotations.PreflightEphemeralProof;
import eu.europa.ec.simpl.common.model.dto.identityprovider.ParticipantWithIdentityAttributesDTO;
import feign.RequestLine;

public interface ParticipantExchange {
    @PreflightEphemeralProof
    @RequestLine("GET /user-api/mtls/ping")
    ParticipantWithIdentityAttributesDTO ping();
}
