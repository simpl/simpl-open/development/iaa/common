package eu.europa.ec.simpl.common.exchanges.onboarding;

import eu.europa.ec.simpl.common.model.dto.onboarding.ParticipantTypeDTO;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.Set;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;

@Tag(name = "Participant Type API", description = "API for managing participant type")
@HttpExchange("participant-type")
public interface ParticipantTypeExchange {
    @GetExchange
    Set<ParticipantTypeDTO> getParticipantTypes();
}
