package eu.europa.ec.simpl.common.exchanges.securityattributeprovider;

import eu.europa.ec.simpl.common.filters.IdentityAttributeFilter;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.model.validators.CreateOperation;
import eu.europa.ec.simpl.common.model.validators.UpdateOperation;
import eu.europa.ec.simpl.common.responses.PageResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import java.util.UUID;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.service.annotation.*;

@Tag(name = "Identity Attribute API", description = "API for managing identity attributes")
@HttpExchange("identity-attribute")
public interface IdentityAttributeExchange {
    @ResponseStatus(HttpStatus.CREATED)
    @PostExchange
    @Operation(
            summary = "Create Identity Attribute",
            description = "Creates a new identity attribute.",
            requestBody =
                    @io.swagger.v3.oas.annotations.parameters.RequestBody(
                            description = "Identity attribute to be created",
                            required = true,
                            content =
                                    @Content(
                                            mediaType = "application/json",
                                            schema = @Schema(implementation = IdentityAttributeDTO.class))),
            responses = {
                @ApiResponse(
                        responseCode = "201",
                        description = "Successfully created identity attribute",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = IdentityAttributeDTO.class))),
                @ApiResponse(responseCode = "400", description = "Invalid input data"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(
                        responseCode = "409",
                        description = "Conflict: Identity attribute with the same code or name already exists")
            })
    IdentityAttributeDTO create(@RequestBody @Validated(CreateOperation.class) IdentityAttributeDTO attribute);

    @GetExchange("{id}")
    @Operation(
            summary = "Find Identity Attribute by ID",
            description = "Retrieves an identity attribute by its unique identifier.",
            parameters = {
                @Parameter(
                        name = "id",
                        description = "UUID of the identity attribute to be retrieved",
                        required = true,
                        schema = @Schema(type = "string", format = "uuid"))
            },
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Successfully retrieved identity attribute",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = IdentityAttributeDTO.class))),
                @ApiResponse(responseCode = "404", description = "Identity attribute not found"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role")
            })
    IdentityAttributeDTO findById(@PathVariable UUID id);

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutExchange("{id}")
    @Operation(
            summary = "Update Identity Attribute",
            description = "Updates an existing identity attribute by its unique identifier.",
            parameters = {
                @Parameter(
                        name = "id",
                        description = "UUID of the identity attribute to be updated",
                        required = true,
                        schema = @Schema(type = "string", format = "uuid"))
            },
            requestBody =
                    @io.swagger.v3.oas.annotations.parameters.RequestBody(
                            description = "Updated identity attribute data",
                            required = true,
                            content =
                                    @Content(
                                            mediaType = "application/json",
                                            schema = @Schema(implementation = IdentityAttributeDTO.class))),
            responses = {
                @ApiResponse(responseCode = "204", description = "Successfully updated identity attribute"),
                @ApiResponse(responseCode = "400", description = "Invalid input data"),
                @ApiResponse(responseCode = "404", description = "Identity attribute not found"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role")
            })
    void updateAttributes(
            @PathVariable UUID id, @RequestBody @Validated(UpdateOperation.class) IdentityAttributeDTO attribute);

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteExchange("{id}")
    @Operation(
            summary = "Delete Identity Attribute",
            description = "Deletes an identity attribute by its unique identifier.",
            parameters = {
                @Parameter(
                        name = "id",
                        description = "UUID of the identity attribute to be deleted",
                        required = true,
                        schema = @Schema(type = "string", format = "uuid"))
            },
            responses = {
                @ApiResponse(responseCode = "204", description = "Successfully deleted identity attribute"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "Identity attribute not found")
            })
    void delete(@PathVariable UUID id);

    @GetExchange("search")
    @Operation(
            summary = "Search Identity Attributes",
            description = "Searches for identity attributes based on provided filters and pagination options.",
            responses = {
                @ApiResponse(responseCode = "200", description = "Successfully retrieved search results"),
                @ApiResponse(responseCode = "400", description = "Invalid input data")
            })
    PageResponse<IdentityAttributeDTO> search(
            @ParameterObject @ModelAttribute IdentityAttributeFilter filter,
            @PageableDefault(sort = "id") @ParameterObject Pageable pageable);

    @ResponseStatus(HttpStatus.OK)
    @PutExchange("/assignable/{value}")
    @Operation(
            summary = "Update Assignable Parameter",
            description = "Updates the assignable parameter for a list of identity attributes.",
            requestBody =
                    @io.swagger.v3.oas.annotations.parameters.RequestBody(
                            description = "List of UUIDs representing identity attributes",
                            required = true,
                            content =
                                    @Content(
                                            mediaType = "application/json",
                                            array = @ArraySchema(schema = @Schema(type = "string", format = "uuid")))),
            parameters = {
                @Parameter(
                        name = "value",
                        description = "Boolean value to assign to the 'assignable' parameter",
                        required = true,
                        schema = @Schema(type = "boolean"))
            },
            responses = {
                @ApiResponse(responseCode = "200", description = "Successfully updated assignable parameter"),
                @ApiResponse(responseCode = "400", description = "Invalid input data"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role")
            })
    void updateAssignableParameter(@RequestBody List<UUID> body, @PathVariable boolean value);

    @ResponseStatus(HttpStatus.OK)
    @PutExchange("/add-participant-type/{participantType}")
    @Operation(
            summary = "Add Participant Type",
            description = "Adds a participant type to a list of identity attributes.",
            requestBody =
                    @io.swagger.v3.oas.annotations.parameters.RequestBody(
                            description = "Array of UUIDs representing identity attributes",
                            required = true,
                            content =
                                    @Content(
                                            mediaType = "application/json",
                                            array = @ArraySchema(schema = @Schema(type = "string", format = "uuid")))),
            parameters = {
                @Parameter(
                        name = "participantType",
                        description = "Type of participant to be added",
                        required = true,
                        schema = @Schema(implementation = String.class))
            },
            responses = {
                @ApiResponse(responseCode = "200", description = "Successfully added participant type"),
                @ApiResponse(responseCode = "400", description = "Invalid input data"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role")
            })
    void addParticipantType(
            @RequestBody List<UUID> body, @PathVariable(name = "participantType") String participantType);

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteExchange("unassign-participant/{participantId}")
    @Operation(
            summary = "Unassign Identity Attributes",
            description = "Unassigns a list of identity attributes from a user.",
            parameters = {
                @Parameter(
                        name = "participantId",
                        description = "UUID of the user from whom the identity attributes will be unassigned",
                        required = true,
                        schema = @Schema(implementation = UUID.class))
            },
            requestBody =
                    @io.swagger.v3.oas.annotations.parameters.RequestBody(
                            description = "List of UUIDs representing identity attributes to be unassigned",
                            required = true,
                            content =
                                    @Content(
                                            mediaType = "application/json",
                                            array = @ArraySchema(schema = @Schema(implementation = UUID.class)))),
            responses = {
                @ApiResponse(responseCode = "200", description = "Successfully unassigned identity attributes"),
                @ApiResponse(responseCode = "400", description = "Invalid input data"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role")
            })
    void unassign(@PathVariable(name = "participantId") UUID participantId, @RequestBody List<UUID> identityAttributes);

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutExchange("assign-participant/{participantId}")
    @Operation(
            summary = "Assign Identity Attributes",
            description = "Assigns a list of identity attributes to a user.",
            parameters = {
                @Parameter(
                        name = "participantId",
                        description = "UUID of the user to whom the identity attributes will be assigned",
                        required = true,
                        schema = @Schema(type = "string", format = "uuid"))
            },
            requestBody =
                    @io.swagger.v3.oas.annotations.parameters.RequestBody(
                            description = "List of UUIDs representing identity attributes to be assigned",
                            required = true,
                            content =
                                    @Content(
                                            mediaType = "application/json",
                                            array = @ArraySchema(schema = @Schema(implementation = UUID.class)))),
            responses = {
                @ApiResponse(responseCode = "204", description = "Successfully assigned identity attributes"),
                @ApiResponse(responseCode = "400", description = "Invalid input data"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role")
            })
    void assign(@PathVariable(name = "participantId") UUID participantId, @RequestBody List<UUID> identityAttributes);

    /**
     * @deprecated unused from 1.0.1
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutExchange("/assign/{participantType}/{participantId}")
    @Deprecated(forRemoval = true)
    void assignSnapshot(
            @PathVariable String participantType,
            @PathVariable UUID participantId,
            @RequestBody List<UUID> excludedAttributes);

    @ResponseStatus(HttpStatus.CREATED)
    @PostExchange("import")
    void importIdentityAttributes(
            @RequestBody @Validated(value = CreateOperation.class) List<IdentityAttributeDTO> list);
}
