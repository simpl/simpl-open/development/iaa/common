package eu.europa.ec.simpl.common.exchanges.usersroles;

import eu.europa.ec.simpl.common.model.dto.identityprovider.ParticipantWithIdentityAttributesDTO;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeWithOwnershipDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.EchoDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;

@Tag(name = "Agent API", description = "API for perform agent operations")
@HttpExchange("agent")
public interface AgentControllerExchange {

    @Operation(
            summary = "Get echo information",
            description = "Returns echo information including connection and MTLS status",
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Successfully retrieved echo information",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = EchoDTO.class))),
            })
    @GetExchange("echo")
    EchoDTO echo();

    @Operation(
            summary = "Get identity attributes with ownership",
            description = "Returns a list of identity attributes with ownership details",
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Successfully retrieved identity attributes with ownership",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        array =
                                                @ArraySchema(
                                                        schema =
                                                                @Schema(
                                                                        implementation =
                                                                                IdentityAttributeWithOwnershipDTO
                                                                                        .class)))),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role")
            })
    @GetExchange("identity-attributes")
    List<IdentityAttributeWithOwnershipDTO> getIdentityAttributesWithOwnership();

    @Operation(
            summary = "Get identity attributes with ownership",
            description = "Returns a list of identity attributes with ownership details",
            parameters = {
                @Parameter(
                        name = "certificateId",
                        description = "The certificate ID of the participant",
                        required = true,
                        schema = @Schema(type = "string"))
            },
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Successfully retrieved identity attributes with ownership",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        array =
                                                @ArraySchema(
                                                        schema =
                                                                @Schema(implementation = IdentityAttributeDTO.class)))),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role")
            })
    @GetExchange("identity-attributes/{credentialId}")
    List<IdentityAttributeDTO> getParticipantIdentityAttributes(@PathVariable("credentialId") String credentialId);

    @Operation(
            summary = "Ping a participant",
            description = "Pings a participant using the provided FQDN and returns identity attributes",
            parameters = {
                @Parameter(
                        name = "fqdn",
                        description = "Fully Qualified Domain Name of the participant",
                        required = true)
            },
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Successfully pinged the participant",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ParticipantWithIdentityAttributesDTO.class))),
                @ApiResponse(responseCode = "400", description = "Invalid FQDN provided")
            })
    @GetExchange("ping")
    ParticipantWithIdentityAttributesDTO ping(@RequestParam(name = "fqdn") String fqdn);
}
