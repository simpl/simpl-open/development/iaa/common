package eu.europa.ec.simpl.common.exchanges.usersroles;

import eu.europa.ec.simpl.common.model.dto.identityprovider.ParticipantDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.CredentialDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.service.annotation.DeleteExchange;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PostExchange;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

@Tag(name = "Credential API", description = "API for managing credentials")
@HttpExchange("credential")
public interface CredentialExchange {
    @ResponseStatus(HttpStatus.CREATED)
    @PostExchange(contentType = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(
            summary = "Upload a credential file",
            description = "Uploads a credential file to the server and returns the ID of the uploaded credential",
            parameters = {
                @Parameter(
                        name = "file",
                        description = "The credential file to be uploaded",
                        required = true,
                        content = @Content(mediaType = MediaType.MULTIPART_FORM_DATA_VALUE))
            },
            responses = {
                @ApiResponse(
                        responseCode = "201",
                        description = "Successfully uploaded credential",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema =
                                                @Schema(
                                                        example = "12345",
                                                        description = "The id of the uploaded credential",
                                                        type = "integer"))),
                @ApiResponse(responseCode = "400", description = "Invalid file format")
            })
    long uploadCredential(@RequestPart("file") MultipartFile file);

    @GetExchange
    @Operation(
            summary = "Check if a credential is present",
            description = "Returns true if a credential is present, false otherwise",
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Successfully checked credential presence",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema =
                                                @Schema(
                                                        example = "true",
                                                        description = "Boolean indicating if a credential is present",
                                                        type = "boolean")))
            })
    boolean hasCredential();

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteExchange
    @Operation(
            summary = "Delete the credential",
            description = "Deletes the existing credential from the server",
            responses = {
                @ApiResponse(responseCode = "204", description = "Successfully deleted credential"),
                @ApiResponse(responseCode = "404", description = "Credential not found")
            })
    void delete();

    @GetExchange("download")
    StreamingResponseBody downloadInstalledCredentials();

    @GetExchange("local")
    StreamingResponseBody downloadLocalCredentials();

    @GetExchange("public-key")
    @Operation(
            summary = "Retrieve the public key",
            description = "Fetches the public key associated with the stored credential",
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Successfully retrieved public key",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema =
                                                @Schema(
                                                        implementation = CredentialDTO.class,
                                                        description = "The public key details"))),
                @ApiResponse(responseCode = "404", description = "Credential not found")
            })
    CredentialDTO getPublicKey();

    @GetExchange("my-id")
    @Operation(
            summary = "Retrieve participant ID",
            description = "Fetches the participant ID associated with the current participant",
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Successfully retrieved participant ID",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema =
                                                @Schema(
                                                        implementation = ParticipantDTO.class,
                                                        description = "An object containing the participant ID"))),
                @ApiResponse(responseCode = "404", description = "Credential not found")
            })
    ParticipantDTO getMyParticipantId();

    @GetExchange("credential-id")
    @Operation(
            summary = "Retrieve credential ID",
            description = "Fetches the credential ID associated with the current participant",
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Successfully retrieved credential ID",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema =
                                                @Schema(
                                                        implementation = ParticipantDTO.class,
                                                        description = "An object containing the credential ID"))),
                @ApiResponse(responseCode = "404", description = "Credential not found")
            })
    ParticipantDTO getCredentialId();
}
