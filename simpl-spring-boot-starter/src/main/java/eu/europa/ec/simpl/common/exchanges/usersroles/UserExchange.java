package eu.europa.ec.simpl.common.exchanges.usersroles;

import eu.europa.ec.simpl.common.filters.KeycloakUserFilter;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakRoleDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakUserDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import java.util.List;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.service.annotation.*;

@Tag(name = "User API", description = "API for managing users")
@HttpExchange("user")
public interface UserExchange {

    @ResponseStatus(HttpStatus.CREATED)
    @PostExchange
    String createUser(@RequestBody @Valid KeycloakUserDTO userDTO);

    @PostExchange("as-t1user")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(
            summary = "Create a user",
            description = "Creates a new tier-one user",
            requestBody =
                    @io.swagger.v3.oas.annotations.parameters.RequestBody(
                            description = "The user details",
                            required = true,
                            content =
                                    @Content(
                                            mediaType = "application/json",
                                            schema = @Schema(implementation = KeycloakUserDTO.class))),
            responses = {
                @ApiResponse(
                        responseCode = "201",
                        description = "User created successfully",
                        content =
                                @Content(
                                        mediaType = "text/plain",
                                        schema =
                                                @Schema(
                                                        description = "The Id of the newly created user",
                                                        example = "01929051-2adb-7ee5-994a-b36f64fbb4dd"))),
                @ApiResponse(responseCode = "400", description = "Invalid input data"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "409", description = "Conflict: User already exists")
            })
    String createUserAsT1(@RequestBody @Valid KeycloakUserDTO userDTO);

    @GetExchange("{uuid}/roles")
    @Operation(
            summary = "Get user roles",
            description = "Retrieves the roles associated with a user by its UUID",
            parameters = {
                @Parameter(
                        name = "uuid",
                        description = "The UUID of the user",
                        required = true,
                        example = "01929051-2adb-7ee5-994a-b36f64fbb4dd")
            },
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Successfully retrieved user roles",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        array =
                                                @ArraySchema(
                                                        schema = @Schema(implementation = KeycloakRoleDTO.class)))),
                @ApiResponse(responseCode = "400", description = "Invalid input data"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "User not found"),
            })
    List<KeycloakRoleDTO> getRoles(@PathVariable("uuid") String uuid);

    @GetExchange
    @Operation(
            summary = "Get user by email",
            description = "Retrieves a user by its email address",
            parameters = {
                @Parameter(
                        name = "email",
                        description = "The email of the user",
                        required = true,
                        example = "user@example.com")
            },
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Successfully retrieved user",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = KeycloakUserDTO.class))),
                @ApiResponse(responseCode = "400", description = "Invalid email format"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "User not found"),
            })
    KeycloakUserDTO getUser(@RequestParam(name = "email") @Valid @Email String email);

    @GetExchange("{uuid}")
    @Operation(
            summary = "Get user by UUID",
            description = "Retrieves a user by its UUID",
            parameters = {
                @Parameter(
                        name = "uuid",
                        description = "The UUID of the user",
                        required = true,
                        example = "01929051-2adb-7ee5-994a-b36f64fbb4dd")
            },
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Successfully retrieved user",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = KeycloakUserDTO.class))),
                @ApiResponse(responseCode = "400", description = "Invalid UUID format"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "User not found"),
            })
    KeycloakUserDTO getUserByUuid(@PathVariable("uuid") String uuid);

    @PutExchange("{uuid}")
    @Operation(
            summary = "Update user",
            description = "Updates the details of an existing user by its UUID",
            parameters = {
                @Parameter(
                        name = "uuid",
                        description = "The UUID of the user",
                        required = true,
                        example = "01929051-2adb-7ee5-994a-b36f64fbb4dd")
            },
            requestBody =
                    @io.swagger.v3.oas.annotations.parameters.RequestBody(
                            description = "The user details to update",
                            required = true,
                            content =
                                    @Content(
                                            mediaType = "application/json",
                                            schema = @Schema(implementation = KeycloakUserDTO.class))),
            responses = {
                @ApiResponse(responseCode = "200", description = "User updated successfully"),
                @ApiResponse(responseCode = "400", description = "Invalid input data"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "User not found"),
            })
    void updateUser(@PathVariable("uuid") String uuid, @RequestBody @Valid KeycloakUserDTO userDTO);

    @PutExchange("{uuid}/roles")
    @Operation(
            summary = "Update user roles",
            description = "Updates the roles associated with a user by its UUID",
            parameters = {
                @Parameter(
                        name = "uuid",
                        description = "The UUID of the user",
                        required = true,
                        example = "01929051-2adb-7ee5-994a-b36f64fbb4dd")
            },
            requestBody =
                    @io.swagger.v3.oas.annotations.parameters.RequestBody(
                            description = "The list of roles to be assigned to the user",
                            required = true,
                            content =
                                    @Content(
                                            mediaType = "application/json",
                                            array = @ArraySchema(schema = @Schema(implementation = String.class)))),
            responses = {
                @ApiResponse(responseCode = "200", description = "User roles updated successfully"),
                @ApiResponse(responseCode = "400", description = "Invalid input data"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "User not found"),
            })
    void updateUserRoles(@PathVariable("uuid") String uuid, @RequestBody List<String> userRoles);

    @DeleteExchange("{uuid}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(
            summary = "Delete user",
            description = "Deletes a user by its UUID",
            parameters = {
                @Parameter(
                        name = "uuid",
                        description = "The UUID of the user",
                        required = true,
                        example = "01929051-2adb-7ee5-994a-b36f64fbb4dd")
            },
            responses = {
                @ApiResponse(responseCode = "204", description = "User deleted successfully"),
                @ApiResponse(responseCode = "400", description = "Invalid UUID format"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "User not found"),
            })
    void deleteUser(@PathVariable("uuid") String uuid);

    @GetExchange("search")
    @Operation(
            summary = "Search users",
            description = "Searches for users based on various filter criteria",
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Successfully retrieved list of users",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        array =
                                                @ArraySchema(
                                                        schema = @Schema(implementation = KeycloakUserDTO.class)))),
                @ApiResponse(responseCode = "400", description = "Invalid filter criteria"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
            })
    List<KeycloakUserDTO> search(@ParameterObject KeycloakUserFilter filter);

    @ResponseStatus(HttpStatus.CREATED)
    @PostExchange("import")
    void importUsers(@Valid @RequestBody List<KeycloakUserDTO> users);
}
