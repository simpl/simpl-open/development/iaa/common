package eu.europa.ec.simpl.common.filters;

import java.time.Instant;
import java.util.UUID;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class IdentityAttributeFilter {
    private String code;
    private String name;
    private Boolean enabled;
    private String participantTypeIn;
    private String participantTypeNotIn;
    private UUID participantIdIn;
    private UUID participantIdNotIn;
    private Instant updateTimestampFrom;
    private Instant updateTimestampTo;
}
