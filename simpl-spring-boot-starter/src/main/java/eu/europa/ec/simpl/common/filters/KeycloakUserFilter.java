package eu.europa.ec.simpl.common.filters;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class KeycloakUserFilter {
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private Integer first;
    private Integer max;
}
