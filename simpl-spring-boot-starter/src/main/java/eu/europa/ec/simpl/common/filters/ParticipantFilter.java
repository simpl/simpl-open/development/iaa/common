package eu.europa.ec.simpl.common.filters;

import java.time.Instant;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ParticipantFilter {
    private String organization;
    private String participantType;
    private Instant dateFrom;
    private Instant dateTo;
    private String credentialId;
}
