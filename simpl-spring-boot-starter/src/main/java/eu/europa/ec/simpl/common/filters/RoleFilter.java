package eu.europa.ec.simpl.common.filters;

import java.io.Serial;
import java.io.Serializable;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class RoleFilter implements Serializable {
    @Serial
    private static final long serialVersionUID = 3170829285240522140L;

    private String name;
    private String description;
    private String attributeName;
}
