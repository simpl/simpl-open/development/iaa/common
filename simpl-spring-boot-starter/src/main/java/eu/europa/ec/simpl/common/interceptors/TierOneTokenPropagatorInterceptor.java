package eu.europa.ec.simpl.common.interceptors;

import eu.europa.ec.simpl.client.core.suppliers.AuthorizationHeaderSupplier;
import jakarta.servlet.http.HttpServletRequest;
import java.util.Optional;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;

@Service
public class TierOneTokenPropagatorInterceptor implements AuthorizationHeaderSupplier {

    private final HttpServletRequest currentRequest;

    public TierOneTokenPropagatorInterceptor(HttpServletRequest currentRequest) {
        this.currentRequest = currentRequest;
    }

    @Override
    public String getAuthorizationHeader() {
        return Optional.ofNullable(RequestContextHolder.getRequestAttributes())
                .map(i -> currentRequest.getHeader(HttpHeaders.AUTHORIZATION))
                .orElse(null);
    }
}
