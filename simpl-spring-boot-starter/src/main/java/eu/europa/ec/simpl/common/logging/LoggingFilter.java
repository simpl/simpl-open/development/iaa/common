package eu.europa.ec.simpl.common.logging;

import static eu.simpl.MessageBuilder.buildMessage;

import eu.simpl.types.LogMessage;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Log4j2
public class LoggingFilter {

    private final List<LoggingRule> loggingRules;
    private final Logger logger;

    public LoggingFilter(List<LoggingRule> loggingRules, Logger logger) {
        this.loggingRules = loggingRules;
        this.logger = logger;
    }

    public LoggingFilter(List<LoggingRule> loggingRules) {
        this(loggingRules, new DefaultLogger());
    }

    public Mono<Void> filter(ServerWebExchange exchange) {
        return Flux.fromIterable(loggingRules)
                .filterWhen(rule -> rule.matches(exchange))
                .next()
                .flatMap(rule -> {
                    logger.log(exchange, rule.config());
                    return Mono.empty();
                });
    }

    private static class DefaultLogger implements Logger {
        @Override
        public void log(ServerWebExchange exchange, LoggingRule.Config config) {
            log.log(
                    Level.getLevel("BUSINESS"),
                    buildMessage(LogMessage.builder()
                            .businessOperations(config.operations())
                            .msg(config.message())
                            .build()));
        }
    }

    public interface Logger {
        void log(ServerWebExchange exchange, LoggingRule.Config config);
    }
}
