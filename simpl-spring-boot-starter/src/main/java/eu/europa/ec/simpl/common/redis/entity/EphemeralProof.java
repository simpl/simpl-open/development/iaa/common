package eu.europa.ec.simpl.common.redis.entity;

import java.time.Duration;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.TimeToLive;

@Accessors(chain = true)
@Getter
@Setter
@ToString
@RedisHash
public class EphemeralProof {

    @Id
    private String credentialId;

    @ToString.Exclude
    private String content;

    @TimeToLive
    private Long expiration;

    public EphemeralProof setExpiration(Duration expireIn) {
        this.expiration = expireIn.toSeconds();
        return this;
    }
}
