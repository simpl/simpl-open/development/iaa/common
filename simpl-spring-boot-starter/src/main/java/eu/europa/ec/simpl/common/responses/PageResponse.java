package eu.europa.ec.simpl.common.responses;

import java.util.Iterator;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.domain.Page;
import org.springframework.data.util.Streamable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class PageResponse<T> implements Streamable<T> {

    private List<T> content;

    private PageMetadata page;

    public PageResponse(Page<T> page) {
        this.content = page.getContent();
        this.page = new PageMetadata(page.getSize(), page.getNumber(), page.getTotalElements(), page.getTotalPages());
    }

    public record PageMetadata(long size, long number, long totalElements, long totalPages) {}

    @Override
    public Iterator<T> iterator() {
        return content.iterator();
    }
}
