package eu.europa.ec.simpl.common.security;

import java.util.List;
import java.util.UUID;

public interface AuthService {
    UUID getUserId();

    String getEmail();

    String getUsername();

    String getClaim(String claim);

    String getKid();

    List<String> getIdentityAttributes();

    boolean isAuthenticated();

    boolean hasRole(String role);
}
