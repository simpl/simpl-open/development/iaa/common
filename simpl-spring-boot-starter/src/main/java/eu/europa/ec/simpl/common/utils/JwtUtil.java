package eu.europa.ec.simpl.common.utils;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.ECDSASigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import eu.europa.ec.simpl.common.exceptions.RuntimeWrapperException;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.ECPrivateKey;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import lombok.experimental.UtilityClass;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;

@Log4j2
@UtilityClass
public class JwtUtil {

    private static final JsonMapper mapper = JsonMapper.builder()
            .disable(MapperFeature.USE_ANNOTATIONS)
            .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            .addModule(new JavaTimeModule())
            .build();

    public static SignedJWT parseJwt(String bearerToken) {
        try {
            return SignedJWT.parse(bearerToken);
        } catch (ParseException e) {
            throw new RuntimeWrapperException(e);
        }
    }

    public static Optional<String> getBearerToken(Supplier<String> authorizationHeaderSupplier) {
        return Optional.ofNullable(authorizationHeaderSupplier.get())
                .filter(s -> s.contains("Bearer "))
                .map(String::trim)
                .map(token -> token.split(" ")[1]);
    }

    public static Optional<String> getBearerToken(HttpHeaders headers) {
        return getBearerToken(() -> Optional.ofNullable(headers.get(HttpHeaders.AUTHORIZATION))
                .map(List::getFirst)
                .orElse(null));
    }

    public static <T> List<T> getListClaim(SignedJWT jwt, String claim, Class<T> elementType) {
        return deserializeListClaim(getClaim(jwt, claim), elementType);
    }

    public static JWTClaimsSet getClaimSet(SignedJWT jwt) {
        try {
            return jwt.getJWTClaimsSet();
        } catch (ParseException e) {
            throw new RuntimeWrapperException(e);
        }
    }

    public static Object getClaim(SignedJWT jwt, String claim) {
        try {
            return jwt.getJWTClaimsSet().getClaim(claim);
        } catch (ParseException e) {
            throw new RuntimeWrapperException(e);
        }
    }

    private static <T> List<T> deserializeListClaim(Object claim, Class<T> elementType) {
        var type = mapper.getTypeFactory().constructParametricType(List.class, elementType);

        return mapper.convertValue(claim, type);
    }

    public static SignedJWT createSignedJWT(Map<String, Object> claims) throws NoSuchAlgorithmException, JOSEException {

        var claimSetBuilder = new JWTClaimsSet.Builder();

        claims.forEach((name, claim) -> claimSetBuilder.claim(name, mapper.convertValue(claim, Object.class)));

        var jwt = new SignedJWT(new JWSHeader.Builder(JWSAlgorithm.ES256).build(), claimSetBuilder.build());

        var kpg = KeyPairGenerator.getInstance("ECDSA");
        kpg.initialize(256);

        var kp = kpg.generateKeyPair();
        jwt.sign(new ECDSASigner((ECPrivateKey) kp.getPrivate()));

        return jwt;
    }
}
