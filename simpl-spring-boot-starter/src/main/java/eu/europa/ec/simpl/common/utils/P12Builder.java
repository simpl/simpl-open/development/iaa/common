package eu.europa.ec.simpl.common.utils;

import eu.europa.ec.simpl.common.exceptions.RuntimeWrapperException;
import java.io.IOException;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.time.Instant;
import java.util.List;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;

public class P12Builder {
    private final String alias;
    private final String password;
    private final PrivateKey privateKey;
    private KeyStore keyStore;
    private final List<X509Certificate> certificateList;
    private final OutputStream outputStream;

    public P12Builder(
            String alias,
            String password,
            PrivateKey privateKey,
            List<X509Certificate> certificateList,
            OutputStream outputStream) {
        this.alias = alias;
        this.password = password;
        this.privateKey = privateKey;
        initKeystore();
        this.certificateList = certificateList;
        this.outputStream = outputStream;
    }

    public void buildKeyStore() {
        try {
            keyStore.setKeyEntry(
                    alias, privateKey, password.toCharArray(), certificateList.toArray(Certificate[]::new));
            keyStore.store(outputStream, password.toCharArray());

        } catch (GeneralSecurityException | IOException e) {
            throw new RuntimeWrapperException(e);
        }
    }

    private void initKeystore() {
        try {
            this.keyStore = KeyStore.getInstance("PKCS12", "BC");
            this.keyStore.load(null, null); // Init empty keystore
        } catch (GeneralSecurityException | IOException e) {
            throw new RuntimeWrapperException(e);
        }
    }

    public Instant getExpiryDate() {
        var cert = CredentialUtil.extractCertificateFromKeystore(this.keyStore);
        try {
            return new JcaX509CertificateHolder(cert).getNotAfter().toInstant();
        } catch (CertificateEncodingException e) {
            throw new RuntimeWrapperException(e);
        }
    }
}
