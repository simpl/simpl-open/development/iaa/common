package eu.europa.ec.simpl.common.utils;

import eu.europa.ec.simpl.common.exceptions.RuntimeWrapperException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import lombok.experimental.UtilityClass;

@UtilityClass
public class Sha384Converter {

    public static String toSha384(Key key) {
        return toSha384(key.getEncoded());
    }

    public static String toSha384(byte[] inputBytes) {
        return toSha(inputBytes, "SHA-384");
    }

    public static String toSha(byte[] inputBytes, String algorithm) {
        try {
            var messageDigest = MessageDigest.getInstance(algorithm);
            messageDigest.update(inputBytes);
            var digestedBytes = messageDigest.digest();
            return Base64.getEncoder().encodeToString(digestedBytes);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeWrapperException(e);
        }
    }
}
