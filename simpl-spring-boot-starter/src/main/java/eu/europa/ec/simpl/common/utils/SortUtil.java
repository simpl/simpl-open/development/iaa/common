package eu.europa.ec.simpl.common.utils;

import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import lombok.experimental.UtilityClass;
import org.springframework.data.domain.Sort;

@UtilityClass
public class SortUtil {

    public static Sort sortBy(List<String> sort) {

        if (sort.size() % 2 != 0) {
            throw new IllegalArgumentException("An even number of sort parameters is required");
        }

        return IntStream.iterate(0, index -> index < sort.size(), index -> index + 2)
                .mapToObj(index -> {
                    var property = sort.get(index);
                    var direction = Sort.Direction.fromString(sort.get(index + 1));
                    return Sort.by(direction, property);
                })
                .reduce(Sort::and)
                .orElse(Sort.unsorted());
    }

    public static List<String> toStringList(Sort sort) {
        return sort.stream()
                .flatMap(order ->
                        Stream.of(order.getProperty(), order.getDirection().name()))
                .toList();
    }
}
