package eu.europa.ec.simpl.common.argumentresolvers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.service.invoker.HttpRequestValues.Builder;

@ExtendWith(MockitoExtension.class)
public class QueryParamsArgumentResolverTest {

    @Mock
    private MethodParameter parameter;

    @Mock
    private Builder requestValues;

    @Test
    public void resolve_whenDoesNotHaveNesteObjects_returnFalse() {
        var queryArgumentResolver = new QueryParamsArgumentResolver();
        Object argument = new TestFields();
        when(parameter.hasParameterAnnotation(ModelAttribute.class)).thenReturn(true);
        try (var beanUtils = mockStatic(BeanUtils.class)) {
            when(BeanUtils.isSimpleProperty(any())).thenReturn(false);
            var result = queryArgumentResolver.resolve(argument, parameter, requestValues);
            assertThat(result).isFalse();
        }
    }

    @Test
    public void resolve_whenIsNotQueryParam_returnFalse() {
        var queryArgumentResolver = new QueryParamsArgumentResolver();
        Object argument = new Object();
        when(parameter.hasParameterAnnotation(ModelAttribute.class)).thenReturn(false);
        var result = queryArgumentResolver.resolve(argument, parameter, requestValues);
        assertThat(result).isFalse();
    }

    @Test
    public void resolve_whenObjectArgument_returnTrue() {
        var queryArgumentResolver = new QueryParamsArgumentResolver();
        Object argument = new Object();
        when(parameter.hasParameterAnnotation(ModelAttribute.class)).thenReturn(true);
        var result = queryArgumentResolver.resolve(argument, parameter, requestValues);
        assertThat(result).isTrue();
    }

    @Test
    public void resolve_whenIterable_returnTrue() {
        var queryArgumentResolver = new QueryParamsArgumentResolver();
        Object argument = List.of(new Object());
        when(parameter.hasParameterAnnotation(ModelAttribute.class)).thenReturn(true);
        var result = queryArgumentResolver.resolve(argument, parameter, requestValues);
        assertThat(result).isTrue();
    }

    @Test
    public void resolve_whenArgumentIdTestFields_returnTrue() {
        var queryArgumentResolver = new QueryParamsArgumentResolver();
        Object argument = new TestFields();
        when(parameter.hasParameterAnnotation(ModelAttribute.class)).thenReturn(true);
        var result = queryArgumentResolver.resolve(argument, parameter, requestValues);
        assertThat(result).isTrue();
    }

    public class TestFields {
        public final String field1 = "field1";
    }
}
