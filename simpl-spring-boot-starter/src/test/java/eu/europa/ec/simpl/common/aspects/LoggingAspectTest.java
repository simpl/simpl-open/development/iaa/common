package eu.europa.ec.simpl.common.aspects;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import eu.europa.ec.simpl.common.aspects.LoggingAspect.LoggingLevels;
import eu.europa.ec.simpl.common.aspects.LoggingAspect.Stringifier;
import java.util.List;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class LoggingAspectTest {

    @Mock
    private LoggingLevels loggingLevels;

    @Mock
    private List<Stringifier> stringifiers;

    @Mock
    private JoinPoint joinPoint;

    @InjectMocks
    private LoggingAspect aspect;

    @Test
    public void logBeforeServiceMethodsTest_success() {
        var signature = mock(Signature.class);
        Object[] args = {"arg1"};
        when(joinPoint.getTarget()).thenReturn(new String());
        when(joinPoint.getSignature()).thenReturn(signature);
        when(joinPoint.getArgs()).thenReturn(args);

        aspect.logBeforeServiceMethods(joinPoint);
    }
}
