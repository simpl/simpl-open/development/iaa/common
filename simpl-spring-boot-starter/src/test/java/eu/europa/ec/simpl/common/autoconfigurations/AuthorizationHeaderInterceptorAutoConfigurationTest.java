package eu.europa.ec.simpl.common.autoconfigurations;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.mock;

import eu.europa.ec.simpl.common.interceptors.AuthorizationHeaderInterceptor;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AuthorizationHeaderInterceptorAutoConfigurationTest {

    @Test
    public void constructorTest() {
        new AuthorizationHeaderInterceptorAutoConfiguration();
    }

    @Test
    public void restClientCustomizerTest() {
        var configuration = new AuthorizationHeaderInterceptorAutoConfiguration();
        var authorizationHeaderInterceptor = mock(AuthorizationHeaderInterceptor.class);
        assertDoesNotThrow(() -> configuration.restClientCustomizer(authorizationHeaderInterceptor));
    }
}
