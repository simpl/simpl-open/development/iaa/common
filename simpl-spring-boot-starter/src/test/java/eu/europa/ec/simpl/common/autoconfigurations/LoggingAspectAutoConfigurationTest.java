package eu.europa.ec.simpl.common.autoconfigurations;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.mock;

import eu.europa.ec.simpl.common.aspects.LoggingAspect;
import java.util.List;
import org.apache.logging.log4j.Level;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class LoggingAspectAutoConfigurationTest {

    @Test
    public void constructorTest() {
        new LoggingAspectAutoConfiguration();
    }

    @Test
    public void loggingAspectTest() {
        var configuration = new LoggingAspectAutoConfiguration();
        var serviceLoggingLevel = Level.ALL;
        var customStringifier = mock(LoggingAspect.Stringifier.class);
        var customStringifiers = List.of(customStringifier);
        assertDoesNotThrow(() -> configuration.loggingAspect(serviceLoggingLevel, customStringifiers));
    }
}
