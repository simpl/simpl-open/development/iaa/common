package eu.europa.ec.simpl.common.autoconfigurations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.time.Duration;
import org.apache.logging.log4j.Level;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.actuate.web.exchanges.HttpExchange;
import org.springframework.boot.actuate.web.exchanges.HttpExchange.Principal;
import org.springframework.boot.actuate.web.exchanges.HttpExchange.Request;
import org.springframework.boot.actuate.web.exchanges.HttpExchange.Response;
import org.springframework.boot.actuate.web.exchanges.HttpExchangeRepository;

@ExtendWith(MockitoExtension.class)
public class LoggingHttpAutoConfigurationTest {

    @Mock
    private HttpExchangeRepository httpExchangeRepository;

    @Mock
    private Level serviceLoggingLevel;

    @Mock
    private HttpExchange httpExchange;

    @InjectMocks
    private LoggingHttpAutoConfiguration configuration;

    @Test
    public void httpExchangesFilter_returnNotNull() {
        var result = configuration.httpExchangesFilter(httpExchangeRepository);
        assertThat(result).isNotNull();
    }

    @Test
    public void loggingHttpExchangeRepository_returnNotNull() {
        configuration.loggingHttpExchangeRepository(serviceLoggingLevel);
    }

    @Test
    public void loggingHttpExchangeRepositoryAdd_willNotThrow() {
        var loggingHttpExchangeRepository =
                new LoggingHttpAutoConfiguration.LoggingHttpExchangeRepository(serviceLoggingLevel);
        var request = mock(Request.class);
        var response = mock(Response.class);
        when(httpExchange.getRequest()).thenReturn(request);
        when(httpExchange.getResponse()).thenReturn(response);
        when(httpExchange.getTimeTaken()).thenReturn(Duration.ofMillis(200));
        when(httpExchange.getPrincipal()).thenReturn(new Principal("junit-principal"));

        when(request.getMethod()).thenReturn("junit-method");
        when(request.getUri()).thenReturn(URI.create("junit-uri"));
        when(response.getStatus()).thenReturn(400);
        assertDoesNotThrow(() -> loggingHttpExchangeRepository.add(httpExchange));
    }
}
