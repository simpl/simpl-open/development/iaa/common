package eu.europa.ec.simpl.common.exceptions;

import org.springframework.http.HttpStatus;

public class CustomStatusException extends StatusException {
    protected CustomStatusException(
            HttpStatus status, String message, String customPropertyName, String customPropertyValue) {
        super(status, message);
        getBody().setProperty(customPropertyName, customPropertyValue);
    }
}
