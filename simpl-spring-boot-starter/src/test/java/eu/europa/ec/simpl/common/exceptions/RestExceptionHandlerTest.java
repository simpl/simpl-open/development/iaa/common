package eu.europa.ec.simpl.common.exceptions;

import static eu.europa.ec.simpl.common.exceptions.RestExceptionHandler.SQL_UNIQUE_KEY_ERROR;
import static eu.europa.ec.simpl.common.exceptions.RestExceptionHandler.VALIDATION_ERRORS_PROPERTY_NAME;
import static eu.europa.ec.simpl.common.exceptions.SimplErrors.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URI;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.*;
import org.springframework.test.context.bean.override.mockito.MockitoSpyBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.resource.NoResourceFoundException;

@WebMvcTest
class RestExceptionHandlerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockitoSpyBean
    TestController controller;

    @Test
    void testHandleDataIntegrityViolationException() throws Exception {

        SQLException sqlException = new SQLException("reason", SQL_UNIQUE_KEY_ERROR);

        String constraintName = "constraint_name";
        String key = "key";
        String value = "value";

        String message = getConstraintErrorMessage(constraintName, key, value);

        ConstraintViolationException constraintViolationException =
                new ConstraintViolationException(message, sqlException, constraintName);
        Mockito.doThrow(new DataIntegrityViolationException(message, constraintViolationException))
                .when(controller)
                .testEndpoint();

        var result = mvc.perform(get("/test")).andReturn();
        var problem =
                objectMapper.readValue(result.getResponse().getContentAsString(), UniqueConstraintProblemDetail.class);

        assertThat(problem.getConstraintName()).isEqualTo(constraintName);
        assertThat(problem.getField()).isEqualTo(key);
        assertThat(problem.getValue()).isEqualTo(value);
        assertThat(problem.getStatus()).isEqualTo(HttpStatus.CONFLICT.value());
        assertThat(problem.getType()).isEqualTo(UNIQUE_CONSTRAINT_ERROR_TYPE);
    }

    private static String getConstraintErrorMessage(String constraintName, String key, String value) {
        return "could not execute statement [ERROR: duplicate key value violates unique constraint \"%1$s\"\n"
                + "  ...: Key (%2$s)=(%3$s) already exists.] [insert into ... (a,b,c) values (?,?,?)]; SQL [insert into ... (a,b,c) values (?,?,?)]; constraint [%1$s]"
                        .formatted(constraintName, key, value);
    }

    @Test
    void testHandleMethodArgumentNotValid() throws Exception {
        var result = mvc.perform(post("/controllerValidation")
                        .contentType("application/json")
                        .content("{ \"content\": \"\" }".getBytes()))
                .andReturn();
        var problem = objectMapper.readValue(result.getResponse().getContentAsString(), ProblemDetail.class);

        assertThat(problem.getProperties()).isNotNull();

        var errors = ((List<?>) problem.getProperties().get(VALIDATION_ERRORS_PROPERTY_NAME))
                .stream()
                        .map(fe -> objectMapper.convertValue(fe, ValidationError.class))
                        .toList();

        assertThat(errors)
                .hasSize(2)
                .anyMatch(error ->
                        error.getKey().equals("content") && error.getValue().equals(""))
                .anyMatch(error -> error.getKey().equals("value") && Objects.isNull(error.getValue()));
    }

    @Test
    void testHandleConstraintViolationException() throws Exception {
        var result = mvc.perform(post("/serviceValidation")
                        .contentType("application/json")
                        .content("{ \"content\": \"\" }".getBytes()))
                .andReturn();
        var problem = objectMapper.readValue(result.getResponse().getContentAsString(), ProblemDetail.class);

        assertThat(problem.getProperties()).isNotNull();
        assertThat(problem.getType()).isEqualTo(VALIDATION_ERROR_TYPE);

        var fieldErrors = ((List<?>) problem.getProperties().get(VALIDATION_ERRORS_PROPERTY_NAME))
                .stream()
                        .map(fe -> objectMapper.convertValue(fe, ValidationError.class))
                        .toList();

        assertThat(fieldErrors)
                .hasSize(2)
                .anyMatch(error ->
                        error.getKey().equals("content") && error.getValue().equals(""))
                .anyMatch(error -> error.getKey().equals("value") && Objects.isNull(error.getValue()));
    }

    @Test
    void testHandleHandlerMethodValidationExceptionQueryParam() throws Exception {
        var problem = testHttpStatusProblem("/requestParam?email=123", HttpStatus.BAD_REQUEST, VALIDATION_ERROR_TYPE);

        assertThat(problem.getProperties()).isNotNull();

        var errors = ((List<?>) problem.getProperties().get(VALIDATION_ERRORS_PROPERTY_NAME))
                .stream()
                        .map(fe -> objectMapper.convertValue(fe, ValidationError.class))
                        .toList();

        assertThat(errors)
                .hasSize(1)
                .anyMatch(error ->
                        error.getKey().equals("email") && error.getValue().equals("123"));
    }

    @Test
    void handleHandlerMethodValidationExceptionPathVariable() throws Exception {
        var problem = testHttpStatusProblem(
                "/pathParam/moreThanFiveCharacters", HttpStatus.BAD_REQUEST, VALIDATION_ERROR_TYPE);

        assertThat(problem.getProperties()).isNotNull();

        var errors = ((List<?>) problem.getProperties().get(VALIDATION_ERRORS_PROPERTY_NAME))
                .stream()
                        .map(fe -> objectMapper.convertValue(fe, ValidationError.class))
                        .toList();

        assertThat(errors)
                .hasSize(1)
                .anyMatch(error ->
                        error.getKey().equals("param") && error.getValue().equals("moreThanFiveCharacters"));
    }

    @Test
    void testHandleClientExceptionServerErrorCode() throws Exception {

        Mockito.doThrow(new HttpClientErrorException(HttpStatusCode.valueOf(503)))
                .when(controller)
                .testEndpoint();

        var result = mvc.perform(get("/test")).andReturn();
        var problem = objectMapper.readValue(result.getResponse().getContentAsString(), ClientProblemDetail.class);

        assertThat(problem.getType()).isEqualTo(CLIENT_ERROR_TYPE);
        assertThat(problem.getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR.value());

        assertThat(problem.getCause()).isNotNull();
        assertThat(problem.getCause().getStatus()).isEqualTo(503);
    }

    @Test
    void testHandleClientExceptionClientErrorCode() throws Exception {

        Mockito.doThrow(new HttpClientErrorException(HttpStatusCode.valueOf(404)))
                .when(controller)
                .testEndpoint();

        var result = mvc.perform(get("/test")).andReturn();
        var problem = objectMapper.readValue(result.getResponse().getContentAsString(), ClientProblemDetail.class);

        assertThat(problem.getType()).isEqualTo(CLIENT_ERROR_TYPE);
        assertThat(problem.getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR.value());

        assertThat(problem.getCause()).isNotNull();
        assertThat(problem.getCause().getStatus()).isEqualTo(404);
    }

    @Test
    void testHandleErrorResponseExceptionStatusException() throws Exception {
        var expectedStatus = HttpStatus.NOT_FOUND;
        var expectedText = "Test exception";
        var customPropertyName = "custom";
        var customPropertyValue = "Custom property value";

        Mockito.doThrow(new CustomStatusException(
                        expectedStatus, expectedText, customPropertyName, customPropertyValue))
                .when(controller)
                .testEndpoint();

        var result = mvc.perform(get("/test")).andReturn();
        var problem = objectMapper.readValue(result.getResponse().getContentAsString(), ProblemDetail.class);

        assertThat(result.getResponse().getStatus()).isEqualTo(expectedStatus.value());
        assertThat(problem.getType()).isEqualTo(URI.create("about:blank"));
        assertThat(problem.getTitle()).isEqualTo(expectedText);
        assertThat(problem.getProperties()).containsEntry(customPropertyName, customPropertyValue);
    }

    @Test
    void testHandleHttpRequestMethodNotSupported() throws Exception {
        Mockito.doThrow(new HttpRequestMethodNotSupportedException("GET", List.of("PUT")))
                .when(controller)
                .testEndpoint();
        testHttpStatusProblem(HttpStatus.METHOD_NOT_ALLOWED);
    }

    @Test
    void testHandleHttpMediaTypeNotSupported() throws Exception {
        Mockito.doThrow(new HttpMediaTypeNotSupportedException(
                        MediaType.APPLICATION_JSON, List.of(MediaType.APPLICATION_XML)))
                .when(controller)
                .testEndpoint();
        testHttpStatusProblem(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    @Test
    void testHandleHttpMediaTypeNotAcceptable() throws Exception {
        Mockito.doThrow(new HttpMediaTypeNotAcceptableException("application/json"))
                .when(controller)
                .testEndpoint();
        testHttpStatusProblem(HttpStatus.NOT_ACCEPTABLE);
    }

    @Test
    void testHandleMissingServletRequestParameter() throws Exception {
        Mockito.doThrow(new MissingServletRequestParameterException("name", "type"))
                .when(controller)
                .testEndpoint();
        testHttpStatusProblem(HttpStatus.BAD_REQUEST);
    }

    @Test
    void testHandleMissingServletRequestPart() throws Exception {
        Mockito.doThrow(new MissingServletRequestPartException("part"))
                .when(controller)
                .testEndpoint();
        testHttpStatusProblem(HttpStatus.BAD_REQUEST);
    }

    @Test
    void testHandleServletRequestBindingException() throws Exception {
        Mockito.doThrow(new ServletRequestBindingException("msg"))
                .when(controller)
                .testEndpoint();
        testHttpStatusProblem(HttpStatus.BAD_REQUEST);
    }

    @Test
    void testHandleNoHandlerFoundException() throws Exception {
        Mockito.doThrow(new NoHandlerFoundException("GET", "url", new HttpHeaders()))
                .when(controller)
                .testEndpoint();
        testHttpStatusProblem(HttpStatus.NOT_FOUND);
    }

    @Test
    void testHandleNoResourceFoundException() throws Exception {
        Mockito.doThrow(new NoResourceFoundException(HttpMethod.GET, "path"))
                .when(controller)
                .testEndpoint();
        testHttpStatusProblem(HttpStatus.NOT_FOUND);
    }

    @Test
    void testHandleAsyncRequestTimeoutException() throws Exception {
        Mockito.doThrow(new AsyncRequestTimeoutException()).when(controller).testEndpoint();
        testHttpStatusProblem(HttpStatus.SERVICE_UNAVAILABLE);
    }

    @Test
    void testHandleMaxUploadSizeExceededException() throws Exception {
        Mockito.doThrow(new MaxUploadSizeExceededException(1024))
                .when(controller)
                .testEndpoint();
        testHttpStatusProblem(HttpStatus.PAYLOAD_TOO_LARGE);
    }

    @Test
    void testHandleMissingPathVariable() throws Exception {
        testHttpStatusProblem("/missing/variable", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Test
    void testHandleMissingPathVariableAfterConversion() throws Exception {
        var result = mvc.perform(get("/converter/variable")).andReturn();
        var problem = objectMapper.readValue(result.getResponse().getContentAsString(), ProblemDetail.class);

        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        // In this case problem detail is not consistent with response code, see
        // https://github.com/spring-projects/spring-framework/issues/31382#event-10752940911 and related issues
        assertThat(problem.getType()).isEqualTo(URI.create("about:blank"));
        assertThat(problem.getTitle()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
        assertThat(problem.getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR.value());
    }

    private ProblemDetail testHttpStatusProblem(HttpStatus expectedStatus) throws Exception {
        return testHttpStatusProblem("/test", expectedStatus, URI.create("about:blank"));
    }

    private ProblemDetail testHttpStatusProblem(String path, HttpStatus expectedStatus) throws Exception {
        return testHttpStatusProblem(path, expectedStatus, URI.create("about:blank"));
    }

    private ProblemDetail testHttpStatusProblem(String path, HttpStatus expectedStatus, URI expectedType)
            throws Exception {
        var result = mvc.perform(get(path)).andReturn();
        var problem = objectMapper.readValue(result.getResponse().getContentAsString(), ProblemDetail.class);
        assertThat(result.getResponse().getStatus()).isEqualTo(expectedStatus.value());
        assertThat(problem.getType()).isEqualTo(expectedType);
        assertThat(problem.getTitle()).isEqualTo(expectedStatus.getReasonPhrase());
        assertThat(problem.getStatus()).isEqualTo(expectedStatus.value());
        return problem;
    }
}
