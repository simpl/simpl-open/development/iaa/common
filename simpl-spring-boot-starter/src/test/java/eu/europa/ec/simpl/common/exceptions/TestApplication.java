package eu.europa.ec.simpl.common.exceptions;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TestApplication {

    @Bean
    public TestService testService() {
        return new TestService();
    }

    @Bean
    public TestConverter testConverter() {
        return new TestConverter();
    }
}
