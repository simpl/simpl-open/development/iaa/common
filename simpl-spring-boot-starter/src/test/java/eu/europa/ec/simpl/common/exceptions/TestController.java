package eu.europa.ec.simpl.common.exceptions;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Max;
import org.springframework.web.bind.annotation.*;

@RestController
public class TestController {

    private final TestService testService;

    public TestController(TestService testService) {
        this.testService = testService;
    }

    @PostMapping("controllerValidation")
    public void testEndpoint(@RequestBody @Valid TestModel model) throws Exception {}

    @PostMapping("serviceValidation")
    public void serviceValidated(@RequestBody TestModel model) throws Exception {
        testService.test(model);
    }

    @GetMapping("test")
    public void testEndpoint() throws Exception {}

    @GetMapping("missing/{variable}")
    public void missingPathVariable(@PathVariable String wrongName) {}

    @GetMapping("converter/{variable}")
    public void converterPathVariable(@PathVariable TestModel variable) {}

    @GetMapping("requestParam")
    public void validRequestParam(@RequestParam @Email String email) {}

    @GetMapping("pathParam/{param}")
    public void validPathParam(@PathVariable @Max(5) String param) {}
}
