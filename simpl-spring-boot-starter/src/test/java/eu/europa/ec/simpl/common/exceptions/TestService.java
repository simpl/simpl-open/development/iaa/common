package eu.europa.ec.simpl.common.exceptions;

import jakarta.validation.Valid;
import org.springframework.validation.annotation.Validated;

@Validated
public class TestService {

    void test(@Valid TestModel model) {}
}
