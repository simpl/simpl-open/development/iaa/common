package eu.europa.ec.simpl.common.interceptors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import jakarta.servlet.http.HttpServletRequest;
import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

@ExtendWith(MockitoExtension.class)
public class AuthorizationHeaderInterceptorTest {

    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private HttpRequest request;

    @Mock
    private ClientHttpRequestExecution execution;

    @InjectMocks
    private AuthorizationHeaderInterceptor interceptor;

    @Test
    public void interceptTest_withRequestAttributesNotNull_thenDoesNotThrow() throws IOException {
        var attributes = mock(RequestAttributes.class);
        var body = "Hello, JunitTest".getBytes();
        var headers = new HttpHeaders();
        when(httpServletRequest.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn("junit-authorization");
        when(request.getHeaders()).thenReturn(headers);
        try (var requestContextHolder = mockStatic(RequestContextHolder.class)) {
            when(RequestContextHolder.getRequestAttributes()).thenReturn(attributes);
            assertDoesNotThrow(() -> interceptor.intercept(request, body, execution));
        }

        assertThat(headers.get(HttpHeaders.AUTHORIZATION).getFirst()).isEqualTo("junit-authorization");
    }

    @Test
    public void interceptTest_withRequestAttributesNull_thenDoesNotThrow() throws IOException {
        var body = "Hello, JunitTest".getBytes();
        assertDoesNotThrow(() -> interceptor.intercept(request, body, execution));
    }
}
