package eu.europa.ec.simpl.common.logging;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import eu.europa.ec.simpl.common.logging.LoggingFilter.Logger;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@ExtendWith(MockitoExtension.class)
public class LoggingFilterTest {

    @Mock
    private LoggingRule loggingRule;

    private List<LoggingRule> loggingRules;

    @Mock
    private Logger logger;

    @Mock
    private ServerWebExchange exchange;

    @BeforeEach
    public void init() {
        loggingRules = List.of(loggingRule);
    }

    @Test
    public void filter_withOutLogger_doNotThrow() {
        var config = mock(LoggingRule.Config.class);
        when(loggingRule.matches(any())).thenReturn(Mono.just(true));
        when(loggingRule.config()).thenReturn(config);
        when(config.operations()).thenReturn(List.of("junit-operation1"));
        when(config.message()).thenReturn("junit-message");
        var loggerFilter = assertDoesNotThrow(() -> new LoggingFilter(loggingRules));
        assertDoesNotThrow(() -> loggerFilter.filter(exchange).block());
    }

    @Test
    public void filter_withLogger_doNotThrow() {
        when(loggingRule.matches(any())).thenReturn(Mono.just(true));
        var loggerFilter = assertDoesNotThrow(() -> new LoggingFilter(loggingRules, logger));
        assertDoesNotThrow(() -> loggerFilter.filter(exchange).block());
    }
}
