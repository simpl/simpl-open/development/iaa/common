package eu.europa.ec.simpl.common.model.csr;

import static org.mockito.Mockito.when;

import eu.europa.ec.simpl.common.csr.AbstractCertificateSignRequest;
import eu.europa.ec.simpl.common.csr.AlgorithmConfig;
import eu.europa.ec.simpl.common.csr.EllipticCertificateSignRequest;
import eu.europa.ec.simpl.common.model.dto.authenticationprovider.DistinguishedNameDTO;
import eu.europa.ec.simpl.common.model.dto.authenticationprovider.KeyPairDTO;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import org.assertj.core.api.Assertions;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class EllipticCertificateSignRequestTest {

    @Mock
    private DistinguishedNameDTO dn;

    @Mock
    private X500Name subject;

    private String dnsName = "dnsName-junit";

    @Mock
    private PKCS10CertificationRequest csr;

    @Mock
    private AlgorithmConfig config;

    private KeyPair keyPair;

    @BeforeAll
    static void registerSecurityProvider() {
        var provider = new BouncyCastleProvider();
        Security.addProvider(provider);
    }

    @BeforeEach
    void init() throws NoSuchAlgorithmException {
        var kpg = KeyPairGenerator.getInstance("ECDSA");
        keyPair = kpg.generateKeyPair();
    }

    @Test
    void getRawCsrTest() {
        when(config.algorithm()).thenReturn("ECDSA");
        when(config.signatureAlgorithm()).thenReturn("SHA256withECDSA");

        var request = abstractCertificateSignRequest();

        Assertions.assertThat(request.getRawCsr()).isNotEmpty();
    }

    private AbstractCertificateSignRequest abstractCertificateSignRequest() {
        var publicKey = keyPair.getPublic().getEncoded();
        var privateKey = keyPair.getPrivate().getEncoded();
        var keyPairDTO = new KeyPairDTO(publicKey, privateKey);
        when(dn.getCommonName()).thenReturn("CommonName");
        when(dn.getOrganization()).thenReturn("Organization");
        when(dn.getOrganizationalUnit()).thenReturn("OrganizationalUnit");
        return new EllipticCertificateSignRequest(dn, keyPairDTO, dnsName, config);
    }
}
