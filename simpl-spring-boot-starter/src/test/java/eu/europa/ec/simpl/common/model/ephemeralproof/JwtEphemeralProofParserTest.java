package eu.europa.ec.simpl.common.model.ephemeralproof;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import com.nimbusds.jwt.SignedJWT;
import eu.europa.ec.simpl.common.ephemeralproof.JwtEphemeralProofParser;
import eu.europa.ec.simpl.common.exceptions.InvalidEphemeralProofException;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.utils.JwtUtil;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.UUID;
import org.instancio.Instancio;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class JwtEphemeralProofParserTest {

    @Mock
    private SignedJWT ephemeralProf;

    private JwtEphemeralProofParser parser;

    private static final String VALID_JWT = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIwMTkzZDkyM"
            + "S0xYzViLTcwYTQtYjgxMi01MTU5YjM5MGQwZmIiLCJuYW1lIjoiVXNlciBuYW1lIiwiaWF0IjoxNTE2MjM5MDIyLCJ"
            + "leHAiOjI1MjQ2MDgwMDAsInB1YmxpY0tleXMiOlsia2V5MSIsImtleTIiXX0.yzXGpfSEctxgzZT4EMjHrTrtGhKpW"
            + "qnJEsb3C2P098c";

    @Test
    void testConstructorWithInvalidJwt() {
        parser = new JwtEphemeralProofParser(VALID_JWT);
        assertThrows(InvalidEphemeralProofException.class, () -> new JwtEphemeralProofParser("invalid_jwt"));
    }

    @Test
    void testGetRaw() {
        parser = new JwtEphemeralProofParser(VALID_JWT);
        assertThat(parser.getRaw()).isEqualTo(VALID_JWT);
    }

    @Test
    void testGetSubject() {
        parser = new JwtEphemeralProofParser(VALID_JWT);
        var expectedUUID = UUID.fromString("0193d921-1c5b-70a4-b812-5159b390d0fb");
        assertThat(parser.getSubject()).isEqualTo(expectedUUID);
    }

    @Test
    void testGetIdentityAttributes() {
        parser = new JwtEphemeralProofParser(VALID_JWT);
        var expectedAttributes = Instancio.createList(IdentityAttributeDTO.class);

        try (var mockedStatic = mockStatic(JwtUtil.class)) {
            mockedStatic
                    .when(() -> JwtUtil.getListClaim(
                            any(SignedJWT.class), eq("attributes"), eq(IdentityAttributeDTO.class)))
                    .thenReturn(expectedAttributes);

            assertThat(parser.getIdentityAttributes()).isEqualTo(expectedAttributes);
        }
    }

    @Test
    void testGetPublicKeys() {
        parser = new JwtEphemeralProofParser(VALID_JWT);
        var expectedPublicKeys = List.of("key1", "key2");
        var result = parser.getPublicKeys();

        assertThat(result).isEqualTo(expectedPublicKeys);
    }

    @Test
    void testGetExpiration() {
        parser = new JwtEphemeralProofParser(VALID_JWT);
        var localDateTime = LocalDateTime.of(2050, 1, 1, 0, 0, 0);
        var expectedExpiration = localDateTime.toInstant(ZoneOffset.UTC);

        var result = parser.getExpiration();
        assertThat(result).isEqualTo(expectedExpiration);
    }
}
