package eu.europa.ec.simpl.common.security;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import eu.europa.ec.simpl.common.utils.JwtUtil;
import jakarta.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Supplier;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@ExtendWith(MockitoExtension.class)
class JwtServiceTest {
    @Mock
    private HttpServletRequest request;

    @InjectMocks
    private JwtService jwtService;

    @Test
    void getUserId_shouldReturnUserId() throws Exception {
        try (var mockedJwtUtil = mockStatic(JwtUtil.class)) {
            var expectedUserId = "123e4567-e89b-12d3-a456-426614174000";
            var signedJWT = mock(SignedJWT.class);
            var claimsSet = new JWTClaimsSet.Builder().subject(expectedUserId).build();
            when(signedJWT.getJWTClaimsSet()).thenReturn(claimsSet);

            mockedJwtUtil
                    .when(() -> JwtUtil.getBearerToken(any(Supplier.class)))
                    .thenReturn(Optional.of(JWT_SAMPLE));
            mockedJwtUtil.when(() -> JwtUtil.parseJwt(anyString())).thenReturn(signedJWT);

            UUID userId = jwtService.getUserId();

            assertThat(userId).isEqualTo(UUID.fromString(expectedUserId));
        }
    }

    @Test
    void getEmail_shouldReturnEmail() throws Exception {
        try (var mockedJwtUtil = mockStatic(JwtUtil.class)) {
            var expectedEmail = "test@example.com";
            var signedJWT = mock(SignedJWT.class);
            var claimsSet =
                    new JWTClaimsSet.Builder().claim("email", expectedEmail).build();
            when(signedJWT.getJWTClaimsSet()).thenReturn(claimsSet);

            mockedJwtUtil
                    .when(() -> JwtUtil.getBearerToken(any(Supplier.class)))
                    .thenReturn(Optional.of(JWT_SAMPLE));
            mockedJwtUtil.when(() -> JwtUtil.parseJwt(anyString())).thenReturn(signedJWT);

            String email = jwtService.getEmail();

            assertThat(email).isEqualTo(expectedEmail);
        }
    }

    @Test
    void getUsername_shouldReturnUsername() throws Exception {
        try (var mockedJwtUtil = mockStatic(JwtUtil.class)) {
            var expectedUsername = "testuser";
            var signedJWT = mock(SignedJWT.class);
            var claimsSet = new JWTClaimsSet.Builder()
                    .claim("preferred_username", expectedUsername)
                    .build();
            when(signedJWT.getJWTClaimsSet()).thenReturn(claimsSet);

            mockedJwtUtil
                    .when(() -> JwtUtil.getBearerToken(any(Supplier.class)))
                    .thenReturn(Optional.of(JWT_SAMPLE));
            mockedJwtUtil.when(() -> JwtUtil.parseJwt(anyString())).thenReturn(signedJWT);

            String username = jwtService.getUsername();

            assertThat(username).isEqualTo(expectedUsername);
        }
    }

    @Test
    void getClaim_shouldReturnClaim() throws Exception {
        try (var mockedJwtUtil = mockStatic(JwtUtil.class)) {
            var claimName = "custom_claim";
            var expectedClaimValue = "custom_value";
            var signedJWT = mock(SignedJWT.class);
            var claimsSet = new JWTClaimsSet.Builder()
                    .claim(claimName, expectedClaimValue)
                    .build();
            when(signedJWT.getJWTClaimsSet()).thenReturn(claimsSet);

            mockedJwtUtil
                    .when(() -> JwtUtil.getBearerToken(any(Supplier.class)))
                    .thenReturn(Optional.of(JWT_SAMPLE));
            mockedJwtUtil.when(() -> JwtUtil.parseJwt(anyString())).thenReturn(signedJWT);

            String claim = jwtService.getClaim(claimName);

            assertThat(claim).isEqualTo(expectedClaimValue);
        }
    }

    @Test
    void getKid_shouldReturnKid() throws Exception {
        try (var mockedJwtUtil = mockStatic(JwtUtil.class)) {
            var expectedKid = "kid123";
            var signedJWT = mock(SignedJWT.class);
            var header =
                    new JWSHeader.Builder(JWSAlgorithm.RS256).keyID(expectedKid).build();
            when(signedJWT.getHeader()).thenReturn(header);

            mockedJwtUtil
                    .when(() -> JwtUtil.getBearerToken(any(Supplier.class)))
                    .thenReturn(Optional.of(JWT_SAMPLE));
            mockedJwtUtil.when(() -> JwtUtil.parseJwt(anyString())).thenReturn(signedJWT);

            String kid = jwtService.getKid();

            assertThat(kid).isEqualTo(expectedKid);
        }
    }

    @Test
    void getIdentityAttributes_shouldReturnIdentityAttributes() throws Exception {
        try (var mockedJwtUtil = mockStatic(JwtUtil.class)) {
            var expectedAttributes = List.of("attr1", "attr2");
            var signedJWT = mock(SignedJWT.class);
            var claimsSet = new JWTClaimsSet.Builder()
                    .claim("identity_attributes", expectedAttributes)
                    .build();
            when(signedJWT.getJWTClaimsSet()).thenReturn(claimsSet);

            mockedJwtUtil
                    .when(() -> JwtUtil.getBearerToken(any(Supplier.class)))
                    .thenReturn(Optional.of(JWT_SAMPLE));
            mockedJwtUtil.when(() -> JwtUtil.parseJwt(anyString())).thenReturn(signedJWT);

            List<String> attributes = jwtService.getIdentityAttributes();

            assertThat(attributes).isEqualTo(expectedAttributes);
        }
    }

    @Test
    void getIdentityAttributes_shouldReturnEmptyListWhenClaimNotPresent() throws Exception {
        try (var mockedJwtUtil = mockStatic(JwtUtil.class)) {
            var signedJWT = mock(SignedJWT.class);
            var claimsSet = new JWTClaimsSet.Builder().build(); // No identity_attributes claim
            when(signedJWT.getJWTClaimsSet()).thenReturn(claimsSet);

            mockedJwtUtil
                    .when(() -> JwtUtil.getBearerToken(any(Supplier.class)))
                    .thenReturn(Optional.of(JWT_SAMPLE));
            mockedJwtUtil.when(() -> JwtUtil.parseJwt(anyString())).thenReturn(signedJWT);

            List<String> attributes = jwtService.getIdentityAttributes();

            assertThat(attributes).isEmpty();
        }
    }

    @Test
    void isAuthenticated_shouldReturnTrueWhenTokenPresent() {
        try (var mockedJwtUtil = mockStatic(JwtUtil.class);
                var mockedRequestContextHolder = mockStatic(RequestContextHolder.class)) {
            mockedJwtUtil
                    .when(() -> JwtUtil.getBearerToken(any(Supplier.class)))
                    .thenReturn(Optional.of(JWT_SAMPLE));

            ServletRequestAttributes mockRequestAttributes = mock(ServletRequestAttributes.class);
            mockedRequestContextHolder
                    .when(RequestContextHolder::getRequestAttributes)
                    .thenReturn(mockRequestAttributes);

            boolean isAuthenticated = jwtService.isAuthenticated();

            assertThat(isAuthenticated).isTrue();
        }
    }

    @Test
    void isAuthenticated_shouldReturnFalseWhenTokenAbsent() {
        try (var mockedJwtUtil = mockStatic(JwtUtil.class)) {
            mockedJwtUtil
                    .when(() -> JwtUtil.getBearerToken(any(Supplier.class)))
                    .thenReturn(Optional.empty());

            boolean isAuthenticated = jwtService.isAuthenticated();

            assertThat(isAuthenticated).isFalse();
        }
    }

    @Test
    void hasRole_shouldReturnTrueWhenRolePresent() throws Exception {
        try (var mockedJwtUtil = mockStatic(JwtUtil.class)) {
            var expectedRoles = List.of("role1", "role2");
            var signedJWT = mock(SignedJWT.class);
            var claimsSet = new JWTClaimsSet.Builder()
                    .claim("client-roles", expectedRoles)
                    .build();
            when(signedJWT.getJWTClaimsSet()).thenReturn(claimsSet);

            mockedJwtUtil
                    .when(() -> JwtUtil.getBearerToken(any(Supplier.class)))
                    .thenReturn(Optional.of(JWT_SAMPLE));
            mockedJwtUtil.when(() -> JwtUtil.parseJwt(anyString())).thenReturn(signedJWT);

            boolean hasRole = jwtService.hasRole("role1");

            assertThat(hasRole).isTrue();
        }
    }

    @Test
    void hasRole_shouldReturnFalseWhenRoleAbsent() throws Exception {
        try (var mockedJwtUtil = mockStatic(JwtUtil.class)) {
            var expectedRoles = List.of("role1", "role2");
            var signedJWT = mock(SignedJWT.class);
            var claimsSet = new JWTClaimsSet.Builder()
                    .claim("client-roles", expectedRoles)
                    .build();
            when(signedJWT.getJWTClaimsSet()).thenReturn(claimsSet);

            mockedJwtUtil
                    .when(() -> JwtUtil.getBearerToken(any(Supplier.class)))
                    .thenReturn(Optional.of(JWT_SAMPLE));
            mockedJwtUtil.when(() -> JwtUtil.parseJwt(anyString())).thenReturn(signedJWT);

            boolean hasRole = jwtService.hasRole("role3");

            assertThat(hasRole).isFalse();
        }
    }

    @Test
    void hasRole_shouldReturnFalseWhenClaimNotPresent() throws Exception {
        try (var mockedJwtUtil = mockStatic(JwtUtil.class)) {
            var signedJWT = mock(SignedJWT.class);
            var claimsSet = new JWTClaimsSet.Builder().build(); // No client-roles claim
            when(signedJWT.getJWTClaimsSet()).thenReturn(claimsSet);

            mockedJwtUtil
                    .when(() -> JwtUtil.getBearerToken(any(Supplier.class)))
                    .thenReturn(Optional.of(JWT_SAMPLE));
            mockedJwtUtil.when(() -> JwtUtil.parseJwt(anyString())).thenReturn(signedJWT);

            boolean hasRole = jwtService.hasRole("anyRole");

            assertThat(hasRole).isFalse();
        }
    }

    private static final String JWT_SAMPLE =
            """
    eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIyRmFweWVUN3g0TWZ3bXJtcFg4cTdlZXRGcTF2cEQz
    YnZoYllZQk1vV3BFIn0.eyJleHAiOjE3MzQwODU5NDIsImlhdCI6MTczNDA4NTY0MiwiYXV0aF90aW1lIjoxNzM0MDg0
    OTY4LCJqdGkiOiJkNWFjOGVjYi01OGU0LTQ3YTItOTkyNC00NTlmNmMyNjc1OTIiLCJpc3MiOiJodHRwOi8vbG9jYWxob
    3N0OjkwMTAvYXV0aC9yZWFsbXMvYXV0aG9yaXR5Iiwic3ViIjoiYmJhZmE5YmEtMGE2Ny00MzcyLWEyNWEtMjA1ZTVlZj
    gyZTVmIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiZnJvbnRlbmQtY2xpIiwic2lkIjoiN2FhNWM2ZWUtYmQ1Ny00ZjU3LTg
    5NzAtOWEzMWYyZDliZTBhIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJy
    b2xlcyI6WyJUMVVBUl9NIl19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwibmFtZ
    SI6Ik1hcmlhIEZveCIsInByZWZlcnJlZF91c2VybmFtZSI6Im0uZiIsImdpdmVuX25hbWUiOiJNYXJpYSIsImZhbWlseV
    9uYW1lIjoiRm94IiwiY2xpZW50LXJvbGVzIjpbIlQxVUFSX00iXSwiaWRlbnRpdHlfYXR0cmlidXRlcyI6W10sImVtYWl
    sIjoibS5mQGVtYWlsLmNvbSJ9.PaPCX-3SJ4NgBKli3U-JMZdHRcmGXlNZEXjScK1lMMmEH9FxbGhBcOSfvJ2HrpItDfU
    oWLrNrzFpcRcbv4AZCc99qMvWb9N6bJqxo-FniXhM2JLWWpigwJFIm8As4bCDHstlDakFjnSZQnYVLZ4juz74NcOSJnKm
    l6eEqWq00HoHpPSsGW3_8cKjoPFz29F3PbpQv2gq5IOihA-cnkU2PEbRKW7ZvTZOf3vpb2AxvxlpG8SzezXD2lLQBKiy9
    EzCmGDhEhUIJlMPw4QbO83W68GaDg3M9FbOsetKZ9JnQ_oS54C-TfriFDrqDcz9YzzVMr-M22SyFER9C6V5UUu8wQ
    """;
}
