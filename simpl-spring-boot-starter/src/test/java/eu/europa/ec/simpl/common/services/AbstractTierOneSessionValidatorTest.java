package eu.europa.ec.simpl.common.services;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import eu.europa.ec.simpl.common.ephemeralproof.EphemeralProofAbstractParser;
import eu.europa.ec.simpl.common.exceptions.EphemeralProofNotFoundException;
import eu.europa.ec.simpl.common.exceptions.InvalidTierOneSessionException;
import eu.europa.ec.simpl.common.exceptions.TierOneTokenNotFound;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.redis.entity.EphemeralProof;
import java.security.KeyPairGenerator;
import java.util.*;
import org.instancio.Instancio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;

@ExtendWith(MockitoExtension.class)
class AbstractTierOneSessionValidatorTest {

    private TestTierOneSessionValidator validator;

    @Mock
    private EphemeralProofAbstractParser<?> mockParser;

    @Mock
    private SignedJWT mockJwt;

    @Mock
    private HttpHeaders mockHeaders;

    @BeforeEach
    void setUp() {
        validator = spy(new TestTierOneSessionValidator());
    }

    @Test
    void validate_withHeadersInvalidIdentityAttributes_shouldThrowInvalidTierOneSessionException() {
        var credentialId = "junit-credentialId";

        var headers = new HttpHeaders();
        var token = "Bearer invalidTokenJunitTests";
        headers.add(HttpHeaders.AUTHORIZATION, token);

        assertThrows(InvalidTierOneSessionException.class, () -> validator.validate(headers, credentialId));
    }

    @Test
    void validate_withValidIdentityAttributes_shouldNotThrowException() throws Exception {
        var claim = "identity_attributes";
        var credentialId = "junit-credentialId";

        prepareMock(claim, credentialId);

        assertDoesNotThrow(() -> validator.validate(mockJwt, credentialId));
    }

    @Test
    void validate_withInvalidIdentityAttributes_shouldThrowInvalidTierOneSessionException() throws Exception {
        var claim = "identity_attributes";
        var credentialId = "junit-credentialId";

        prepareMock(claim, credentialId);
        when(mockJwt.verify(any())).thenReturn(false);

        assertThrows(InvalidTierOneSessionException.class, () -> validator.validate(mockJwt, credentialId));
    }

    @Test
    void validate_withNullJwt_shouldThrowTierOneTokenNotFound() {
        var credentialId = "validCredentialId";
        assertThrows(TierOneTokenNotFound.class, () -> validator.validate((SignedJWT) null, credentialId));
    }

    @Test
    void validate_withNonExistentCredentialId_shouldThrowEphemeralProofNotFoundException() {
        var credentialId = "nonExistentCredentialId";
        doReturn(Optional.empty()).when(validator).fetchEphemeralProofById(credentialId);

        assertThrows(EphemeralProofNotFoundException.class, () -> validator.validate(mockJwt, credentialId));
    }

    private void prepareMock(String claim, String credentialId) throws Exception {
        var ephemeralProof = Instancio.create(EphemeralProof.class);
        doReturn(Optional.of(ephemeralProof)).when(validator).fetchEphemeralProofById(credentialId);
        doReturn(mockParser).when(validator).getEphemeralProofParser(ephemeralProof.getContent());
        var publicKey = Base64.getEncoder()
                .encodeToString(KeyPairGenerator.getInstance("RSA")
                        .genKeyPair()
                        .getPublic()
                        .getEncoded());
        when(mockParser.getPublicKeys()).thenReturn(List.of(publicKey));
        var identityAttributeDTO = Instancio.create(IdentityAttributeDTO.class);
        identityAttributeDTO.setCode("attribute1");
        when(mockParser.getIdentityAttributes()).thenReturn(List.of(identityAttributeDTO));
        var jWTClaimsSet = mock(JWTClaimsSet.class);
        when(mockJwt.getJWTClaimsSet()).thenReturn(jWTClaimsSet);
        var listAttributes = List.of("attribute1");
        when(jWTClaimsSet.getClaim(claim)).thenReturn(listAttributes);
        when(mockJwt.verify(any())).thenReturn(true);
    }

    private static class TestTierOneSessionValidator
            extends AbstractTierOneSessionValidator<EphemeralProofAbstractParser<?>> {
        @Override
        protected Optional<EphemeralProof> fetchEphemeralProofById(String credentialId) {
            return Optional.empty();
        }

        @Override
        protected EphemeralProofAbstractParser<?> getEphemeralProofParser(String rawEphemeralProof) {
            return null;
        }
    }
}
