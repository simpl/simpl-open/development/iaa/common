package eu.europa.ec.simpl.common.test.ephemeralproof;

import eu.europa.ec.simpl.common.redis.entity.EphemeralProof;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EphemeralProofRepository extends CrudRepository<EphemeralProof, String> {}
