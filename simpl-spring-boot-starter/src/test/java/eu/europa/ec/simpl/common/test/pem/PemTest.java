package eu.europa.ec.simpl.common.test.pem;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import eu.europa.ec.simpl.common.exceptions.RuntimeWrapperException;
import eu.europa.ec.simpl.common.test.util.TestCertificateUtil;
import eu.europa.ec.simpl.common.utils.PemUtil;
import java.io.*;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.cert.CertificateException;
import java.util.List;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.OperatorCreationException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.Assert;

@ExtendWith(MockitoExtension.class)
class PemTest {

    @BeforeAll
    static void setUp() {
        Security.addProvider(new BouncyCastleProvider());
    }

    @Test
    void withCertificate_shouldWritePem()
            throws NoSuchAlgorithmException, OperatorCreationException, CertificateException {
        var cert = new JcaX509CertificateConverter()
                .getCertificate(TestCertificateUtil.anX509Certificate("CN=issuer", "CN=subject"));
        var output = new ByteArrayOutputStream();
        PemUtil.writePem(List.of(cert), output);
        Assert.isTrue(output.size() > 0, "Expected non empty stream");
    }

    @Test
    void loadPrivateKey_shouldLoadCorrectly() {
        var privateKeyPem =
                """
                -----BEGIN PRIVATE KEY-----
                MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgevZzL1gdAFr88hb2
                OF/2NxApJCzGCEDdfSp6VQO30hyhRANCAAQRWz+jn65BtOMvdyHKcvjBeBSDZH2r
                1RTwjmYSi9R/zpBnuQ4EiMnCqfMPWiZqB4QdbAd0E7oH50VpuZ1P087G
                -----END PRIVATE KEY-----
                """;
        var inputStream = new ByteArrayInputStream(privateKeyPem.getBytes(StandardCharsets.UTF_8));

        var privateKey = PemUtil.loadPrivateKey(inputStream, "EC");

        assertThat(privateKey).isNotNull();
        assertThat(privateKey.getAlgorithm()).isEqualTo("EC");
    }

    @Test
    void loadPemObjects_shouldLoadMultiplePemObjects() {
        String multiplePemContent =
                """
                -----BEGIN CERTIFICATE-----
                MIIBhTCCASugAwIBAgIJAIGUe0vBo6+qMAoGCCqGSM49BAMCMCMxITAfBgNVBAMM
                GEFydWJhIFNpbXBsIFRlc3QgUm9vdCBDQTAeFw0yMzA1MjMxNDI3MzhaFw0zMzA1
                MjAxNDI3MzhaMCMxITAfBgNVBAMMGEFydWJhIFNpbXBsIFRlc3QgUm9vdCBDQTBZ
                MBMGByqGSM49AgEGCCqGSM49AwEHA0IABHxD7M2RgXTYlDptR8pVZ7LVQeqJmCe5
                Qm1Ynp4FHvXXPHUj7aeKJJITpJJQPKJYe+7YTKHJuKQZJOzEGgGjUzBRMB0GA1Ud
                DgQWBBTx/92WpMjaRtD4F12REccB8MptPDAfBgNVHSMEGDAWgBTx/92WpMjaRtD4
                F12REccB8MptPDAPBgNVHRMBAf8EBTADAQH/MAoGCCqGSM49BAMCA0gAMEUCIQCQ
                1e3xALZ8H8TJC9NTl5MRPEsWFLwVQBzJFbTIrMd2xQIgMgXCTOC6JRQyC/dWrMM8
                hLsbaAJfK/hAfuJVtZ+TUqc=
                -----END CERTIFICATE-----
                -----BEGIN CERTIFICATE-----
                MIIBhTCCASugAwIBAgIJAIGUe0vBo6+qMAoGCCqGSM49BAMCMCMxITAfBgNVBAMM
                GEFydWJhIFNpbXBsIFRlc3QgUm9vdCBDQTAeFw0yMzA1MjMxNDI3MzhaFw0zMzA1
                MjAxNDI3MzhaMCMxITAfBgNVBAMMGEFydWJhIFNpbXBsIFRlc3QgUm9vdCBDQTBZ
                MBMGByqGSM49AgEGCCqGSM49AwEHA0IABHxD7M2RgXTYlDptR8pVZ7LVQeqJmCe5
                Qm1Ynp4FHvXXPHUj7aeKJJITpJJQPKJYe+7YTKHJuKQZJOzEGgGjUzBRMB0GA1Ud
                DgQWBBTx/92WpMjaRtD4F12REccB8MptPDAfBgNVHSMEGDAWgBTx/92WpMjaRtD4
                F12REccB8MptPDAPBgNVHRMBAf8EBTADAQH/MAoGCCqGSM49BAMCA0gAMEUCIQCQ
                1e3xALZ8H8TJC9NTl5MRPEsWFLwVQBzJFbTIrMd2xQIgMgXCTOC6JRQyC/dWrMM8
                hLsbaAJfK/hAfuJVtZ+TUqc=
                -----END CERTIFICATE-----
                """;

        var inputStream = new ByteArrayInputStream(multiplePemContent.getBytes(StandardCharsets.UTF_8));
        var result = PemUtil.loadPemObjects(inputStream);

        assertThat(result).hasSize(2);

        for (InputStream is : result) {
            try {
                assertThat(is.available() > 0).isTrue();
            } catch (Exception e) {
                fail("Exception while checking InputStream: " + e.getMessage());
            }
        }
    }

    @Test
    void loadPemObjects_shouldThrowRuntimeWrapperException_whenIOExceptionOccurs() throws Exception {
        var mockInputStream = mock(InputStream.class);
        when(mockInputStream.read(any(), anyInt(), anyInt())).thenThrow(new IOException("IO error"));

        assertThatThrownBy(() -> PemUtil.loadPemObjects(mockInputStream))
                .isInstanceOf(RuntimeWrapperException.class)
                .hasCauseInstanceOf(IOException.class);

        verify(mockInputStream).read(any(), anyInt(), anyInt());
    }
}
