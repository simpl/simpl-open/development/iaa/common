package eu.europa.ec.simpl.common.test.services;

import eu.europa.ec.simpl.common.ephemeralproof.JwtEphemeralProofParser;
import eu.europa.ec.simpl.common.redis.entity.EphemeralProof;
import eu.europa.ec.simpl.common.services.AbstractTierOneSessionValidator;
import eu.europa.ec.simpl.common.test.ephemeralproof.EphemeralProofRepository;
import java.util.Optional;
import org.springframework.stereotype.Component;

@Component
public class FakeTierOneSessionValidator extends AbstractTierOneSessionValidator<JwtEphemeralProofParser> {

    private final EphemeralProofRepository repository;

    public FakeTierOneSessionValidator(EphemeralProofRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<EphemeralProof> fetchEphemeralProofById(String credentialId) {
        return repository.findById(credentialId);
    }

    @Override
    public JwtEphemeralProofParser getEphemeralProofParser(String rawEphemeralProof) {
        return new JwtEphemeralProofParser(rawEphemeralProof);
    }
}
