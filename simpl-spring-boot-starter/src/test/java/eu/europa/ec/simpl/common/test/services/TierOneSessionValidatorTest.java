package eu.europa.ec.simpl.common.test.services;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import com.nimbusds.jwt.SignedJWT;
import eu.europa.ec.simpl.common.exceptions.EphemeralProofNotFoundException;
import eu.europa.ec.simpl.common.exceptions.TierOneTokenNotFound;
import eu.europa.ec.simpl.common.test.ephemeralproof.EphemeralProofRepository;
import java.util.Optional;
import org.instancio.Instancio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TierOneSessionValidatorTest {

    @Mock
    EphemeralProofRepository repository;

    @InjectMocks
    FakeTierOneSessionValidator validator;

    private String credentialId;

    @BeforeEach
    void setUp() {
        credentialId = Instancio.gen().hash().get();
    }

    @Test
    void shouldThrowTokenNotFoundWhenJWTIsNull() {
        assertThrows(TierOneTokenNotFound.class, () -> {
            validator.validate((SignedJWT) null, credentialId);
        });
    }

    @Test
    void shouldThrowEphemeralProofNotFoundWhenProofIsMissing() {
        var jwt = Instancio.of(SignedJWT.class).create();
        when(repository.findById(credentialId)).thenReturn(Optional.empty());

        assertThrows(EphemeralProofNotFoundException.class, () -> validator.validate(jwt, credentialId));
    }
}
