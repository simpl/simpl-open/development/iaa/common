package eu.europa.ec.simpl.common.test.util;

import eu.europa.ec.simpl.common.utils.CredentialUtil;
import eu.europa.ec.simpl.common.utils.PemUtil;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateException;
import org.assertj.core.api.Assertions;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CredentialUtilTest {

    private static final String PRIVATE_KEY =
            """
            -----BEGIN PRIVATE KEY-----
            MIGTAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQg7VQESFg6MBW1uEQM
            ZDV6R5eCTPY1bux7dUHpPMrUdCqgCgYIKoZIzj0DAQehRANCAATCcRx6VGC7mIHl
            zdw32UVHf6WswIxAKjTXAvQsItYlHIoh2mqL2OxED11tBqS7mDSImR4EIug3GcSN
            GzXpIPQ6
            -----END PRIVATE KEY-----
            """;

    private static final String PEM_EXAMPLE =
            """
            -----BEGIN CERTIFICATE-----
            MIIDBjCCAqygAwIBAgIUPCN5qyFiXiQSbQ/DObwZ7P6FA5MwCgYIKoZIzj0EAwIw
            FzEVMBMGA1UEAwwMT25Cb2FyZGluZ0NBMB4XDTI0MDgyMzA4NDQxMloXDTI2MDgy
            MDA4NDQxMVowTjEdMBsGA1UECgwUR09WRVJOQU5DRV9BVVRIT1JJVFkxLTArBgNV
            BAMMJDAxOTE3ZTcxLTc4MDQtN2Q2YS1iZWNmLTdmNDJhZTI5Yzk4YjBZMBMGByqG
            SM49AgEGCCqGSM49AwEHA0IABMJxHHpUYLuYgeXN3DfZRUd/pazAjEAqNNcC9Cwi
            1iUciiHaaovY7EQPXW0GpLuYNIiZHgQi6DcZxI0bNekg9DqjggGdMIIBmTAfBgNV
            HSMEGDAWgBT1ctM6xEV8ANPsDb1Dp0Plf4E47zCBiwYIKwYBBQUHAQEEfzB9MEIG
            CCsGAQUFBzAChjZodHRwczovL2F1dGhvcml0eS5iZS5hcnViYS1zaW1wbC5jbG91
            ZC9jYS9PbkJvYXJkaW5nQ0EwNwYIKwYBBQUHMAGGK2h0dHBzOi8vYXV0aG9yaXR5
            LmJlLmFydWJhLXNpbXBsLmNsb3VkL29jc3AwUAYDVR0RBEkwR4IfdGxzLmF1dGhv
            cml0eS5hcnViYS1zaW1wbC5jbG91ZIIkMDE5MTdlNzEtNzgwNC03ZDZhLWJlY2Yt
            N2Y0MmFlMjljOThiMB0GA1UdJQQWMBQGCCsGAQUFBwMCBggrBgEFBQcDATBIBgNV
            HR8EQTA/MD2gO6A5hjdodHRwczovL2F1dGhvcml0eS5iZS5hcnViYS1zaW1wbC5j
            bG91ZC9jcmwvT25Cb2FyZGluZ0NBMB0GA1UdDgQWBBQaGaEzSM50hx0lXcZxHGvV
            MxZQdDAOBgNVHQ8BAf8EBAMCBaAwCgYIKoZIzj0EAwIDSAAwRQIgP3WQUDHZj79u
            Vcz8jQXRh6EWyyJ1zGk5o2ySXPekRy0CIQCTFqczuC8yIdJRjLLMlANiTtJu28lD
            AoWWpLlqO5VWEQ==
            -----END CERTIFICATE-----
            -----BEGIN CERTIFICATE-----
            MIIBjTCCATSgAwIBAgIUL1iIet5OYqAye9D2XSiXehac3VQwCgYIKoZIzj0EAwIw
            EjEQMA4GA1UEAwwHU2ltcGxDQTAeFw0yNDA3MTAwOTUyMjZaFw0zOTA3MDcwOTUy
            MjVaMBcxFTATBgNVBAMMDE9uQm9hcmRpbmdDQTBZMBMGByqGSM49AgEGCCqGSM49
            AwEHA0IABBRBA2W51oeHyQub9SkQCyFK1PGMVxJdhqgJ+qAFWK994XLfdLjRgQj0
            Nnspl+OQCMJuxnDA6FZ4MBBtSmn1GFSjYzBhMA8GA1UdEwEB/wQFMAMBAf8wHwYD
            VR0jBBgwFoAU8f/dlqTI2kbQ+BddkRHHAfDKbTwwHQYDVR0OBBYEFPVy0zrERXwA
            0+wNvUOnQ+V/gTjvMA4GA1UdDwEB/wQEAwIBhjAKBggqhkjOPQQDAgNHADBEAiAK
            jMEyHssYiIQ//WWZfP+3jGwarCn58MfoN4NaZCk2mwIgY5qsizW4OfFbwb4cGjhY
            ZEyqptOWq10VcYqGl4kWSsQ=
            -----END CERTIFICATE-----
            -----BEGIN CERTIFICATE-----
            MIIBiDCCAS+gAwIBAgIUYfU7fzOiwcFENAMzSWkK+qttVUkwCgYIKoZIzj0EAwIw
            EjEQMA4GA1UEAwwHU2ltcGxDQTAeFw0yNDA3MTAwOTQ3MzlaFw00OTA3MTEwOTQ3
            MzhaMBIxEDAOBgNVBAMMB1NpbXBsQ0EwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNC
            AASpwyjHXCKCq0OFPA/07RHQbwBkEzU/iyeia+BvL7kDXxSxt5ojrFskK8iBqPp4
            c0+yiQAQvgEEtRDK1IPSD3dro2MwYTAPBgNVHRMBAf8EBTADAQH/MB8GA1UdIwQY
            MBaAFPH/3ZakyNpG0PgXXZERxwHwym08MB0GA1UdDgQWBBTx/92WpMjaRtD4F12R
            EccB8MptPDAOBgNVHQ8BAf8EBAMCAYYwCgYIKoZIzj0EAwIDRwAwRAIgaqb0slrT
            DOKWIqYrxZkCXPHitiIRbies0tCkaAxGVLoCIGGE7TogEtgeXVd+DD3tOxiqK2ED
            KrvR6DcvbU1kSo7v
            -----END CERTIFICATE-----
            """;

    @BeforeAll
    static void setUpBeforeClass() {
        Security.addProvider(new BouncyCastleProvider());
    }

    @Test
    void withValidPem_shouldLoadKeystore() throws KeyStoreException {
        var privateKey =
                PemUtil.loadPrivateKey(new ByteArrayInputStream(PRIVATE_KEY.getBytes(StandardCharsets.UTF_8)), "EC");
        var keyStore = CredentialUtil.loadCredential(
                new ByteArrayInputStream(PEM_EXAMPLE.getBytes(StandardCharsets.UTF_8)), privateKey);
        Assertions.assertThat(keyStore.aliases().hasMoreElements()).isTrue();
    }

    @Test
    void withValidPem_withInvalidPublicKey_shouldThrowSignatureException() throws NoSuchAlgorithmException {
        var privateKey = TestCertificateUtil.generateKeyPair().getPrivate();

        Assertions.assertThatThrownBy(() -> CredentialUtil.loadCredential(
                        new ByteArrayInputStream(PEM_EXAMPLE.getBytes(StandardCharsets.UTF_8)), privateKey))
                .hasCauseInstanceOf(SignatureException.class);
    }

    @Test
    void withValidPem_shouldLoadKeystore_withThreeCertificates() throws KeyStoreException {
        var privateKey =
                PemUtil.loadPrivateKey(new ByteArrayInputStream(PRIVATE_KEY.getBytes(StandardCharsets.UTF_8)), "EC");
        var keyStore = CredentialUtil.loadCredential(
                new ByteArrayInputStream(PEM_EXAMPLE.getBytes(StandardCharsets.UTF_8)), privateKey);
        Assertions.assertThat(keyStore.getCertificateChain("chain")).hasSize(3);
    }

    @Test
    void withValidKeystore_ShouldCreateTrustStore()
            throws NoSuchAlgorithmException, IOException, CertificateException, KeyStoreException {
        var privateKey =
                PemUtil.loadPrivateKey(new ByteArrayInputStream(PRIVATE_KEY.getBytes(StandardCharsets.UTF_8)), "EC");
        var keyStore = CredentialUtil.loadCredential(
                new ByteArrayInputStream(PEM_EXAMPLE.getBytes(StandardCharsets.UTF_8)), privateKey);
        var trustStore = CredentialUtil.buildTrustStore(keyStore);
        Assertions.assertThat(keyStore.getCertificateChain("chain")).hasSize(trustStore.size() + 1);
    }
}
