package eu.europa.ec.simpl.common.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class Base64UtilTest {

    @ParameterizedTest
    @CsvSource({"TWFu, 3", "TWE=, 2", "TQ==, 1", "VGhpcyBpcyBhIHRlc3Q=, 14", "YWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXo=, 26"
    })
    void getSizeInBytes_shouldReturnCorrectSize(String input, long expected) {
        assertEquals(expected, Base64Util.getSizeInBytes(input));
    }

    @Test
    void getSizeInBytes_withNullInput_shouldThrowNullPointerException() {
        assertThrows(NullPointerException.class, () -> Base64Util.getSizeInBytes(null));
    }

    @Test
    void getSizeInBytes_withLongInput_shouldHandleCorrectly() {
        String longInput = "A".repeat(1000000); // 1 million 'A's
        assertEquals(750000, Base64Util.getSizeInBytes(longInput));
    }

    @Test
    void getSizeInBytes_withInvalidBase64_shouldStillCalculate() {
        String invalidBase64 = "ThisIsNotValidBase64!@#?";
        assertEquals(18, Base64Util.getSizeInBytes(invalidBase64));
    }

    @Test
    void getSizeInBytes_withMixedPadding_shouldHandleCorrectly() {
        assertEquals(1, Base64Util.getSizeInBytes("TQ=="));
        assertEquals(1, Base64Util.getSizeInBytes("TQ="));
        assertEquals(1, Base64Util.getSizeInBytes("TQ"));
    }
}
