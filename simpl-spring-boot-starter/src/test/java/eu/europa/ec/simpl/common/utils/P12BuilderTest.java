package eu.europa.ec.simpl.common.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import eu.europa.ec.simpl.common.test.util.TestCertificateUtil;
import java.io.ByteArrayOutputStream;
import java.security.*;
import java.util.Collections;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class P12BuilderTest {

    @BeforeEach
    void setUp() {
        Security.addProvider(new BouncyCastleProvider());
    }

    @Test
    void testGetExpiryDate() throws Exception {
        var alias = "testAlias";
        var password = "testPassword";
        var keyPair = TestCertificateUtil.generateKeyPair();
        var privateKey = keyPair.getPrivate();
        var certificateHolder = TestCertificateUtil.anX509Certificate("CN=issuer", "CN=subject", keyPair);
        var certificate = new JcaX509CertificateConverter().setProvider("BC").getCertificate(certificateHolder);
        var certificateList = Collections.singletonList(certificate);

        var outputStream = new ByteArrayOutputStream();
        var p12Builder = new P12Builder(alias, password, privateKey, certificateList, outputStream);
        p12Builder.buildKeyStore();

        var expiryDate = p12Builder.getExpiryDate();

        assertThat(expiryDate).isEqualTo(certificate.getNotAfter().toInstant());
    }
}
