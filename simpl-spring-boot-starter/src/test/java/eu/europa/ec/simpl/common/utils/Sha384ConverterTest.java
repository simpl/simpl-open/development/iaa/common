package eu.europa.ec.simpl.common.utils;

import static org.junit.jupiter.api.Assertions.*;

import eu.europa.ec.simpl.common.exceptions.RuntimeWrapperException;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.stream.Stream;
import javax.crypto.KeyGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class Sha384ConverterTest {

    @Test
    void toSha384_withKey_shouldReturnValidSha384() throws NoSuchAlgorithmException {
        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        keyGen.init(256);
        Key key = keyGen.generateKey();

        String result = Sha384Converter.toSha384(key);

        assertNotNull(result);
        assertEquals(64, result.length());
        assertTrue(result.matches("^[A-Za-z0-9+/]+=*$"));
    }

    @Test
    void toSha384_withByteArray_shouldReturnValidSha384() {
        byte[] input = "Test".getBytes(StandardCharsets.UTF_8);

        String result = Sha384Converter.toSha384(input);

        assertNotNull(result);
        assertEquals(64, result.length());
        assertEquals("e49GVAdrgOuWORHxnPrRqvQoXtSOgm9s3hsBp5qnP621RG5mf8T5BBd4LJEnBUDz", result);
    }

    @ParameterizedTest
    @MethodSource("provideShaTestCases")
    void toSha_withVariousAlgorithms_shouldReturnCorrectHash(byte[] input, String algorithm, String expected) {
        String result = Sha384Converter.toSha(input, algorithm);

        assertNotNull(result);
        assertEquals(expected, result);
    }

    @Test
    void toSha_withInvalidAlgorithm_shouldThrowRuntimeWrapperException() {
        byte[] input = "Test".getBytes(StandardCharsets.UTF_8);

        assertThrows(RuntimeWrapperException.class, () -> Sha384Converter.toSha(input, "INVALID_ALGORITHM"));
    }

    private static Stream<Arguments> provideShaTestCases() {
        return Stream.of(
                Arguments.of(
                        "Hello, World!".getBytes(StandardCharsets.UTF_8),
                        "SHA-256",
                        "3/1gIbsr1bCvZ2KQgJ7DpTGR3YHH9wpLKGiKNiGCmG8="),
                Arguments.of(
                        "Hello, World!".getBytes(StandardCharsets.UTF_8),
                        "SHA-384",
                        "VIXMmzNltDBd+06DN+ClmKV0+CQr8XKJ4N1sIKPNRKCJ3harSrMI9j5EsRcOtfUV"),
                Arguments.of(
                        "Hello, World!".getBytes(StandardCharsets.UTF_8),
                        "SHA-512",
                        "N015SpXNz9izWZMYX++bo2jxYNja9DLQi6nx7R5avmzGkpHg+i/gAGpSVw7xjBne9OYXwzzlLvCm5fvjGMsDhw=="));
    }
}
