package eu.europa.ec.simpl.common.utils;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.data.domain.Sort.Order.asc;
import static org.springframework.data.domain.Sort.Order.desc;

import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Sort;

class SortUtilTest {

    @Test
    void sortBy() {
        var actual = SortUtil.sortBy(List.of("property1", "ASC", "property2", "DESC"));
        assertThat(actual).isEqualTo(Sort.by(List.of(asc("property1"), desc("property2"))));
    }

    @Test
    void toStringList() {
        var actual = SortUtil.toStringList(Sort.by(List.of(asc("property1"), desc("property2"))));
        assertThat(actual).isEqualTo(List.of("property1", "ASC", "property2", "DESC"));
    }
}
