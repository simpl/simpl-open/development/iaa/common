package eu.europa.ec.simpl.common.test;

import static org.mockito.AdditionalAnswers.delegatesTo;

import java.util.Arrays;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import org.mockito.Mockito;

public class MockUtil {

    private MockUtil() {
        throw new UnsupportedOperationException("Utility class");
    }

    @SuppressWarnings("unchecked")
    public static <R> Supplier<R> spyLambda(final Supplier<R> lambda) {
        Class<?>[] interfaces = Arrays.stream(lambda.getClass().getInterfaces())
                .filter(Supplier.class::isAssignableFrom)
                .toArray(Class[]::new);
        return (Supplier<R>) Mockito.mock(interfaces[0], delegatesTo(lambda));
    }

    @SuppressWarnings("unchecked")
    public static <T, R> Function<T, R> spyLambda(final Function<T, R> lambda) {
        Class<?>[] interfaces = Arrays.stream(lambda.getClass().getInterfaces())
                .filter(Function.class::isAssignableFrom)
                .toArray(Class[]::new);
        return (Function<T, R>) Mockito.mock(interfaces[0], delegatesTo(lambda));
    }

    @SuppressWarnings("unchecked")
    public static <T, U, R> BiFunction<T, U, R> spyLambda(final BiFunction<T, U, R> lambda) {
        Class<?>[] interfaces = Arrays.stream(lambda.getClass().getInterfaces())
                .filter(BiFunction.class::isAssignableFrom)
                .toArray(Class[]::new);
        return (BiFunction<T, U, R>) Mockito.mock(interfaces[0], delegatesTo(lambda));
    }
}
