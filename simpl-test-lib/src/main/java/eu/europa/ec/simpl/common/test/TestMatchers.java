package eu.europa.ec.simpl.common.test;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import org.mockito.ArgumentMatcher;

public class TestMatchers {

    private TestMatchers() {
        throw new UnsupportedOperationException("Utility class");
    }

    @SafeVarargs
    public static <T> ArgumentMatcher<T> matches(Predicate<T>... predicates) {
        return argument -> Arrays.stream(predicates)
                .reduce(Predicate::and)
                .map(predicate -> predicate.test(argument))
                .orElse(true);
    }

    @SafeVarargs
    public static <T> ArgumentMatcher<List<T>> elementsMatchByIndex(BiFunction<T, Integer, Boolean>... predicates) {
        return elementsMatchBy(Function.identity(), predicates);
    }

    @SafeVarargs
    public static <T, O> ArgumentMatcher<List<T>> elementsMatchBy(
            Function<Integer, O> expectedElementSupplier, BiFunction<T, O, Boolean>... predicates) {
        return argument -> Arrays.stream(predicates)
                .reduce((p1, p2) -> (t, integer) -> p1.apply(t, integer) && p2.apply(t, integer))
                .map(predicate -> IntStream.range(0, argument.size())
                        .mapToObj(index -> predicate.apply(argument.get(index), expectedElementSupplier.apply(index)))
                        .reduce((a, b) -> a && b)
                        .orElse(true))
                .orElse(true);
    }
}
