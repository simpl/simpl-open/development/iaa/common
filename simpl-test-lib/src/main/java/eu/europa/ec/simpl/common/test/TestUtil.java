package eu.europa.ec.simpl.common.test;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import org.instancio.Instancio;

public class TestUtil {

    private TestUtil() {
        throw new UnsupportedOperationException("Utility class");
    }

    public static <T> T a(Class<T> type) {
        return Instancio.of(type).create();
    }

    public static <T> T an(Class<T> type) {
        return a(type);
    }

    public static <T> Optional<T> anOptional(Class<T> type) {
        return Optional.of(a(type));
    }

    public static <T> List<T> aListOf(Class<T> type) {
        return Instancio.ofList(type).create();
    }

    public static <T> List<T> aListOf(int size, Class<T> type) {
        return Instancio.ofList(type).size(size).create();
    }

    public static <T> Set<T> aSetOf(Class<T> type) {
        return Instancio.ofSet(type).create();
    }

    public static <T> List<T> anEmptyList() {
        return List.of();
    }

    public static UUID anUUID() {
        return Instancio.gen().uuid().get();
    }

    public static URI anURI() {
        return Instancio.gen().net().uri().get();
    }

    public static URI anURI(String suffix) {
        return Instancio.gen().net().uri().map(uri -> uri.resolve(suffix));
    }

    public static String aString() {
        return Instancio.gen().string().get();
    }

    public static String anHash() {
        return Instancio.gen().hash().get();
    }
}
