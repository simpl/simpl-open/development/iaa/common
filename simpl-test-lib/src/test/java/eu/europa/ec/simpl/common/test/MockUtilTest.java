package eu.europa.ec.simpl.common.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import org.junit.jupiter.api.Test;

class MockUtilTest {

    @Test
    void testSpyLambdaSupplier() {
        Supplier<String> originalSupplier = () -> "test";
        Supplier<String> spySupplier = MockUtil.spyLambda(originalSupplier);

        var result = spySupplier.get();

        assertThat(result).isEqualTo("test");
        verify(spySupplier, times(1)).get();
    }

    @Test
    void testSpyLambdaFunction() {
        Function<Integer, String> originalFunction = (i) -> "Number: " + i;
        Function<Integer, String> spyFunction = MockUtil.spyLambda(originalFunction);

        var result = spyFunction.apply(5);

        assertThat(result).isEqualTo("Number: 5");
        verify(spyFunction, times(1)).apply(5);
    }

    @Test
    void testSpyLambdaBiFunction() {
        BiFunction<String, Integer, String> originalBiFunction = (s, i) -> s + i;
        BiFunction<String, Integer, String> spyBiFunction = MockUtil.spyLambda(originalBiFunction);

        var result = spyBiFunction.apply("Test: ", 42);

        assertThat(result).isEqualTo("Test: 42");
        verify(spyBiFunction, times(1)).apply("Test: ", 42);
    }
}
