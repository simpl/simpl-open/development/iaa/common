package eu.europa.ec.simpl.common.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;

class ResultCaptorTest {

    @Test
    void testConstructorAndGetResult() {
        var captor = new ResultCaptor<>(String.class);
        assertThat(captor.getResult()).isNull();
    }

    @Test
    void testAnswer() throws Throwable {
        var captor = new ResultCaptor<>(String.class);
        var invocation = mock(InvocationOnMock.class);

        var expectedResult = "test result";
        when(invocation.callRealMethod()).thenReturn(expectedResult);

        var result = captor.answer(invocation);

        assertThat(result).isEqualTo(expectedResult);
        assertThat(captor.getResult()).isEqualTo(expectedResult);
    }

    @Test
    void testAnswerWithDifferentType() throws Throwable {
        var captor = new ResultCaptor<>(Integer.class);
        var invocation = mock(InvocationOnMock.class);

        var expectedResult = 42;
        when(invocation.callRealMethod()).thenReturn(expectedResult);

        var result = captor.answer(invocation);

        assertThat(result).isEqualTo(expectedResult);
        assertThat(captor.getResult()).isEqualTo(expectedResult);
    }
}
