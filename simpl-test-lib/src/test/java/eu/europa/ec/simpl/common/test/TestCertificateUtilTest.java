package eu.europa.ec.simpl.common.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.cert.CertificateException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.OperatorCreationException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class TestCertificateUtilTest {

    private static final String ISSUER = "CN=Test Issuer";
    private static final String SUBJECT = "CN=Test Subject";

    @BeforeAll
    static void setUp() {
        Security.addProvider(new BouncyCastleProvider());
    }

    @Test
    void testAnEncodedKeystore2() throws NoSuchAlgorithmException, OperatorCreationException, CertificateException {
        var keyPair = TestCertificateUtil.generateKeyPair();
        var encodedKeystore = TestCertificateUtil.anEncodedKeystore(ISSUER, SUBJECT, keyPair);
        assertThat(encodedKeystore).isNotEmpty();
    }

    @Test
    void testGenerateKeyPair() throws NoSuchAlgorithmException {
        var keyPair = TestCertificateUtil.generateKeyPair();
        assertThat(keyPair).isNotNull();
        assertThat(keyPair.getPrivate()).isNotNull();
        assertThat(keyPair.getPublic()).isNotNull();
    }

    @Test
    void testAnX509CertificateWithKeyPair() throws NoSuchAlgorithmException, OperatorCreationException {
        var keyPair = TestCertificateUtil.generateKeyPair();
        var certificateHolder = TestCertificateUtil.anX509Certificate(ISSUER, SUBJECT, keyPair);
        assertThat(certificateHolder).isNotNull();
        assertThat(certificateHolder.getIssuer()).hasToString(ISSUER);
        assertThat(certificateHolder.getSubject()).hasToString(SUBJECT);
    }

    @Test
    void testAnX509CertificateWithoutKeyPair() throws NoSuchAlgorithmException, OperatorCreationException {
        var certificateHolder = TestCertificateUtil.anX509Certificate(ISSUER, SUBJECT);
        assertThat(certificateHolder).isNotNull();
        assertThat(certificateHolder.getIssuer()).hasToString(ISSUER);
        assertThat(certificateHolder.getSubject()).hasToString(SUBJECT);
    }
}
