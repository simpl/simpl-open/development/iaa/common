package eu.europa.ec.simpl.common.test;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class TestDataUtilTest {

    @Test
    void testAPageOf() {
        var page = TestDataUtil.aPageOf(String.class);

        assertThat(page).isNotNull();
        assertThat(page.getContent()).isNotEmpty();
        assertThat(page.getContent().getFirst()).isInstanceOf(String.class);
    }

    @Test
    void testAPageableWithSize() {
        var size = 20;
        var pageable = TestDataUtil.aPageable(size);

        assertThat(pageable).isNotNull();
        assertThat(pageable.getPageSize()).isEqualTo(size);
    }

    @Test
    void testAPageableWithDefaultSize() {
        var pageable = TestDataUtil.aPageable();

        assertThat(pageable).isNotNull();
        assertThat(pageable.getPageSize()).isEqualTo(10);
    }
}
