package eu.europa.ec.simpl.common.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import org.junit.jupiter.api.Test;

class TestMatchersTest {

    @Test
    void testMatches() {
        Predicate<Integer> isEven = i -> i % 2 == 0;
        Predicate<Integer> isPositive = i -> i > 0;

        var matcher = TestMatchers.matches(isEven, isPositive);

        assertThat(matcher.matches(4)).isTrue();
        assertThat(matcher.matches(3)).isFalse();
        assertThat(matcher.matches(-2)).isFalse();
    }

    @Test
    void testElementsMatchByIndex() {
        BiFunction<String, Integer, Boolean> startsWithIndex = (s, i) -> s.startsWith(i.toString());
        BiFunction<String, Integer, Boolean> endsWithIndex = (s, i) -> s.endsWith(i.toString());

        var matcher = TestMatchers.elementsMatchByIndex(startsWithIndex, endsWithIndex);

        assertThat(matcher.matches(Arrays.asList("0zero0", "1one1", "2two2"))).isTrue();
        assertThat(matcher.matches(Arrays.asList("0zero1", "1one2", "2two0"))).isFalse();
    }

    @Test
    void testElementsMatchBy() {
        Function<Integer, String> expectedElementSupplier = i -> i + "test" + i;
        BiFunction<String, String, Boolean> startsWith = (actual, expected) -> actual.startsWith(expected);
        BiFunction<String, String, Boolean> endsWith = (actual, expected) -> actual.endsWith(expected);

        var matcher = TestMatchers.elementsMatchBy(expectedElementSupplier, startsWith, endsWith);

        assertThat(matcher.matches(Arrays.asList("0test0", "1test1", "2test2"))).isTrue();
        assertThat(matcher.matches(Arrays.asList("0test1", "1test2", "2test0"))).isFalse();
    }
}
