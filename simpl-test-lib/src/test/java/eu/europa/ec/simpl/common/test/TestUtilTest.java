package eu.europa.ec.simpl.common.test;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class TestUtilTest {

    @Test
    void testA() {
        var result = TestUtil.a(String.class);
        assertThat(result).isNotNull();
    }

    @Test
    void testAn() {
        var result = TestUtil.an(Integer.class);
        assertThat(result).isNotNull();
    }

    @Test
    void testAnOptional() {
        var result = TestUtil.anOptional(Double.class);
        assertThat(result).isPresent();
    }

    @Test
    void testAListOf() {
        var result = TestUtil.aListOf(Boolean.class);
        assertThat(result).isNotEmpty();
    }

    @Test
    void testAListOfWithSize() {
        var size = 5;
        var result = TestUtil.aListOf(size, Character.class);
        assertThat(result).hasSize(size);
    }

    @Test
    void testASetOf() {
        var result = TestUtil.aSetOf(Float.class);
        assertThat(result).isNotEmpty();
    }

    @Test
    void testAnEmptyList() {
        var result = TestUtil.anEmptyList();
        assertThat(result).isEmpty();
    }

    @Test
    void testAnUUID() {
        var result = TestUtil.anUUID();
        assertThat(result).isNotNull();
    }

    @Test
    void testAnURI() {
        var result = TestUtil.anURI();
        assertThat(result).isNotNull();
    }

    @Test
    void testAnURIWithSuffix() {
        var suffix = "test";
        var result = TestUtil.anURI(suffix);
        assertThat(result).isNotNull();
        assertThat(result.toString()).endsWith(suffix);
    }

    @Test
    void testAString() {
        var result = TestUtil.aString();
        assertThat(result).isNotEmpty();
    }

    @Test
    void testAnHash() {
        var result = TestUtil.anHash();
        assertThat(result).isNotEmpty();
    }
}
