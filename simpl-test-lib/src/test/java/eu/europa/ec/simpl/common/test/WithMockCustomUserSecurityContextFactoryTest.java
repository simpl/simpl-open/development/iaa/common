package eu.europa.ec.simpl.common.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

class WithMockCustomUserSecurityContextFactoryTest {

    private WithMockCustomUserSecurityContextFactory factory;

    @Mock
    private WithMockSecurityContext annotation;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        factory = new WithMockCustomUserSecurityContextFactory("ROLE_");
    }

    @Test
    void testCreateSecurityContext() {
        String[] roles = {"USER", "ADMIN"};
        String email = "test@example.com";
        when(annotation.roles()).thenReturn(roles);
        when(annotation.email()).thenReturn(email);

        SecurityContext securityContext = factory.createSecurityContext(annotation);

        Authentication authentication = securityContext.getAuthentication();
        JwtAuthenticationToken jwtAuth = (JwtAuthenticationToken) authentication;
        Jwt jwt = jwtAuth.getToken();

        assertThat(email).isEqualTo(jwt.getClaim("email"));
        assertThat(jwtAuth.getAuthorities().size()).isSameAs(2);
        assertThat(jwtAuth.getAuthorities().stream()
                        .anyMatch(a -> a.getAuthority().equals("ROLE_USER")))
                .isTrue();
        assertThat(jwtAuth.getAuthorities().stream()
                        .anyMatch(a -> a.getAuthority().equals("ROLE_ADMIN")))
                .isTrue();
    }
}
